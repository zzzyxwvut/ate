package org.zzzyxwvut.ate.internal;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;

public class ClassMethodNameFilterTests implements Testable
{
	private static void doTest(String testName,
				String regex,
				String tokens,
				Predicate<ClassMethodNameFilter> tester)
	{
		final ClassMethodNameFilter filter = new ClassMethodNameFilter(
								regex,
								tokens);
		assert tester.test(filter) : String.format("%s: %s",
							testName,
							filter.toString());
	}

	public void testIsEmpty(String regex)
	{
		doTest("testIsEmpty", regex, null,
				ClassMethodNameFilter::useDefaultSelection);
	}

	public void testIsEmpty(String regex, String tokens)
	{
		doTest("testIsEmpty", regex, tokens,
				ClassMethodNameFilter::useDefaultSelection);
	}

	public void testIsNotEmpty(String regex)
	{
		doTest("testIsNotEmpty", regex, null,
			Predicate.not(ClassMethodNameFilter::useDefaultSelection));
	}

	public void testIsNotEmpty(String regex, String tokens)
	{
		doTest("testIsNotEmpty", regex, tokens,
			Predicate.not(ClassMethodNameFilter::useDefaultSelection));
	}

	public void testRequestedClass(Class<? extends Testable> testClass,
								String regex)
	{
		final ClassMethodNameFilter filter = new ClassMethodNameFilter(
								regex,
								",@@");
		assert filter.isRequestedClass(testClass) :
					"testRequestedClass: "
						.concat(filter.toString());
	}

	public void testRequestedMethods(Class<? extends Testable> testClass,
					String regex,
					List<String> expected)
	{
		final List<String> obtained = new ClassMethodNameFilter(regex,
									null)
			.requestedMethods(testClass)
			.stream()
			.flatMap(methods -> methods
				.stream()
				.map(Pattern::pattern))
			.collect(Collectors.toUnmodifiableList());
		assert expected.equals(obtained) : String.format("%s != %s",
								expected,
								obtained);
	}

	public void testRightToLeftComponentProcessing(String regex,
							String expected)
	{
		final String obtained = new ClassMethodNameFilter(regex, null)
			.toString();
		assert expected.equals(obtained) : String.format("%s != %s",
								expected,
								obtained);
	}

	public static Instantiable<ClassMethodNameFilterTests> instantiable()
	{
		return Instantiable.<ClassMethodNameFilterTests>newBuilder(
				Set.of(ClassMethodNameFilterTests::new))
			.arguments(arguments())
			.build();
	}

	private static Map<String, Collection<List<?>>> arguments()
	{
		final String aClassName = A.class.getCanonicalName();
		final String zClassName = Z.class.getCanonicalName();
		final String ηταMethodsValue = "org\\..+\\.[^A]@η,τ,α";
		final String ηταMethodsClass = ηταMethodsValue.substring(
					0,
					ηταMethodsValue.lastIndexOf('@'));
		return Map.of("testIsEmpty",
				Set.of(List.of(""),
					List.of(":"),
					List.of("::"),
					List.of("@"),
					List.of("@@"),
					List.of(","),
					List.of(",,"),
					List.of(":@,"),

					List.of("", ":@,"),
					List.of(":", ":@,"),
					List.of("::", ":@,"),
					List.of(":@,", ":@,"),

					List.of("", ";ƒƒ"),
					List.of(";", ";ƒƒ"),
					List.of(";;", ";ƒƒ"),
					List.of("ƒ", ";ƒƒ"),
					List.of("ƒƒ", ";@ƒ"),
					List.of(";ƒƒ", ";ƒƒ")),
			"testIsNotEmpty",
				Set.of(List.of("org.Α"),
					List.of(":org.Α"),
					List.of("org.Α:"),
					List.of("org.Α:org.Δ"),
					List.of("org.Α@α"),
					List.of("org.Α@α:org.Δ@δ"),
					List.of("org.Α@α,β:org.Δ@δ,ε"),
					List.of(".+@[βδ]:com.+@ignore[βδ]"),

					List.of("org.Α", ":@,"),
					List.of(":org.Α", ":@,"),
					List.of("org.Α:", ":@,"),
					List.of("org.Α:org.Δ", ":@,"),
					List.of("org.Α@α", ":@,"),
					List.of("org.Α@α:org.Δ@δ", ":@,"),
					List.of("org.Α@α,β:org.Δ@δ,ε", ":@,"),
					List.of(".+@[βδ]:com.+@[βδ]", ":@,"),

					List.of("org.Α", ";ƒƒ"),
					List.of(";org.Α", ";ƒƒ"),
					List.of("org.Α;", ";ƒƒ"),
					List.of("org.Α;org.Δ", ";ƒƒ"),
					List.of("org.Αƒα", ";ƒƒ"),
					List.of("org.Αƒα;org.Δƒδ", ";ƒƒ"),
					List.of("org.Αƒαƒβ;org.Δƒδƒε", ";ƒƒ"),
					List.of(".+ƒ[βδ];com.+ƒ[βδ]", ";ƒƒ")),
			"testRequestedClass",
				Set.of(List.of(ClassMethodNameFilterTests.class,
							"org\\.zzzyxwvut\\."
						+ "ate\\.internal\\."
						+ "ClassMethodNameFilterTests"),
					List.of(ClassMethodNameFilterTests.class,
							"o.+?\\.z.+?\\."
						+ "ate\\.i.+?\\."
						+ "(?:[CMNFT]\\p{Lower}+?){5}?"),
					List.of(ClassMethodNameFilterTests.class,
						"org\\.zzzyxwvut\\.ate"),
					List.of(ClassMethodNameFilterTests.class,
						".+?\\.ClassMethodNameFilterTests")),
			"testRequestedMethods",
				Set.of(List.of(A.class,
						aClassName,
						List.of()),
					List.of(A.class,
						aClassName.concat("@α"),
						List.of("α")),
					List.of(A.class,
						aClassName.concat("@α,β"),
						List.of("β", "α")),
					List.of(Z.class, String.format(
						"%1$s@ψ:%2$s@β:%1$s@ω:%2$s@α",
							zClassName,
							aClassName),
						List.of("ω", "ψ"))),
			"testRightToLeftComponentProcessing",
				Set.of(List.of(String.format("%s:%s:%s",
							ηταMethodsValue,
							aClassName,
							ηταMethodsValue),
						String.format("{%s=%s, %s=[]}",
							ηταMethodsClass,
							"[α, τ, η]",
							aClassName)),
					List.of(String.format("%s:%s:%s",
							aClassName,
							ηταMethodsValue,
							ηταMethodsValue),
						String.format("{%s=%s, %s=[]}",
							ηταMethodsClass,
							"[α, τ, η]",
							aClassName))));
			/* See java.util.Abstract{Collection,Map}#toString(). */
	}

	static class A implements Testable { }
	static class Z implements Testable { }
}
