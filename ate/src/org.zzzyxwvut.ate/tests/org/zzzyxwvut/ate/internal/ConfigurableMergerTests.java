package org.zzzyxwvut.ate.internal;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Configurable.Prefixable;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.TestClassContext.ConfigurableMerger;

public class ConfigurableMergerTests implements Testable
{
	private static final Configurable PRIMARY = newConfigurable(
		ExecutionPolicy.SEQUENTIAL,
		Set.of(new DefaultPrefix("test")),
		new PrintStream(OutputStream.nullOutputStream()),
		Result.resultor()
			.apply(Result.renderer()
				.apply(Map.of(OtherResult.class,
					OtherResult.RENDITION_PLAIN,
					TestResult.class,
					TestResult.RENDITION_PLAIN))));
	private static final Configurable SECONDARY = newConfigurable(
		ExecutionPolicy.CONCURRENT,
		Set.of(new DefaultPrefix("assert")),
		System.out,
		Result.resultor()
			.apply(Result.renderer()
				.apply(Map.of(OtherResult.class,
					OtherResult.RENDITION_REVERSE_VIDEO,
					TestResult.class,
					TestResult.RENDITION_REVERSE_VIDEO))));
	private static final boolean PLAIN_OUTPUT = true;

	private static Configurable newConfigurable(
					ExecutionPolicy executionPolicy,
					Set<? extends Prefixable> prefixables,
					PrintStream printStream,
					Reportable reportable)
	{
		return Configurable
			.newBuilder()
			.executionPolicy(executionPolicy)
			.prefixables(prefixables)
			.printStream(printStream)
			.reportable(reportable)
			.build();
	}

	private static boolean areSame(Configurable left, Configurable right)
	{
		return Stream.<Function<Configurable, Object>>of(
					Configurable::executionPolicy,
					Configurable::prefixables,
					Configurable::printStream,
					Configurable::reportable)
			.allMatch(Support.<Configurable,
					Function<Configurable,
					Predicate<Function<Configurable,
							Object>>>>partialApply(
					l -> r -> accessor -> accessor.apply(l)
						== accessor.apply(r),
					left)
				.apply(right));
	}

	public void testAcceptingDefaultConfiguration()
	{
		final Configurable merged = new ConfigurableMerger(
								!PLAIN_OUTPUT)
			.merge(Configurable.newBuilder().build(),
				Configurable.newBuilder().build());
		assert areSame(ConfigurableMerger.BASE_CONFIGURABLE_MULTI,
								merged);
	}

	public void testAcceptingPrimaryConfiguration()
	{
		final Instantiable<ConfigurableMergerTests> primary =
								Instantiable
				.<ConfigurableMergerTests>newBuilder(
					Set.of(ConfigurableMergerTests::new))
			.executionPolicy(PRIMARY.executionPolicy())
			.prefixables(PRIMARY.prefixables())
			.printStream(PRIMARY.printStream())
			.reportable(PRIMARY.reportable())
			.build();
		final Configurable merged = new ConfigurableMerger(
								PLAIN_OUTPUT)
			.merge(primary, Configurable.newBuilder().build());
		assert areSame(primary, merged);
	}

	public void testAcceptingSecondaryConfiguration()
	{
		final Configurable secondary = newConfigurable(
						SECONDARY.executionPolicy(),
						SECONDARY.prefixables(),
						SECONDARY.printStream(),
						SECONDARY.reportable());
		final Configurable merged = new ConfigurableMerger(
								!PLAIN_OUTPUT)
			.merge(Configurable.newBuilder().build(), secondary);
		assert areSame(secondary, merged);
	}

	public void testAcceptingMixedConfiguration()
	{
		final Instantiable<ConfigurableMergerTests> primary =
								Instantiable
				.<ConfigurableMergerTests>newBuilder(
					Set.of(ConfigurableMergerTests::new))
			.executionPolicy(PRIMARY.executionPolicy())
			.prefixables(PRIMARY.prefixables())
			.build();
		final Configurable secondary = Configurable.newBuilder()
			.printStream(SECONDARY.printStream())
			.reportable(SECONDARY.reportable())
			.build();
		final Configurable mixed = newConfigurable(
						primary.executionPolicy(),
						primary.prefixables(),
						secondary.printStream(),
						secondary.reportable());
		final Configurable merged = new ConfigurableMerger(
								!PLAIN_OUTPUT)
			.merge(primary, secondary);
		assert areSame(mixed, merged);
	}
}
