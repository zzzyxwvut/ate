package org.zzzyxwvut.ate.internal;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;

/*
 * This class serves to usher in a speculative set of requested-for-execution
 * test method names for a requested test class after a name filter has been
 * applied to a class and its methods (see org.zzzyxwvut.ate.Tester#PROPERTY_SOME)
 * so that method-related bearings for meta-tests can be taken.
 *
 * Note that an instance of this class should lag behind a Tester's instance in
 * the latter's context so that this instance would acquire mutable properties
 * set by either that Tester's instance or, if introduced later, by a racey
 * Tester's instance from elsewhere (see tester.ate.AteTester).
 */
public final class ClassMethodNameOracle
{
	private final boolean isRequestedClass;
	private final List<Pattern> requestedMethods;

	private ClassMethodNameOracle(boolean isRequestedClass,
					List<Pattern> requestedMethods)
	{
		this.isRequestedClass = isRequestedClass;
		this.requestedMethods = requestedMethods;
	}

	public boolean isRequestedClass()	{ return isRequestedClass; }

	public boolean isRequestedMethod(String methodName)
	{
		Objects.requireNonNull(methodName, "methodName");
		return (isRequestedClass
				&& (requestedMethods.isEmpty()
							|| requestedMethods
			.stream()
			.anyMatch(Support.<String, Predicate<Pattern>>
								partialApply(
					methodName_ -> pattern -> pattern
						.matcher(methodName_)
						.lookingAt(),
					methodName))));
	}

	@Override
	public String toString()
	{
		return requestedMethods.toString();
	}

	public static ClassMethodNameOracle newInstance(
					Class<? extends Testable> testClass)
	{
		Objects.requireNonNull(testClass, "testClass");
		final ClassMethodNameFilter filter = new ClassMethodNameFilter(
				System.getProperty(Tester.PROPERTY_SOME),
				System.getProperty(Tester.PROPERTY_TOKENS));
		return (filter.useDefaultSelection())
			? new ClassMethodNameOracle(true, List.of())
			: new ClassMethodNameOracle(
					filter.isRequestedClass(testClass),
					filter.requestedMethods(testClass)
						.orElseGet(List::of));
	}

	public static Function<String, IntUnaryOperator>
						methodNameMatchingCounter(
					ClassMethodNameOracle oracle)
	{
		Objects.requireNonNull(oracle, "oracle");
		return Support.<ClassMethodNameOracle,
					Function<String, IntUnaryOperator>>
								partialApply(
			oracle_ -> methodName -> amount ->
					(oracle_.isRequestedMethod(methodName))
				? amount
				: 0,
			oracle);
	}
}
