package org.zzzyxwvut.ate.internal;

import java.util.ArrayList;
import java.util.List;

import org.zzzyxwvut.ate.Testable;

public class InterfaceHierarchyWalkerTests implements Testable
{
	private static void compareHierarchies(List<Class<?>> expected,
							Class<?> sourceClass)
	{
		final List<Class<?>> obtained = new ArrayList<>(
			new TestClassContext.InterfaceHierarchyWalker(
					sourceClass,
					TestClassContext.TYPE_COMPARATOR)
				.types());
		assert expected.equals(obtained) : String.format("%n%s !=%n%s",
							expected, obtained);
	}

	private interface Distinct extends A, BB, CCCC, D { }
	private interface A { }
	private interface B { }
	private interface BB extends B { }
	private interface C { }
	private interface CC extends C { }
	private interface CCC extends CC { }
	private interface CCCC extends CCC { }
	private interface D { }

	//		     Distinct
	//		   -----------
	//		  /   |   |   \
	//		 A   BB  CCCC  D
	//		      |   |
	//		      B  CCC
	//			  |
	//			  CC
	//			  |
	//			  C
	public void testDistinctHierarchyWalking()
	{
		compareHierarchies(List.of(
				A.class,
				B.class, BB.class,
				C.class, CC.class, CCC.class, CCCC.class,
				D.class),
			Distinct.class);
	}

	private interface Mixed extends ZZZZ, ZZZ, ZZ, Z { }
	private interface Z { }
	private interface ZZ extends Z { }
	private interface ZZZ extends ZZ { }
	private interface ZZZZ extends ZZZ { }

	//		      Mixed
	//		   -----------
	//		  /   |   |   \
	//		ZZZZ ZZZ  ZZ   Z
	//		 |    |   |
	//		ZZZ   ZZ  Z
	//		 |    |
	//		 ZZ   Z
	//		 |
	//		 Z
	public void testMixedHierarchyWalking()
	{
		compareHierarchies(List.of(
				Z.class, ZZ.class, ZZZ.class, ZZZZ.class),
			Mixed.class);
	}

	private interface Diamond extends XZZ, YZZ { }
	private interface XZZ extends ZZ { }
	private interface YZZ extends ZZ { }

	//		     Diamond
	//		   -----------
	//		      |   |
	//		     XZZ YZZ
	//		      |   |
	//		      ZZ  ZZ
	//		      |   |
	//		      Z   Z
	public void testDiamondHierarchyWalking()
	{
		compareHierarchies(List.of(
				Z.class, ZZ.class, XZZ.class, YZZ.class),
			Diamond.class);
	}

	private interface Independent { }

	public void testWantOfWalking()
	{
		compareHierarchies(List.of(), Independent.class);
	}
}
