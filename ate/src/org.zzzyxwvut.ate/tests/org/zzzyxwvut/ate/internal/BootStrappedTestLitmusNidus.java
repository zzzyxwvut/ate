package org.zzzyxwvut.ate.internal;

import static org.zzzyxwvut.ate.internal.ClassMethodNameOracle.methodNameMatchingCounter;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;

import tester.ate.AteTester;
import tester.ate.DeferredAssertionError;
import tester.ate.LifeCycleMethodResultRegistry.Reporters;

/*
 * The following tests serve as a gentle reminder of showing that _hierarchies_
 * of test type members of any org.zzzyxwvut.ate package are special and must
 * claim assignability to org.zzzyxwvut.ate.Testable for every ascending,
 * contributing type (life cycle methods, test methods).
 *
 * See TestClassContext#traversePublicStaticMethods(Set, Class).
 */
public class BootStrappedTestLitmusNidus
{
	public static class ClientBaseTests
	{
		public static void setUpAll(Void stub)
		{
			throw new DeferredAssertionError(
						new AssertionError());
		}

		public void test0(Queue<ClientBaseTests> instances)
		{
			throw new AssertionError();
		}

		public void test1(Queue<ClientBaseTests> instances)
		{
			throw new AssertionError();
		}
	}

	public static class ClientTests extends ClientBaseTests
							implements Testable
	{
		public void test2(Queue<ClientBaseTests> instances)
		{
			instances.offer(this);
		}

		public void test3(Queue<ClientBaseTests> instances)
		{
			instances.offer(this);
		}

		public static void setUpAll(Queue<ClientBaseTests> instances)
		{
			instances.clear();
		}

		public static void tearDownBatch(int expectedSize,
					Queue<ClientBaseTests> instances)
		{
			try {
				assert expectedSize == instances.size();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		public static Instantiable<ClientTests> instantiable()
		{
			return Instantiable.<ClientTests>newBuilder(
						Set.of(ClientTests::new))
				.executionPolicy(ExecutionPolicy.SEQUENTIAL)
				.arguments(arguments())
				.reportable(AteTester.TEST_REPORTER
					.apply(Reportable.newBuilder()
						.otherResultReporter(
							Reporters.otherResultReporter()
								.apply(OtherResult
									.RENDITION_REVERSE_VIDEO))
						.testResultReporter(
							Reporters.testResultReporter()
								.apply(TestResult
									.RENDITION_RED_YELLOW_GREEN))

						/* Against ClientBaseTests. */
						.setUpBatchResultReporter(
							Reporters.deferredSetUpBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.tearDownBatchResultReporter(
							Reporters.deferredTearDownBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.build()))
				.build();
		}

		private static Map<String, Collection<List<?>>> arguments()
		{
			final Function<String, IntUnaryOperator> nameCounter =
						methodNameMatchingCounter(
					ClassMethodNameOracle.newInstance(
							ClientTests.class));
			final int instanceCount = nameCounter
							.apply("test2")
							.applyAsInt(1)
						+ nameCounter
							.apply("test3")
							.applyAsInt(1);
			final Queue<ClientBaseTests> instances =
							new ArrayDeque<>();
			final Collection<List<?>> commonArgs = List.of(
							List.of(instances));
			return Map.of("test0", commonArgs,
					"test1", commonArgs,
					"test2", commonArgs,
					"test3", commonArgs,
					"tearDownBatch",
						List.of(List.of(instanceCount,
								instances)),
					"setUpAll", commonArgs);
		}
	}

	public static class PatronBaseTests implements Testable
	{
		public static void setUpAll(Queue<PatronBaseTests> instances)
		{
			instances.clear();
		}

		public void test0(Queue<PatronBaseTests> instances)
		{
			instances.offer(this);
		}

		public void test1(Queue<PatronBaseTests> instances)
		{
			instances.offer(this);
		}
	}

	public static class PatronTests extends PatronBaseTests
							implements Testable
	{
		public void test2(Queue<PatronBaseTests> instances)
		{
			instances.offer(this);
		}

		public void test3(Queue<PatronBaseTests> instances)
		{
			instances.offer(this);
		}

		public static void setUpBatch(
					Queue<PatronBaseTests> instances)
		{
			try {
				assert instances.isEmpty();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		public static void tearDownBatch(int expectedSize,
					Queue<PatronBaseTests> instances)
		{
			try {
				assert expectedSize == instances.size();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		public static Instantiable<PatronTests> instantiable()
		{
			return Instantiable.<PatronTests>newBuilder(
						Set.of(PatronTests::new))
				.executionPolicy(ExecutionPolicy.SEQUENTIAL)
				.arguments(arguments())
				.reportable(AteTester.TEST_REPORTER
					.apply(Reportable.newBuilder()
						.otherResultReporter(
							Reporters.otherResultReporter()
								.apply(OtherResult
									.RENDITION_REVERSE_VIDEO))
						.testResultReporter(
							Reporters.testResultReporter()
								.apply(TestResult
									.RENDITION_RED_YELLOW_GREEN))
						.setUpBatchResultReporter(
							Reporters.deferredSetUpBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.tearDownBatchResultReporter(
							Reporters.deferredTearDownBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.build()))
				.build();
		}

		private static Map<String, Collection<List<?>>> arguments()
		{
			final Function<String, IntUnaryOperator> nameCounter =
						methodNameMatchingCounter(
					ClassMethodNameOracle.newInstance(
							PatronTests.class));
			final int instanceCount = nameCounter
							.apply("test0")
							.applyAsInt(1)
						+ nameCounter
							.apply("test1")
							.applyAsInt(1)
						+ nameCounter
							.apply("test2")
							.applyAsInt(1)
						+ nameCounter
							.apply("test3")
							.applyAsInt(1);
			final Queue<PatronBaseTests> instances =
							new ArrayDeque<>();
			final Collection<List<?>> commonArgs = List.of(
							List.of(instances));
			return Map.of("test0", commonArgs,
					"test1", commonArgs,
					"test2", commonArgs,
					"test3", commonArgs,
					"setUpBatch", commonArgs,
					"tearDownBatch",
						List.of(List.of(instanceCount,
								instances)),
					"setUpAll", commonArgs);
		}
	}
}
