package tester.ate;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.support.TestablePath;

import tester.ate.misc.ate.OffsetTestablePathTests;

class TestablePathTests implements Testable
{
	private static final Path REPORTS_PATH = new TestablePath(
						TestablePathTests.class,
						"tests")
		.createSiblingDirectories("reports")
		.orElseThrow(() -> new IllegalStateException(
				"Failed to create a \"reports\" directory"));
	private static final Path JDIRMAKER_PATH = REPORTS_PATH
		.resolve("jdirmaker.txt");
	private static final PrintStream STREAM;

	static {
		try {
			final OutputStream os = Files.newOutputStream(
					JDIRMAKER_PATH,
					StandardOpenOption.TRUNCATE_EXISTING,
					StandardOpenOption.CREATE,
					StandardOpenOption.WRITE);
			final OutputStream bos = new BufferedOutputStream(os);
			STREAM = new PrintStream(bos);
		} catch (final IOException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	private static final TestablePathTests INSTANCE =
						new TestablePathTests();

	private static void cleanUp(Path tmpDir)
	{
		try {
			try (Stream<Path> pathStream = Files.walk(tmpDir, 1)) {
				pathStream.filter(Files::isRegularFile)
						.forEach(path -> {
					try {
						Files.deleteIfExists(path);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				});
			}

			Files.deleteIfExists(tmpDir);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public void testReceivingNoStreamError(Status status)
	{
		assert !status.printStream().checkError();
	}

	private static Path toPath(Class<?> topClass)
	{
		return Path.of(topClass.getResource(topClass.getSimpleName()
					.concat(".class"))
				.getPath())
			.normalize();
	}

	public void testFindingDirectoryOffsetByZeroMatches() throws IOException
	{
		final Path expected = toPath(getClass())
			.getParent();
		final Path obtained = new TestablePath(getClass(), "ate")
			.parentDirectoryPath()
			.orElseGet(() -> Path.of("/dev/null"));
		assert Files.isSameFile(expected, obtained);
	}

	public void testFindingDirectoryOffsetByOneMatch() throws IOException
	{
		final Path expected = toPath(getClass())
			.getParent();
		final Path obtained = new TestablePath(
				OffsetTestablePathTests.class, "ate", 1)
			.parentDirectoryPath()
			.orElseGet(() -> Path.of("/dev/null"));
		assert Files.isSameFile(expected, obtained);
	}

	public void testCreatingEmptyNamedDirectory()
	{
		Optional<Path> emptyPath = Optional.empty();

		try {
			emptyPath = new TestablePath(TestablePathTests.class, "")
				.createSiblingDirectories("");
		} catch (final IllegalArgumentException expected) {
		} finally {
			emptyPath.ifPresent(TestablePathTests::cleanUp);
		}

		assert emptyPath.isEmpty() : "Created an \"\" named directory";
	}

	public static void tearDownAll(Status status)
	{
		try {
			status.printStream().flush();
			Files.copy(JDIRMAKER_PATH, System.err);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		} finally {
			cleanUp(REPORTS_PATH);
			status.printStream().close();
		} /* Safe, TearDownAllResult#summary(String) is empty. */
	}

	public static Instantiable<TestablePathTests> instantiable()
	{
		return Instantiable.<TestablePathTests>newBuilder(Set.of(
							() -> INSTANCE))
			.executionPolicy(ExecutionPolicy.CONCURRENT)
			.prefixables(Set.of(new DefaultPrefix("test")))
			.printStream(STREAM)
			.reportable(AteTester.TEST_REPORTER
				.apply(Result.resultor()
					.apply(Result.renderer()
						.apply(Map.of(
							OtherResult.class,
							OtherResult.RENDITION_REVERSE_VIDEO,
							TestResult.class,
							TestResult.RENDITION_RED_YELLOW_GREEN)))))
			.build();
	}
}
