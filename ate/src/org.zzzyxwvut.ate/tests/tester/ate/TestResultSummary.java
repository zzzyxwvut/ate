package tester.ate;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.Support;

class TestResultSummary
{
	/* See ECMA-48 CSI sequences in console_codes(4). */

	static final String OTHER_RENDITION_PLAIN =
			"%n%s:%nNon-matching prefix: %s%n";
	static final String OTHER_RENDITION_REVERSE_VIDEO =
			"%n\033[7m%s\033[0m"
			+ "%nNon-matching prefix: \033[7m%s\033[0m%n";

	static final String TEST_RENDITION_PLAIN =
			"%n%s:%nPASSED: %d, FAILED: %d%n";
	static final String TEST_RENDITION_REVERSE_VIDEO =
			"%n\033[7m%s\033[0m:"
			+ "%n\033[7mPASSED: %d\033[0m,"
			+ " \033[7mFAILED: %d\033[0m%n";

	private static Function<PrintStream,
				BiFunction<TestResultTally,
					Map.Entry<String,
						Map<MethodKind, List<Result>>>,
								TestResultTally>>
							testResultSummariser()
	{
		return stream -> (tally, entry) -> {
			entry.getValue()
				.getOrDefault(MethodKind.OTHER, List.of())
				.forEach(Support.<PrintStream,
						Function<String,
						Consumer<Result>>>partialApply(
					stream_ -> className -> result ->
									stream_
						.format(OTHER_RENDITION_PLAIN,
								className,
								result),
					stream)
						.apply(tally.className));
			return entry
				.getValue()
				.getOrDefault(MethodKind.TEST, List.of())
				.stream()
				.map(TestResult.class::cast)
				.reduce(tally,
					TestResultTally::successor,
					Support.uoeThrower());
		};
	}

	static void report(PrintStream printStream,
			String classNameSuccessFailureTemplet,
			Map<Class<? extends Testable>,
				Map<String,
					Map<MethodKind, List<Result>>>> summary)
	{
		Objects.requireNonNull(printStream, "printStream");
		Objects.requireNonNull(classNameSuccessFailureTemplet,
					"classNameSuccessFailureTemplet");
		Objects.requireNonNull(summary, "summary");
		summary.entrySet()
			.forEach(Support.<PrintStream,
					Function<String,
					Consumer<Map.Entry<Class<? extends Testable>,
						Map<String,
						Map<MethodKind, List<Result>>>>>>>
								partialApply(
				stream -> templet -> entry -> entry
					.getValue()
					.entrySet()
					.stream()
					.reduce(new TestResultTally(entry
							.getKey()
							.getName()),
						testResultSummariser()
							.apply(stream),
						Support.uoeThrower())
					.reportIfAny(stream, templet),
				printStream)
					.apply(classNameSuccessFailureTemplet));
	}

	private static class TestResultTally
	{
		final String className;

		private int success;
		private int failure;

		TestResultTally(String className)
		{
			this.className = className;
		}

		TestResultTally successor(TestResult testResult)
		{
			success += testResult.successTotal();
			failure += testResult.failureTotal();
			return this;
		}

		void reportIfAny(PrintStream stream,
				String classNameSuccessFailureTemplet)
		{
			/*
			 * Distinguish an initial state from a successory one,
			 * i.e. honour OtherResult.
			 */
			if (success > 0 || failure > 0)
				stream.format(classNameSuccessFailureTemplet,
								className,
								success,
								failure);
		}
	}
}
