package tester.ate;

import java.io.Closeable;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.support.TestablePath;

class ПотоковыеПробы implements Testable
{
	static boolean success = false;

	private static final ПотоковыеПробы INSTANCE = new ПотоковыеПробы();

	private static Function<PrintStream,
				Function<String,
				Function<String,
				Supplier<PrintStream>>>> streamer()
	{
		return stream -> head -> tail -> () -> {
			stream.format("%s: %s%n", head, tail);
			stream.flush();
			return stream;
		};
	}

	public void testStreamingToFiles(Set<Supplier<PrintStream>> streamers)
	{
		streamers.forEach(Supplier::get);
	}

	private static Function<PrintStream,
				Function<Pipe,
				Function<Queue<String>, Callable<String>>>>
								writer()
	{
		return outStream -> pipe -> messages -> () -> {
			final String out = messages.poll();
			pipe.write(out);
			outStream.println(out);
			return null;
		};
	}

	private static Function<PrintStream,
				Function<Pipe, Callable<String>>> reader()
	{
		return inStream -> pipe -> () -> {
			final String in = pipe.read();
			inStream.println(in);
			return in;
		};
	}

	public void testStreamingInPipe(List<String> expected,
						ExecutorService executors,
						PrintStream outStream,
						PrintStream inStream)
						throws InterruptedException,
							IOException
	{
		try (final Pipe pipe = new Pipe()) {
			final List<Callable<String>> tasks = new ArrayList<>(
							expected.size() << 1);
			tasks.addAll(Collections.nCopies(expected.size(),
						writer().apply(outStream)
							.apply(pipe)
							.apply(
				new ConcurrentLinkedDeque<>(expected))));
			tasks.addAll(Collections.nCopies(expected.size(),
						reader().apply(inStream)
							.apply(pipe)));
			Collections.shuffle(tasks);
			assert expected.containsAll(executors.invokeAll(tasks)
				.stream()
				.parallel()
				.map(result -> {
					try {
						return result.get();
					} catch (final ExecutionException e) {
						throw new RuntimeException(e);
					} catch (final InterruptedException e) {
						Thread.currentThread()
							.interrupt();
					}

					return null;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toUnmodifiableList()));
		}
	}

	public static void tearDownAll(ExecutorService executors,
					Collection<PrintStream> streams)
	{
		streams.forEach(PrintStream::close);
		executors.shutdown();
	}

	public static Instantiable<ПотоковыеПробы> instantiable()
	{
		return Instantiable.<ПотоковыеПробы>newBuilder(Set.of(
							() -> INSTANCE))
			.arguments(arguments())
			.executionPolicy(ExecutionPolicy.CONCURRENT)
			.prefixables(Set.of(new DefaultPrefix("test")))
			.printStream(new PrintStream((System.err)) { })
			.build();
	}

	private static Function<Path, Runnable> unlinkingPrinter()
	{
		return path -> () -> {
			try {
				Files.lines(path, StandardCharsets.UTF_8)
					.forEach(line ->
						System.err.println(line));
				Files.deleteIfExists(path);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		};
	}

	private static PrintStream tempFilePrintStream(String fileName)
	{
		try {
			final Path path = Files.createTempFile(fileName, null);
			Runtime.getRuntime()
				.addShutdownHook(new Thread(unlinkingPrinter()
					.apply(path)));
			return new PrintStream(path.toFile());
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private static Map<String, PrintStream> fileStreams(String outStream,
							String inStream)
	{
		final Path miscPath = new TestablePath(ПотоковыеПробы.class,
								"tests")
			.createSiblingDirectories("misc")
			.orElseThrow(() -> new IllegalStateException(
				"Failed to create a \"misc\" directory"));

		try {
			return Map.of(outStream, new PrintStream(miscPath
						.resolve("out.txt")
						.toFile(),
					StandardCharsets.UTF_8),
				inStream, new PrintStream(miscPath
						.resolve("in.txt")
						.toFile(),
					StandardCharsets.UTF_8));
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private static Map<String, Collection<List<?>>> arguments()
	{
		final String leftSeries0 = "zeroth first second third"
			+ " fourth fifth sixth seventh eighth ninth"
			+ " tenth eleventh twelfth thirteenth"
			+ " fourteenth fifteenth sixteenth"
			+ " seventeenth eighteenth nineteenth"
			+ " twentieth twenty-first twenty-second"
			+ " twenty-third twenty-fourth twenty-fifth"
			+ " twenty-sixth twenty-seventh twenty-eighth"
			+ " twenty-ninth thirtieth thirty-first"
			+ " thirty-second thirty-third thirty-fourth"
			+ " thirty-fifth thirty-sixth thirty-seventh"
			+ " thirty-eighth thirty-ninth fortieth"
			+ " forty-first forty-second forty-third"
			+ " forty-fourth forty-fifth forty-sixth"
			+ " forty-seventh forty-eighth"
			+ " forty-ninth ...";			// 554
		final String leftSeries1 = "... fiftieth fifty-first"
			+ " fifty-second fifty-third fifty-fourth"
			+ " fifty-fifth fifty-sixth fifty-seventh"
			+ " fifty-eighth fifty-ninth sixtieth"
			+ " sixty-first sixty-second sixty-third"
			+ " sixty-fourth sixty-fifth sixty-sixth"
			+ " sixty-seventh sixty-eighth"
			+ " sixty-ninth";			// 247
		final String rightSeries0 = "sixty-ninth sixty-eighth"
			+ " sixty-seventh sixty-sixth sixty-fifth"
			+ " sixty-fourth sixty-third sixty-second"
			+ " sixty-first sixtieth fifty-ninth"
			+ " fifty-eighth fifty-seventh fifty-sixth"
			+ " fifty-fifth ...";			// 188
		final String rightSeries1 = "... fifty-fourth"
			+ " fifty-third fifty-second fifty-first"
			+ " fiftieth forty-ninth forty-eighth"
			+ " forty-seventh forty-sixth forty-fifth"
			+ " forty-fourth forty-third forty-second"
			+ " forty-first fortieth thirty-ninth"
			+ " thirty-eighth thirty-seventh thirty-sixth"
			+ " thirty-fifth thirty-fourth thirty-third"
			+ " thirty-second thirty-first thirtieth"
			+ " twenty-ninth twenty-eighth twenty-seventh"
			+ " twenty-sixth twenty-fifth twenty-fourth"
			+ " twenty-third twenty-second twenty-first"
			+ " twentieth nineteenth eighteenth seventeenth"
			+ " sixteenth fifteenth fourteenth thirteenth"
			+ " twelfth eleventh tenth ninth eighth seventh"
			+ " sixth fifth fourth third second first"
			+ " zeroth";				// 613
		final List<String> expectedInPipe = Arrays.asList("",
						leftSeries0, leftSeries1,
						rightSeries0, rightSeries1);
		Collections.shuffle(expectedInPipe);
		final ExecutorService executors = Executors
			.newWorkStealingPool(expectedInPipe.size() + 1);
		final Map<String, PrintStream> streams = fileStreams("out",
									"in");
		return Map.of("testStreamingInPipe",
				List.of(List.of(expectedInPipe,
					executors,
					streams.get("out"),
					streams.get("in"))),
			"testStreamingToFiles",
				List.of(List.of(Set.of(
					streamer()
						.apply(tempFilePrintStream(
								"hello"))
						.apply("stream0")
						.apply("hello"),
					streamer()
						.apply(tempFilePrintStream(
								"world"))
						.apply("stream1")
						.apply("world")))),
			"tearDownAll", List.of(List.of(executors,
						streams.values())));
	}

	static class Pipe implements Closeable
	{
		private static final int PIPE_SIZE = 256;

		private final Object readLock = new Object();
		private final Object writeLock = new Object();
		private final PipedOutputStream pipeOutput =
						new PipedOutputStream();
		private final PipedInputStream pipeInput;
		private final Charset charset;

		Pipe(Charset charset)
		{
			this.charset = Objects.requireNonNull(charset,
								"charset");

			try {
				pipeInput = new PipedInputStream(pipeOutput,
								PIPE_SIZE);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		}

		Pipe()	{ this(StandardCharsets.UTF_8); }

		private static byte[] toBytes(int value)
		{
			final byte[] out = new byte[4];		// Integer.BYTES
			out[0] = (byte) (value >> 0x18);	// 256 ** 3 > 0
			out[1] = (byte) (value >> 0x10);	// 256 ** 2 > 0
			out[2] = (byte) (value >> 0x08);	// 256 ** 1 > 0
			out[3] = (byte) value;			// 256 ** 0 > 0
			return out;
		}

		private static int toInt(byte[] values)
		{
			if (values.length != 4)			// Integer.BYTES
				throw new IllegalArgumentException(
							"4 bytes expected");

			return (((values[0] & 0xff) << 0x18)
					| ((values[1] & 0xff) << 0x10)
					| ((values[2] & 0xff) << 0x08)
					| (values[3] & 0xff));
		}

		void write(String message)
		{
			final byte[] out = message.getBytes(charset);

			try {
				synchronized (writeLock) {
					pipeOutput.write(toBytes(out.length));
					pipeOutput.write(out);
				}
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		}

		String read()
		{
			final byte[] in;

			try {
				synchronized (readLock) {
					int length = toInt(pipeInput
							.readNBytes(4));
					in = new byte[length];
					int offset = 0;

					do {
						final int total = pipeInput
							.read(in,
								offset,
								length);
						offset += total;
						length -= total;
					} while (length != 0);
				}
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}

			return new String(in, charset);
		}

		@Override
		public void close() throws IOException
		{
			IOException caughtIn = null;
			IOException caughtOut = null;

			try {
				pipeInput.close();
			} catch (final IOException e) {
				caughtIn = e;
			}

			try {
				pipeOutput.close();
			} catch (final IOException e) {
				caughtOut = e;
			}

			if (caughtIn != null && caughtOut != null) {
				caughtOut.addSuppressed(caughtIn);
				throw caughtOut;
			} else if (caughtIn != null) {
				throw caughtIn;
			} else if (caughtOut != null) {
				throw caughtOut;
			}
		}
	}

	public static void main(String... args)
	{
		success = false;
		success = Tester.newBuilder(Set.of(ПотоковыеПробы.class))
			.configurable(Configurable.newBuilder()
				.reportable(AteTester.TEST_REPORTER
					.apply(Result.resultor()
						.apply(Result.renderer()
							.apply(Map.of(
								OtherResult.class,
								OtherResult.RENDITION_PLAIN,
								TestResult.class,
								TestResult.RENDITION_PLAIN)))))
				.build())
			.verifyAssertion()
			.build()
			.runAndReport();
	}
}
