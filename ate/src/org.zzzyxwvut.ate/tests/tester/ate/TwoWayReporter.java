package tester.ate;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.SetUpAllResult;
import org.zzzyxwvut.ate.Result.SetUpBatchResult;
import org.zzzyxwvut.ate.Result.TearDownAllResult;
import org.zzzyxwvut.ate.Result.TearDownBatchResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.Support;

/*
 * (Clients may entertain an idea of using a derivative form of
 * org.zzzyxwvut.ate.configuration.SummarisingRecorder throughout
 * to dispense with the necessity of collecting results piecemeal
 * and merging prefix-related ones afterwards.)
 *
 * This class manages four reporters: a test pair and a meta-test
 * pair for an arbitrary and a file-bound ones.
 *
 * All generated results-data files shall be named after
 * the fully.qualified.Test.number fashion (so as to amass data
 * in distinct files for each requested test-method-name prefix
 * of a test class) and their contents be laid out as follows:
 *
 * +--------------------------------------------------------------------+
 * |testMethodNamePrefix<SPACE>successCount<SPACE>failureCount<NL>	|
 * |[fully.qualified.ExceptionType[<SPACE>Oops!^^See #1.^^trace...]]<NL>|
 * |[fully.qualified.ExceptionType[<SPACE>Oops!^^See #2.^^trace...]]<NL>|
 * |...									|
 * +--------------------------------------------------------------------+
 *
 * For every failure instance there shall be a separate line with
 * an error type and a detail message, if any, and some top stack
 * frame traces represented as strings.
 * All embedded new-line characters (0x0A) shall be replaced with
 * record-separator characters (0x1E).
 */
class TwoWayReporter
{
	private static final Function<ReportWriter, UnaryOperator<Reportable>>
								BASE_REPORTER =
				Support.<Map<Class<? extends Result>,
						Function<ReportWriter,
						BiConsumer<Class<? extends Testable>,
								Result>>>,
					Function<ReportWriter,
					UnaryOperator<Reportable>>>
								partialApply(
		strategy -> writer -> resultor -> stream ->
						testClass -> result -> {
			try {
				strategy.get(result.getClass())
					.apply(writer)
					.accept(testClass, result);
			} finally {
				resultor.apply(stream)
					.apply(testClass)
					.accept(result);
			}
		},
		Map.of(SetUpAllResult.class,
			writer -> (testClass, result) -> { },
			TearDownAllResult.class,
			writer -> (testClass, result) -> { },
			SetUpBatchResult.class,
			writer -> (testClass, result) -> { },
			TearDownBatchResult.class,
			writer -> (testClass, result) -> { },
			TestResult.class,
			writer -> (testClass, result) ->
					writer.writeResult(testClass,
							(TestResult) result),
			OtherResult.class,
			writer -> (testClass, result) -> { }));

	private final UnaryOperator<Reportable> metaReporter;
	private final UnaryOperator<Reportable> testReporter;

	TwoWayReporter(Path directoryPath)
	{
		final AtomicInteger id = new AtomicInteger(0);
		metaReporter = BASE_REPORTER
			.apply(new MetaReportWriter(directoryPath, id));
		testReporter = BASE_REPORTER
			.apply(new TestReportWriter(directoryPath, id));
	}

	UnaryOperator<Reportable> metaReporter() { return metaReporter; }

	UnaryOperator<Reportable> testReporter() { return testReporter; }

	private static abstract sealed class ReportWriter
	{
		static final String DELIMITER = " ";
		static final String RECORD_SEPARATOR = "\u001e";

		private final Path directoryPath;
		private final AtomicInteger id;

		ReportWriter(Path directoryPath, AtomicInteger id)
		{
			this.directoryPath = Objects.requireNonNull(
							directoryPath,
							"directoryPath");
			this.id = Objects.requireNonNull(id, "id");
		}

		abstract void doWriteResult(BufferedWriter writer,
							TestResult result)
							throws IOException;

		final void writeResult(Class<? extends Testable> klass,
							TestResult result)
		{
			try (BufferedWriter writer = Files.newBufferedWriter(
					directoryPath
						.resolve(Path.of(
							String.format(
								"%s.%04d",
								klass.getName(),
								id.incrementAndGet()))),
					StandardOpenOption.TRUNCATE_EXISTING,
					StandardOpenOption.CREATE,
					StandardOpenOption.WRITE)) {
				doWriteResult(writer, result);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		}
	}

	private static final class TestReportWriter extends ReportWriter
	{
		TestReportWriter(Path directoryPath, AtomicInteger id)
		{
			super(directoryPath, id);
		}

		private static Function<Throwable, String> errorSummariser()
		{
			return error -> {
				final Throwable cause = Support
					.primaryCause(error)
					.orElse(error);
				final String className = cause
					.getClass()
					.getName();
				final String message =
						Objects.requireNonNullElse(
							cause.getMessage(),
							"");
				return new StringBuilder(className.length()
						+ message.length()
						+ 2
						+ 512)
					.append(className)
					.append(DELIMITER)
					.append((!message.isEmpty())
						? new StringBuilder(
								message.length()
									+ 1)
							.append(message.replace(
								System.lineSeparator(),
								RECORD_SEPARATOR))
							.append(RECORD_SEPARATOR)
							.toString()
						: "")
					.append(Arrays
						.stream(cause.getStackTrace())
						.limit(2L)
						.map(StackTraceElement
								::toString)
						.collect(Collectors.joining(
							RECORD_SEPARATOR))
						.replace(System.lineSeparator(),
							RECORD_SEPARATOR))
					.toString();
			};
		}

		@Override
		void doWriteResult(BufferedWriter writer, TestResult result)
							throws IOException
		{
			writer.write(result.testMethodNamePrefix()
				.orElseThrow(IllegalStateException::new));
			writer.write(DELIMITER);
			writer.write(Integer.toString(result.successTotal()));
			writer.write(DELIMITER);
			writer.write(Integer.toString(result.failureTotal()));
			writer.newLine();
			final String errorSummary = result
				.errors()
				.getOrDefault(MethodKind.TEST, List.of())
				.stream()
				.map(errorSummariser())
				.collect(Collectors.joining(
						System.lineSeparator()));

			if (!errorSummary.isEmpty()) {
				writer.write(errorSummary);
				writer.newLine();
			}
		}
	}

	private static final class MetaReportWriter extends ReportWriter
	{
		MetaReportWriter(Path directoryPath, AtomicInteger id)
		{
			super(directoryPath, id);
		}

		@Override
		void doWriteResult(BufferedWriter writer, TestResult result)
							throws IOException
		{
			writer.write(result.testMethodNamePrefix()
				.orElseThrow(IllegalStateException::new));
			writer.write(DELIMITER);
			writer.write(Integer.toString(result.successTotal()
						+ result.failureTotal()));
			writer.write(DELIMITER);
			writer.write("0");
			writer.newLine();
		}
	}
}
