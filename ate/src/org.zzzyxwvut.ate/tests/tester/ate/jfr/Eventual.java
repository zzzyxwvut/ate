package tester.ate.jfr;

/* This interface re-declares abstract some methods of jdk.jfr.Event. */
public interface Eventual
{
	void commit();

	boolean shouldCommit();

	static Eventual newHeapMemoryUsage(Class<?> typeContext,
							String nameContext)
	{
		/* See the compile-test Ant target manoeuvre. */
		throw new UnsupportedOperationException("Stub");
	}
}
