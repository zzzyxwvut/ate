package tester.ate;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class LifeCycleMethodResultTests implements Testable
{
	static boolean success = false;

	public void passOrFail(Optional<AssertionError> deferredError)
	{
		deferredError.ifPresent(error -> {
			throw error;
		});
	}

	public static Instantiable<LifeCycleMethodResultTests> instantiable()
	{
		final LifeCycleMethodResultRegistry registry =
					AteTester.DEFERRED_RESULT_REGISTRY;
		final boolean noEntries = registry.isBlank();
		return Instantiable.<LifeCycleMethodResultTests>newBuilder(
				Set.of(LifeCycleMethodResultTests::new))
			.arguments(arguments(registry, noEntries))
			.prefixables(Set.of(new DefaultPrefix((noEntries)
							? "skipPrefix"
							: "passOrFail")))
			.reportable(AteTester.TEST_REPORTER
				.apply(Reportable.newBuilder()
					.otherResultReporter(
						Reporters.otherResultReporter()
							.apply(OtherResult
								.RENDITION_REVERSE_VIDEO))
					.testResultReporter(
						Reporters.testResultReporter()
							.apply(TestResult
								.RENDITION_REVERSE_VIDEO))
					.build()))
			.build();
	}

	private static Map<String, Collection<List<?>>> arguments(
				LifeCycleMethodResultRegistry registry,
				boolean noEntries)
	{
		return Map.of("passOrFail", ((noEntries)
			? Stream.of(List.of(Optional.<AssertionError>empty()))
			: registry.traverseEntries())
				.collect(Collectors.toUnmodifiableList()));
	}

	public static void main(String... args)
	{
		success = false;
		success = Tester.newBuilder(Set.of(
					LifeCycleMethodResultTests.class))
			.continueOnFailure()
			.build()
			.runAndReport();
	}
}
