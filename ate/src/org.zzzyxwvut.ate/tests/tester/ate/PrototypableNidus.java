package tester.ate;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class PrototypableNidus
{
	static class BaseSetUpTests implements Testable
	{
		static final ConcurrentHashMap<Testable, LongAdder>
					INSTANCES = new ConcurrentHashMap<>();
		private static final VarHandle VALUE;

		private volatile long value; /* Play with bitwise atomicity. */

		static {
			try {
				VALUE = MethodHandles
					.lookup()
					.findVarHandle(BaseSetUpTests.class,
								"value",
								long.class);
			} catch (final ReflectiveOperationException e) {
				throw new Error(e);
			}
		}

		BaseSetUpTests(long value)	{ this.value = value; }

		static void incrementUsage(Testable instance)
		{
			INSTANCES.computeIfAbsent(instance, k -> new LongAdder())
				.increment();
		}

		public void setUp0()
		{
			incrementUsage(this);
			VALUE.setOpaque(this, 128L);
		}

		public void tearDown0()
		{
			incrementUsage(this);
			VALUE.setOpaque(this, 0L);
		}

		void doTest(long expected, long x)
		{
			incrementUsage(this);
			final long obtained = ((long) VALUE.getOpaque(this)) | x;
			assert (expected == obtained) : String.format(
								"%d != %d",
								expected,
								obtained);
		}
	}

	static class InstanceCountingTests extends BaseSetUpTests
	{
		private static final long SET_UP_COUNT;
		private static final long TEAR_DOWN_COUNT;

		static {
			final Method[] methods = InstanceCountingTests.class
							.getMethods();
			SET_UP_COUNT = count(methods, matcher()
						.apply(Pattern.compile(
							"^setUp(?!All)")));
			TEAR_DOWN_COUNT = count(methods, matcher()
						.apply(Pattern.compile(
							"^tearDown(?!All)")));
		}

		private static long count(Method[] methods,
						Predicate<Method> matcher)
		{
			return Arrays.stream(methods)
				.filter(matcher)
				.count();
		}

		private static Function<Pattern, Predicate<Method>> matcher()
		{
			return pattern -> method -> pattern.matcher(
							method.getName())
				.lookingAt();
		}

		private InstanceCountingTests(long value) { super(value); }

		public static void tearDownAll()
		{
			final long expected = (SET_UP_COUNT
						+ TEAR_DOWN_COUNT
						+ 1L)	/* A prototype. */
				* INSTANCES.size();	/* 5 * (4 * 4 * 2). */
			final long obtained = INSTANCES.reduceValuesToLong(0L,
							LongAdder::longValue,
							0L,
							Long::sum);

			try {
				assert (expected == obtained) : String.format(
								"%d != %d",
								expected,
								obtained);
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			} finally {
				INSTANCES.clear();
			}
		}

		public void setUp1()	{ incrementUsage(this); }
		public void tearDown1()	{ incrementUsage(this); }

		public void test00Value(long expected, long x)
		{
			doTest(expected, x);
		}

		public void test01Value(long expected, long x)
		{
			doTest(expected, x);
		}

		public static Instantiable<InstanceCountingTests> instantiable()
		{
			return Instantiable.<InstanceCountingTests>newBuilder(
									Set.of(
					() -> new InstanceCountingTests(0L),
					() -> new InstanceCountingTests(0L),
					() -> new InstanceCountingTests(0L),
					() -> new InstanceCountingTests(0L)))
				.arguments(Map.of("test00Value",
					Set.of(List.of(129L, 1L),
						List.of(130L, 2L),
						List.of(132L, 4L),
						List.of(136L, 8L)),
					"test01Value",
					Set.of(List.of(144L, 16L),
						List.of(160L, 32L),
						List.of(192L, 64L),
						List.of(128L, 128L))))
				.executionPolicy(ExecutionPolicy.CONCURRENT)
				.prefixables(new LinkedHashSet<>(List.of(
					new DefaultPrefix("test00"),
					new DefaultPrefix("test01"))))
				.reportable(AteTester.TEST_REPORTER
					.apply(Reportable.newBuilder()
						.otherResultReporter(
							Reporters.otherResultReporter()
								.apply(OtherResult
									.RENDITION_REVERSE_VIDEO))
						.testResultReporter(
							Reporters.testResultReporter()
								.apply(TestResult
									.RENDITION_RED_YELLOW_GREEN))
						.tearDownAllResultReporter(
							Reporters.deferredTearDownAllResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.build()))
				.build();
		}
	}
}
