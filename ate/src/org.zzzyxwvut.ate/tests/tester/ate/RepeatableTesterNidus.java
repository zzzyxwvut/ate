package tester.ate;

import static org.zzzyxwvut.ate.internal.ClassMethodNameOracle.methodNameMatchingCounter;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.configuration.SummarisingRecorder;
import org.zzzyxwvut.ate.internal.ClassMethodNameOracle;
import org.zzzyxwvut.ate.internal.Support;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class RepeatableTesterNidus
{
	static boolean success = false;

	interface Identifiable extends Testable
	{
		default void testEntry0(Set<? super Testable> identities)
		{
			identities.add(this);
		}

		default void testEntry1(Set<? super Testable> identities)
		{
			identities.add(this);
		}

		default void testEntry2(Set<? super Testable> identities)
		{
			identities.add(this);
		}

		default void testEntry3(Set<? super Testable> identities)
		{
			identities.add(this);
		}

		static void tearDownAll(Set<? super Testable> identities,
							int expectedSize)
		{
			try {
				assert expectedSize == identities.size() :
							identities.size();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		static <T extends Testable> Instantiable<T> newInstantiable(
					Set<Supplier<? extends T>> testables,
					Set<? super Testable> identities,
					int expectedSize)
		{
			return Instantiable
				.<T>newBuilder(testables)
				.executionPolicy(ExecutionPolicy.SEQUENTIAL)
				.arguments(Map.of(
					"testEntry0", Set.of(List.of(identities)),
					"testEntry1", Set.of(List.of(identities)),
					"testEntry2", Set.of(List.of(identities)),
					"testEntry3", Set.of(List.of(identities)),
					"setUpAll", Set.of(List.of(identities)),
					"tearDownAll", Set.of(List.of(
								identities,
								expectedSize))))
				.build();
		}
	}

	static class SingletonTests implements Identifiable
	{
		private static final SingletonTests INSTANCE =
							new SingletonTests();

		public static Instantiable<SingletonTests> instantiable()
		{
			final Function<String, IntUnaryOperator> nameCounter =
						methodNameMatchingCounter(
					ClassMethodNameOracle.newInstance(
							SingletonTests.class));
			final int count = ((nameCounter
						.apply("testEntry0")
						.applyAsInt(1)
					+ nameCounter
						.apply("testEntry1")
						.applyAsInt(1)
					+ nameCounter
						.apply("testEntry2")
						.applyAsInt(1)
					+ nameCounter
						.apply("testEntry3")
						.applyAsInt(1)) > 0)
				? 1
				: 0;
			return Identifiable.<SingletonTests>newInstantiable(
					Set.of(() -> INSTANCE,
						() -> INSTANCE),
					new HashSet<>(count),
					count);
		}
	} /* -Date.some=.+?RepeatableTesterNidus.SingletonTests@testEntry[01] */

	static class PrototypeTests implements Identifiable
	{
		public static Instantiable<PrototypeTests> instantiable()
		{
			final Function<String, IntUnaryOperator> nameCounter =
						methodNameMatchingCounter(
					ClassMethodNameOracle.newInstance(
							PrototypeTests.class));
			final int count = nameCounter
						.apply("testEntry0")
						.applyAsInt(2)	/* The instances. */
					+ nameCounter
						.apply("testEntry1")
						.applyAsInt(2)
					+ nameCounter
						.apply("testEntry2")
						.applyAsInt(2)
					+ nameCounter
						.apply("testEntry3")
						.applyAsInt(2);
			return Identifiable.<PrototypeTests>newInstantiable(
					Set.of(PrototypeTests::new,
						PrototypeTests::new),
					new HashSet<>(count),
					count);
		}
	} /* -Date.some=.+?RepeatableTesterNidus.PrototypeTests@testEntry[01] */

	private static Reportable threeWayReporter()
	{
		return AteTester.TEST_REPORTER
			.apply(Support.<Reportable, UnaryOperator<Reportable>>
								partialApply(
							summarisingReporter ->
							deferringReporter ->
							stream ->
							testClass ->
							result -> {
							summarisingReporter
					.apply(stream)
					.apply(testClass)
					.accept(result);
							deferringReporter
					.apply(stream)
					.apply(testClass)
					.accept(result);
				},
				SummarisingRecorder
					.recorder()
					.apply(key -> new ArrayDeque<>())
					.apply(AteTester.TALLY))
				.apply(Reportable.newBuilder()
					/*
					 * The reports for MethodKind.OTHER or
					 * MethodKind.TEST shall be managed by
					 * TestResultSummary#testResultSummariser().
					 */

					.tearDownAllResultReporter(
						Reporters.deferredTearDownAllResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))
					.build()));
	}

	public static void main(String... args)
	{
		success = false;
		success = IntStream.range(0,
				ThreadLocalRandom.current().nextInt(2, 5))
			.mapToObj(Support.<Tester, IntFunction<Boolean>>
								partialApply(
				tester -> stub -> tester.runAndReport(),
				Tester.newBuilder(Set.of(
						SingletonTests.class,
						PrototypeTests.class))
					.executionPolicy(ExecutionPolicy
								.SEQUENTIAL)
					.configurable(Configurable.newBuilder()
						.reportable(threeWayReporter())
						.build())
					.continueOnFailure()
					.build()))
			.reduce(true, (left, right) -> left && right);
	}
}
