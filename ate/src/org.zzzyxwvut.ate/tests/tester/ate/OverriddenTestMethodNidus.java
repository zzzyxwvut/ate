package tester.ate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.Support;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class OverriddenTestMethodNidus
{
	static final String TEST_0 = "test0";
	static final String TEST_1 = "test1";
	static final String TEST_00 = "test00";
	static final String TEST_11 = "test11";
	static final String BATCH_UP_0 = "setUpBatch0";
	static final String BATCH_UP_1 = "setUpBatch1";
	static final String BATCH_DOWN_0 = "tearDownBatch0";
	static final String BATCH_DOWN_1 = "tearDownBatch1";
	static final Set<Supplier<? extends FourthDescendant>> SINGLETONS;

	static {
		final int count = ThreadLocalRandom.current().nextInt(2, 5);
		final List<Phase> expectedTests = new ArrayList<>();
		expectedTests.addAll(Collections.nCopies(count, Phase.FIRST));
		expectedTests.addAll(Collections.nCopies(count, Phase.SECOND));

		/*
		 * Note that for a set of some Suppliers that many distinct
		 * references are expected and so a mapping function is used,
		 * whereas Collections#nCopies(int, T) reuses the reference.
		 */
		SINGLETONS = IntStream.range(0, count)
			.mapToObj(Support.<FourthDescendant,
						IntFunction<? extends
							Supplier<? extends
								FourthDescendant>>>
								partialApply(
				testable -> stub -> () -> testable,
				new FourthDescendant(
					List.copyOf(EnumSet.allOf(Phase.class)),
								expectedTests)))
			.collect(Collectors.toUnmodifiableSet());
	}

	static Function<List<Phase>,
			Function<Phase,
			BiPredicate<String, String>>> batchTester()
	{
		return stub0 -> stub1 ->
				(testMethodNamePrefix, currentPrefix) ->
			(testMethodNamePrefix.equals(currentPrefix));
	}

	static Function<List<Phase>,
			Function<Phase,
			BiPredicate<String, String>>> fellowTester()
	{
		return phases -> phase ->
				(testMethodNamePrefix, currentPrefix) ->
			(testMethodNamePrefix.equals(currentPrefix)
					&& !phases.contains(phase));
	}

	static class FirstDescendant
	{
		final List<Phase> expectedCycle;
		final List<Phase> expectedTests;

		FirstDescendant(List<Phase> expectedCycle,
						List<Phase> expectedTests)
		{
			this.expectedCycle = Objects.requireNonNull(
					expectedCycle, "expectedCycle");
			this.expectedTests = Objects.requireNonNull(
					expectedTests, "expectedTests");
		}

		static final void adder(Phase phase,
			String testMethodNamePrefix,
			Function<List<Phase>,
				Function<Phase,
				BiPredicate<String, String>>> tester,
			Function<Function<List<Phase>,
					Function<Phase,
					BiPredicate<String, String>>>,
				Function<Phase,
				BiConsumer<String, String>>> adder,
			Status status)
		{
			adder.apply(tester)
				.apply(phase)
				.accept(testMethodNamePrefix,
					status.testMethodNamePrefix()
						.orElse(""));
		}

		static void assertPrefix(String expected, Status status)
		{
			try {
				assert status.testMethodNamePrefix()
					.orElse("")
					.startsWith(expected) :
						status.testMethodNamePrefix();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		public static void setUpAll(Status status)
		{
			try {
				assert status.testMethodNamePrefix().isEmpty();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		public static void setUpBatch0(Status status, Adder adder)
		{
			adder(Phase.FIRST, TEST_0, fellowTester(), adder,
								status);
			assertPrefix(TEST_0, status);
		}

		public static void setUpBatch0(Status status, Adder adder,
							boolean overloaded)
		{
			adder(Phase.FIRST, TEST_0, fellowTester(), adder,
								status);
			assertPrefix(TEST_0, status);
		}

		public static void setUpBatch1(Status status, Adder adder)
		{
			adder(Phase.FIRST, TEST_1, fellowTester(), adder,
								status);
			assertPrefix(TEST_1, status);
		}

		public static void setUpBatch1(Status status, Adder adder,
							boolean overloaded)
		{
			adder(Phase.FIRST, TEST_1, fellowTester(), adder,
								status);
			assertPrefix(TEST_1, status);
		}

		public static void tearDownBatch0(Status status,
						List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			status.printStream()
				.format("CYCLE_0: %s%nTEST_0: %s%n",
						cyclePhases, testPhases);

			try {
				assertPrefix(TEST_0, status);
			} finally {
				testPhases.removeAll(EnumSet.allOf(Phase.class));
			}
		}

		public static void tearDownBatch1(Status status,
						List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			status.printStream()
				.format("CYCLE_1: %s%nTEST_1: %s%n",
						cyclePhases, testPhases);

			try {
				assertPrefix(TEST_1, status);
			} finally {
				testPhases.removeAll(EnumSet.allOf(Phase.class));
			}
		}

		public void test0(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			throw new AssertionError();
		}

		public void test1(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			throw new AssertionError();
		}

		public final void test00(Status status, List<Phase> phases)
		{
			phases.add(Phase.FIRST);
		}

		public final void test11(Status status, List<Phase> phases)
		{
			phases.add(Phase.FIRST);
		}

		public static void tearDownAll(Status status)
		{
			try {
				assert status.testMethodNamePrefix().isEmpty();
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}
	}

	static class SecondDescendant extends FirstDescendant
	{
		SecondDescendant(List<Phase> expectedCycle,
						List<Phase> expectedTests)
		{
			super(expectedCycle, expectedTests);
		}

		public static void setUpBatch0(Status status, Adder adder)
		{
			adder(Phase.SECOND, TEST_0, batchTester(), adder,
								status);
			assertPrefix(TEST_0, status);
		}

		public static void setUpBatch1(Status status, Adder adder)
		{
			adder(Phase.SECOND, TEST_1, batchTester(), adder,
								status);
			assertPrefix(TEST_1, status);
		}

		public final void test00(Status status, List<Phase> phases,
								boolean none)
		{
			phases.add(Phase.SECOND);
		}

		public final void test11(Status status, List<Phase> phases,
								boolean none)
		{
			phases.add(Phase.SECOND);
		}

		@Override
		public void test0(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			throw new AssertionError();
		}

		@Override
		public void test1(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			throw new AssertionError();
		}
	}

	static class ThirdDescendant extends SecondDescendant
	{
		ThirdDescendant(List<Phase> expectedCycle,
						List<Phase> expectedTests)
		{
			super(expectedCycle, expectedTests);
		}

		public static void setUpBatch0(Status status, Adder adder)
		{
			adder(Phase.THIRD, TEST_0, batchTester(), adder,
								status);
			assertPrefix(TEST_0, status);
		}

		public static void setUpBatch1(Status status, Adder adder)
		{
			adder(Phase.THIRD, TEST_1, batchTester(), adder,
								status);
			assertPrefix(TEST_1, status);
		}
	}

	static class FourthDescendant extends ThirdDescendant
						implements Testable
	{
		FourthDescendant(List<Phase> expectedCycle,
						List<Phase> expectedTests)
		{
			super(expectedCycle, expectedTests);
		}

		public static void setUpBatch0(Status status, Adder adder)
		{
			adder(Phase.FOURTH, TEST_0, batchTester(), adder,
								status);
			assertPrefix(TEST_0, status);
		}

		public static void setUpBatch1(Status status, Adder adder)
		{
			adder(Phase.FOURTH, TEST_1, batchTester(), adder,
								status);
			assertPrefix(TEST_1, status);
		}

		private void doTest(List<Phase> cyclePhases,
					List<Phase> testPhases, Status status)
		{
			assert expectedCycle.equals(cyclePhases) : cyclePhases;
			assert expectedTests.equals(testPhases) : testPhases;
		}

		@Override
		public void test0(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			doTest(cyclePhases, testPhases, status);
		}

		@Override
		public void test1(Status status, List<Phase> cyclePhases,
						List<Phase> testPhases)
		{
			doTest(cyclePhases, testPhases, status);
		}

		public static Instantiable<FourthDescendant> instantiable()
		{
			class Bottle
			{
				private final StringBuilder bottle;

				Bottle(String hay)
				{
					bottle = new StringBuilder(hay);
				}

				private Bottle toUCNeedleHead(String needle,
								int wisp)
				{
					bottle.replace(wisp,
						wisp + 1,
						Character.toString(needle
								.codePointAt(0))
							.toUpperCase(
								Locale.ROOT));
					return this;
				}

				private Bottle toLCNeedleTail(String needle,
								int wisp)
				{
					bottle.replace(wisp + 1,
						wisp + needle.length(),
						needle.substring(1,
								needle.length())
							.toLowerCase(
								Locale.ROOT));
					return this;
				}

				Bottle lookForAndCapitalise(String needle)
				{
					final int wisp = bottle.indexOf(needle);
					return (wisp < 0 || needle.length() < 1)
						? this
						: (needle.length() < 2)
							? toUCNeedleHead(needle,
									wisp)
							: toUCNeedleHead(needle,
									wisp)
								.toLCNeedleTail(
									needle,
									wisp);
				}

				@Override
				public String toString()
				{
					return bottle.toString();
				}
			}

			final String testResultTemplet = Set.of("FAILED",
								"PASSED")
				.stream()	/* Change to "Passed|Failed". */
				.reduce(new Bottle(TestResult
						.RENDITION_RED_YELLOW_GREEN),
					Bottle::lookForAndCapitalise,
					Support.uoeThrower())
				.toString();
			return Instantiable.<FourthDescendant>newBuilder(
								SINGLETONS)
				.arguments(arguments())
				.executionPolicy(ExecutionPolicy.CONCURRENT)
				.prefixables(Set.of(new DefaultPrefix(TEST_0,
							Set.of(BATCH_UP_0,
								BATCH_DOWN_0)),
						new DefaultPrefix(TEST_1,
							Set.of(BATCH_UP_1,
								BATCH_DOWN_1))))
				.reportable(AteTester.TEST_REPORTER
					.apply(Reportable.newBuilder()
						.otherResultReporter(
							Reporters.otherResultReporter()
								.apply(OtherResult
									.RENDITION_REVERSE_VIDEO))
						.testResultReporter(
							Reporters.testResultReporter()
								.apply(testResultTemplet))
						.setUpAllResultReporter(
							Reporters.deferredSetUpAllResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.tearDownAllResultReporter(
							Reporters.deferredTearDownAllResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.setUpBatchResultReporter(
							Reporters.deferredSetUpBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.tearDownBatchResultReporter(
							Reporters.deferredTearDownBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.build()))
				.build();
		}
	}

	private static Map<String, Collection<List<?>>> arguments()
	{
		final List<Phase> phases0 = Collections.synchronizedList(
							new ArrayList<>());
		final List<Phase> phases1 = Collections.synchronizedList(
							new ArrayList<>());
		final List<Phase> phases00 = Collections.synchronizedList(
							new ArrayList<>());
		final List<Phase> phases11 = Collections.synchronizedList(
							new ArrayList<>());
		final Adder adder0 = phasedAdder()
			.apply(phases0);
		final Adder adder1 = phasedAdder()
			.apply(phases1);
		return Map.of(BATCH_UP_0, Set.of(List.of(adder0),
					List.of(adder0, false)),
			BATCH_UP_1, Set.of(List.of(adder1),
					List.of(adder1, true)),
			TEST_0, Set.of(List.of(phases0, phases00)),
			TEST_00, Set.of(List.of(phases00),
					List.of(phases00, false)),
			TEST_1, Set.of(List.of(phases1, phases11)),
			TEST_11, Set.of(List.of(phases11, false),
					List.of(phases11)),
			BATCH_DOWN_0, Set.of(List.of(phases0, phases00)),
			BATCH_DOWN_1, Set.of(List.of(phases1, phases11)));
	}

	/*
	 * Makes {setUp,tearDown}{,Batch} methods observe transition of
	 * prefixables without relying on their #lifeCycleMethodNamePrefixes().
	 */
	private static Function<List<Phase>, Adder> phasedAdder()
	{
		return phases -> tester -> phase ->
				(testMethodNamePrefix, currentPrefix) ->
					((tester.apply(phases)
						.apply(phase)
						.test(testMethodNamePrefix,
							currentPrefix))
				? Optional.<Phase>of(phase)
				: Optional.<Phase>empty())
			.ifPresent(Support.<List<Phase>, Consumer<Phase>>
								partialApply(
				phases_ -> phases_::add,
				phases));
	}

	interface Adder extends Function<Function<List<Phase>,
					Function<Phase,
					BiPredicate<String, String>>>,
				Function<Phase,
				BiConsumer<String, String>>> { }
	enum Phase { FIRST, SECOND, THIRD, FOURTH }
}
