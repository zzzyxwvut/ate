package tester.ate;

import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.ParaprefixableResult;
import org.zzzyxwvut.ate.Result.SetUpAllResult;
import org.zzzyxwvut.ate.Result.SetUpBatchResult;
import org.zzzyxwvut.ate.Result.TearDownAllResult;
import org.zzzyxwvut.ate.Result.TearDownBatchResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.Support;

/*
 * This registry offers the managing of assertion results done in life cycle
 * methods for future processing in some test method context, conducing to
 * an overall result value (but consider Tester.Builder#continueOnFailure()).
 *
 * Note that every relayed assertion error would maintain its identity and
 * make a future "failed" total accurate, whereas its complement would be
 * a unity element and make a future "passed" total deficient whenever there
 * are at least two methods of a kind.
 */
public final class LifeCycleMethodResultRegistry
{
	private static final List<Throwable> ERROR_SENTINEL = List.of(
				new Throwable("A beast of _when_") { });

	private final Map<Class<? extends Testable>,
					Queue<List<Optional<AssertionError>>>>
								registry =
						new ConcurrentHashMap<>();
	private final Function<Class<? extends Testable>, Consumer<Throwable>>
								registrar;

	LifeCycleMethodResultRegistry(
			Class<? extends AssertionError> deferredErrorClass)
	{
		Objects.requireNonNull(deferredErrorClass, "deferredErrorClass");
		registrar = Support.<Map<Class<? extends Testable>,
					Queue<List<Optional<AssertionError>>>>,
				Function<Class<? extends AssertionError>,
				Function<Class<? extends Testable>,
				Consumer<Throwable>>>>
								partialApply(
			registry_ -> deferredErrorClass_ -> testClass -> error ->
								registry_
				.computeIfAbsent(testClass,
					key -> new ConcurrentLinkedDeque<>())

				/*
				 * Each optional is made the element of a list
				 * in order to prepare the collection elements
				 * for a map value of Instantiable#arguments().
				 */
				.offer(List.of((deferredErrorClass_
							.isInstance(error))
					? Optional.ofNullable(
						(AssertionError)
							error.getCause())
					: Optional.empty())),
			registry)
				.apply(deferredErrorClass);
	}

	private void doMakeEntry(Class<? extends Testable> testClass,
						ParaprefixableResult result,
						MethodKind methodKind)
	{
		Objects.requireNonNull(testClass, "testClass");
		Objects.requireNonNull(result, "result");
		result.errors()
			.getOrDefault(methodKind, ERROR_SENTINEL)
			.stream()
			.forEach(registrar
				.apply(testClass));
	}

	void makeEntry(Class<? extends Testable> testClass,
						SetUpAllResult result)
	{
		doMakeEntry(testClass, result, MethodKind.SET_UP_ALL);
	}

	void makeEntry(Class<? extends Testable> testClass,
						TearDownAllResult result)
	{
		doMakeEntry(testClass, result, MethodKind.TEAR_DOWN_ALL);
	}

	void makeEntry(Class<? extends Testable> testClass,
						SetUpBatchResult result)
	{
		doMakeEntry(testClass, result, MethodKind.SET_UP_BATCH);
	}

	void makeEntry(Class<? extends Testable> testClass,
						TearDownBatchResult result)
	{
		doMakeEntry(testClass, result, MethodKind.TEAR_DOWN_BATCH);
	}

	public Stream<List<Optional<AssertionError>>> traverseEntries()
	{
		return registry
			.values()
			.stream()
			.flatMap(Queue::stream);
	}

	public boolean isBlank()		{ return registry.isEmpty(); }
	public void expungeAllEntries()		{ registry.clear(); }

	public static class Reporters
	{
		private Reporters() { /* No instantiation. */ }

		public static Function<String,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<OtherResult>>>> otherResultReporter()
		{
			return templet -> stream -> testClass -> result ->
								stream.format(
				String.format(templet, result
						.testMethodNamePrefix()
						.orElseThrow()),
				testClass.getName());
		}	/* It takes after Result#summary(String). */

		public static Function<String,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TestResult>>>> testResultReporter()
		{
			return templet -> stream -> testClass -> result ->
								stream.format(
				String.format(templet,
					result.testMethodNamePrefix()
						.orElseThrow(),
					result.successTotal(),
					result.failureTotal()),
				testClass.getName());
		}	/* It takes after Result#summary(String). */

		public static Function<LifeCycleMethodResultRegistry,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpAllResult>>>>
					deferredSetUpAllResultReporter()
		{
			return registry -> stream -> testClass -> result ->
								registry
				.makeEntry(testClass, result);
		}

		public static Function<LifeCycleMethodResultRegistry,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownAllResult>>>>
					deferredTearDownAllResultReporter()
		{
			return registry -> stream -> testClass -> result ->
								registry
				.makeEntry(testClass, result);
		}

		public static Function<LifeCycleMethodResultRegistry,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpBatchResult>>>>
					deferredSetUpBatchResultReporter()
		{
			return registry -> stream -> testClass -> result ->
								registry
				.makeEntry(testClass, result);
		}

		public static Function<LifeCycleMethodResultRegistry,
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownBatchResult>>>>
					deferredTearDownBatchResultReporter()
		{
			return registry -> stream -> testClass -> result ->
								registry
				.makeEntry(testClass, result);
		}
	}
}
