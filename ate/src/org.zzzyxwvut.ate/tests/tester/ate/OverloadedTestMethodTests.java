package tester.ate;

import static org.zzzyxwvut.ate.internal.ClassMethodNameOracle.methodNameMatchingCounter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.ClassMethodNameOracle;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class OverloadedTestMethodTests implements Testable
{
	private static final AtomicInteger TOTAL = new AtomicInteger();

	public static void tearDownBatch(int expected)
	{
		final int obtained = TOTAL.get();
		TOTAL.set(0);

		try {
			assert expected == obtained : String.format("%d != %d",
								expected,
								obtained);
		} catch (final AssertionError e) {
			throw new DeferredAssertionError(e);
		}
	}

	public void tearDown()			{ TOTAL.incrementAndGet(); }

	public void testPlain(String text) { }
	public void testPlain(int value) { }

	public void testVarArity(Status status, String text, int... values) { }
	public void testVarArity(String text, int... values) { }
	public void testVarArity(Status status, String text) { }
	public void testVarArity(String text) { }
	public void testVarArity(Status status, String... words) { }
	public void testVarArity(String... words) { }
	public void testVarArity(Status status) { }
	public void testVarArity() { }

	public void testObjectWidening(Object... values) { }

	public void testNumberWidening(Status status, Number... values) { }
	public void testNumberWidening(Number... values) { }
	public void testNumberWidening(Status status, Number[] some,
							Number... other) { }
	public void testNumberWidening(Number[] result, Number[] values) { }
	public void testNumberWidening(byte[] result, byte[] values) { }
	public void testNumberWidening(Number[]... values) { }

	public void testNarrowingToInt(int... values) { }

	public <T extends Number> void testGenericTypePassing(
				List<Class<T>> list0, List<Class<T>> list1) { }

	public void testLambdaPassing(StringSupplier s0, StringSupplier s1) { }
	public void testLambdaPassing(IntegerSupplier s0, IntegerSupplier s1) { }

	public void testNotTooManyArguments(int x, int y)
	{
		throw new AssertionError();
	}

	public void testNotPrimitiveAsBoxedNarrowing(int value)
	{
		throw new AssertionError();
	}

	public void testNotPrimitiveAsBoxedWidening(long value)
	{
		throw new AssertionError();
	}

	public void testNotPrimitiveAsBoxedWidening(long... values)
	{
		throw new AssertionError();
	}

	public static Instantiable<OverloadedTestMethodTests> instantiable()
	{
		return Instantiable.<OverloadedTestMethodTests>newBuilder(
				Set.of(OverloadedTestMethodTests::new))
			.arguments(arguments())
			.executionPolicy(ExecutionPolicy.CONCURRENT)
			.reportable(AteTester.TEST_REPORTER
				.apply(Reportable.newBuilder()
					.otherResultReporter(
						Reporters.otherResultReporter()
							.apply(OtherResult
								.RENDITION_REVERSE_VIDEO))
					.testResultReporter(
						Reporters.testResultReporter()
							.apply(TestResult
								.RENDITION_RED_YELLOW_GREEN))
					.tearDownBatchResultReporter(
						Reporters.deferredTearDownBatchResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))
					.build()))
			.build();
	}

	private static Map<String, Collection<List<?>>> arguments()
	{
		final Function<String, IntUnaryOperator> nameCounter =
						methodNameMatchingCounter(
					ClassMethodNameOracle.newInstance(
						OverloadedTestMethodTests.class));
		int assignables = 0;

		final Map.Entry<String, Collection<List<?>>> entry0 =
			Map.entry("testPlain",
				List.of(List.of("hello"),
					List.of(7)));
		assignables += nameCounter
			.apply("testPlain")
			.applyAsInt(2);

		final List<Object> none = new ArrayList<>(2);
		none.add(null);
		none.add(0);
		final Map.Entry<String, Collection<List<?>>> entry1 =
			Map.entry("testVarArity",
				List.of(List.of(),			/* +1 */
			// testVarArity(Status, String...)
			// testVarArity(String...)
					none,				/* +1 */
			// testVarArity(Status, String, int...)
			// testVarArity(String, int...)
					List.of("hello"),		/* +5 */
			// testVarArity(Status, String, int...)
			// testVarArity(Status, String)
			// testVarArity(Status, String...)
			// testVarArity(String, int...)
			// testVarArity(String)
			// testVarArity(String...)
					List.of("hello", 1, 2)));	/* +1 */
			// testVarArity(Status, String, int...)
			// testVarArity(String, int...)
		assignables += nameCounter
			.apply("testVarArity")
			.applyAsInt(4 + 8 + 2);
			// testVarArity(Status)
			// testVarArity()

		final Map.Entry<String, Collection<List<?>>> entry2 =
			Map.entry("testObjectWidening",
				List.of(List.of((byte) 0, (byte) -1),
					List.of((short) 0, (short) -1)));
		assignables += nameCounter
			.apply("testObjectWidening")
			.applyAsInt(2);

		final Map.Entry<String, Collection<List<?>>> entry3 =
			Map.entry("testNumberWidening",
				List.of(List.of((byte) 1, (byte) 1),	/* +1 */
					List.of((short) 0, (short) 0),	/* +1 */
					List.of(new byte[] {
						(byte) 0, (byte) 1,
						(byte) 2, (byte) 3
					}, new byte[] {
						(byte) 0, (byte) 1,
						(byte) 2, (byte) 3
					}),
					List.of(new Long[] { 255L }),	/* +1 */
					List.of(new Byte[] {		/* +2 */
						(byte) 0, (byte) 1,
						(byte) -2, (byte) 3
					}, new Byte[] {
						(byte) 0, (byte) 1,
						(byte) -2, (byte) 3
					}),
					List.of(new Byte[] {
						(byte) 3, (byte) 0,
						(byte) 1, (byte) 2
					}, (short) 11, 2.0, 1.0f)));
		assignables += nameCounter
			.apply("testNumberWidening")
			.applyAsInt(6 + 5);

		final Map.Entry<String, Collection<List<?>>> entry4 =
			Map.entry("testNarrowingToInt",
				List.of(List.of((int) 1L),
					List.of((int) Double.NaN)));
		assignables += nameCounter
			.apply("testNarrowingToInt")
			.applyAsInt(2);

		final Map.Entry<String, Collection<List<?>>> entry5 =
			Map.entry("testGenericTypePassing",
				List.of(List.of(List.of(Byte.class,
							Long.class),
						List.of(Long.class,
							Byte.class)),
					List.of(List.of(Number.class),
						List.of(Number.class))));
		assignables += nameCounter
			.apply("testGenericTypePassing")
			.applyAsInt(2);

		final Map.Entry<String, Collection<List<?>>> entry6 =
			Map.entry("testLambdaPassing",
				List.of(List.<StringSupplier>of(
						() -> "hello",
						() -> "hello"),
					List.<IntegerSupplier>of(
						() -> 0,
						() -> 0)));
		assignables += nameCounter
			.apply("testLambdaPassing")
			.applyAsInt(2);

		final Map.Entry<String, Collection<List<?>>> entry7 =
			Map.entry("tearDownBatch",
				List.of(List.of(assignables)));	/* 35 */

		return Map.ofEntries(entry0, entry1, entry2, entry3,
				entry4, entry5, entry6, entry7,
			Map.entry("testNotTooManyArguments",
				List.of(List.of(0, 1, 2),
					List.of(0, 1, 2, 3))),
			Map.entry("testNotPrimitiveAsBoxedNarrowing",
				List.of(List.of(0L),
					List.of(-0.1))),
			Map.entry("testNotPrimitiveAsBoxedWidening",
				List.of(List.of((byte) 0, (byte) 1),
					List.of((short) 0, (short) -1),
					List.of(10, 1L),
					List.of(10))));
	}

	interface StringSupplier extends Supplier<String> { }
	interface IntegerSupplier extends Supplier<Integer> { }
}
