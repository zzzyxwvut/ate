package tester.ate;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Objects;

public final class DeferredAssertionError extends AssertionError
{
	private static final long serialVersionUID = 1L;

	public DeferredAssertionError(AssertionError cause)
	{
		super(Objects.requireNonNull(cause, "cause"));
	}

	@Override
	public AssertionError getCause()
	{
		return (AssertionError) super.getCause();
	}

	private void readObject(ObjectInputStream stream) throws IOException,
							ClassNotFoundException
	{
		stream.defaultReadObject();

		if (!(super.getCause() instanceof AssertionError))
			throw new InvalidObjectException(
						"Not an AssertionError");
	}
}
