package tester.ate;

import java.util.ArrayDeque;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.configuration.SummarisingRecorder;

class InheritedAndGroupedNidus
{
	interface NumericCalc<T extends Number>
	{
		T plus(T i, T j);
		T minus(T i, T j);
		T times(T i, T j);
		T dividedBy(T i, T j);
	}

	interface Cyclable
	{
		static void setUpBatch(Status status)
		{
			status.printStream().println("[");
		}

		static void tearDownBatch(Status status)
		{
			status.printStream().println("]");
		}

		default void setUp(Status status)
		{
			status.printStream().print("<");
		}

		default void tearDown(Status status)
		{
			status.printStream().println(">");
		}
	}

	interface LongCalcTests extends Cyclable
	{
		NumericCalc<Long> longCalc();

		private static void assertEqual(long i, long j, String s,
								Status status)
		{
			assert i == j : String.format("%d != %d", i, j);
			status.printStream().print(s);
		}

		default void testPlusLong(Status status, long i, long j,
								long k)
		{
			assertEqual(k, longCalc().plus(i, j), "+", status);
		}

		default void testMinusLong(Status status, long i, long j,
								long k)
		{
			assertEqual(k, longCalc().minus(i, j), "-", status);
		}

		default void testTimesLong(Status status, long i, long j,
								long k)
		{
			assertEqual(k, longCalc().times(i, j), "*", status);
		}

		default void testDividedByLong(Status status, long i, long j,
								long k)
		{
			assertEqual(k, longCalc().dividedBy(i, j), "/", status);
		}
	}

	interface ByteCalcTests extends Cyclable
	{
		NumericCalc<Byte> byteCalc();

		private static void assertEqual(byte i, byte j, String s,
								Status status)
		{
			assert i == j : String.format("%d != %d", i, j);
			status.printStream().print(s);
		}

		default void testPlusByte(Status status, byte i, byte j,
								byte k)
		{
			assertEqual(k, byteCalc().plus(i, j), "+", status);
		}

		default void testMinusByte(Status status, byte i, byte j,
								byte k)
		{
			assertEqual(k, byteCalc().minus(i, j), "-", status);
		}

		default void testTimesByte(Status status, byte i, byte j,
								byte k)
		{
			assertEqual(k, byteCalc().times(i, j), "*", status);
		}

		default void testDividedByByte(Status status, byte i, byte j,
								byte k)
		{
			assertEqual(k, byteCalc().dividedBy(i, j), "/", status);
		}
	}

	interface LongByteCalcTestable extends Testable, ByteCalcTests,
							LongCalcTests
	{
		static Instantiable<?> instantiable()
		{
			class LongNumericCalc implements NumericCalc<Long>
			{
				@Override
				public Long plus(Long i, Long j)
				{
					return (i + j);
				}

				@Override
				public Long minus(Long i, Long j)
				{
					return (i - j);
				}

				@Override
				public Long times(Long i, Long j)
				{
					return (i * j);
				}

				@Override
				public Long dividedBy(Long i, Long j)
				{
					return (i / j);
				}
			}

			class ByteNumericCalc implements NumericCalc<Byte>
			{
				@Override
				public Byte plus(Byte i, Byte j)
				{
					return (byte) (i + j);
				}

				@Override
				public Byte minus(Byte i, Byte j)
				{
					return (byte) (i - j);
				}

				@Override
				public Byte times(Byte i, Byte j)
				{
					return (byte) (i * j);
				}

				@Override
				public Byte dividedBy(Byte i, Byte j)
				{
					return (byte) (i / j);
				}
			}

			class DefaultLongByteCalcTestable implements
							LongByteCalcTestable
			{
				@Override
				public ByteNumericCalc byteCalc()
				{
					return new ByteNumericCalc();
				}

				@Override
				public LongNumericCalc longCalc()
				{
					return new LongNumericCalc();
				}
			}

			return Instantiable.newBuilder(Set.of(
					DefaultLongByteCalcTestable::new))
				.arguments(Map.of("testDividedByByte",
					Set.of(List.of((byte) -45, (byte) -33,
								(byte) 1),
						List.of((byte) 8, (byte) -72,
								(byte) 0),
						List.of((byte) 49, (byte) -3,
								(byte) -16),
						List.of((byte) 22, (byte) 56,
								(byte) 0)),
					"testTimesByte",
					Set.of(List.of((byte) 89, (byte) 121,
								(byte) 17),
						List.of((byte) 68, (byte) 10,
								(byte) -88),
						List.of((byte) 29, (byte) 4,
								(byte) 116),
						List.of((byte) 103, (byte) -90,
								(byte) -54)),
					"testMinusByte",
					Set.of(List.of((byte) 56, (byte) -48,
								(byte) 104),
						List.of((byte) -120, (byte) 29,
								(byte) 107),
						List.of((byte) 85, (byte) 10,
								(byte) 75),
						List.of((byte) -54, (byte) 127,
								(byte) 75)),
					"testPlusByte",
					Set.of(List.of((byte) -15, (byte) 125,
								(byte) 110),
						List.of((byte) -48, (byte) 77,
								(byte) 29),
						List.of((byte) 88, (byte) 19,
								(byte) 107),
						List.of((byte) 113, (byte) 59,
								(byte) -84)),
					"testDividedByLong",
					Set.of(List.of(-25818L, 12443L, -2L),
						List.of(13699L, 12059L, 1L),
						List.of(-7455L, -32139L, 0L),
						List.of(14264L, 6485L, 2L)),
					"testTimesLong",
					Set.of(List.of(8596L, -32286L,
								-277530456L),
						List.of(3517L, 12881L,
								45302477L),
						List.of(27088L, -32196L,
								-872125248L),
						List.of(28604L, 22069L,
								631261676L)),
					"testMinusLong",
					Set.of(List.of(17711L, 17405L,
								306L),
						List.of(11825L, 18283L,
								-6458L),
						List.of(9212L, 12335L,
								-3123L),
						List.of(-1986L, 2429L,
								-4415L)),
					"testPlusLong",
					Set.of(List.of(-6429L, 20302L,
								13873L),
						List.of(-13377L, 12941L,
								-436L),
						List.of(18520L, 4690L,
								23210L),
						List.of(27701L, 6871L,
								34572L))))
				.executionPolicy(ExecutionPolicy.SEQUENTIAL)
				.prefixables(new LinkedHashSet<>(List.of(
					new DefaultPrefix("testDividedBy"),
					new DefaultPrefix("testTimes"),
					new DefaultPrefix("testMinus"),
					new DefaultPrefix("testPlus"))))
				.reportable(AteTester.TEST_REPORTER
					.apply(SummarisingRecorder
						.recorder()
						.apply(key ->
							new ArrayDeque<>())
						.apply(AteTester.TALLY)))
				.build();
		}
	}
}
