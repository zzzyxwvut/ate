package tester.ate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Configurable.Prefixable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.Support;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class HierarchicalExecutionOrderNidus
{
	static final String UP_ALL = "setUpAll";
	static final String DOWN_ALL = "tearDownAll";

	static final String UP_BATCH_0 = "setUpBatch0";
	static final String DOWN_BATCH_0 = "tearDownBatch0";

	static final String UP_BATCH_1 = "setUpBatch1";
	static final String DOWN_BATCH_1 = "tearDownBatch1";

	static final String UP_0 = "setUp0";
	static final String UP_00 = "setUp00";
	static final String UP_000 = "setUp000";
	static final String UP_0000 = "setUp0000";

	static final String DOWN_0 = "tearDown0";
	static final String DOWN_00 = "tearDown00";
	static final String DOWN_000 = "tearDown000";
	static final String DOWN_0000 = "tearDown0000";

	static final String UP_1 = "setUp1";
	static final String UP_11 = "setUp11";
	static final String UP_111 = "setUp111";
	static final String UP_1111 = "setUp1111";

	static final String DOWN_1 = "tearDown1";
	static final String DOWN_11 = "tearDown11";
	static final String DOWN_111 = "tearDown111";
	static final String DOWN_1111 = "tearDown1111";

	static final String TEST_0 = "test0";
	static final String TEST_00 = "test00";
	static final String TEST_000 = "test000";
	static final String TEST_0000 = "test0000";

	static final String TEST_1 = "test1";
	static final String TEST_11 = "test11";
	static final String TEST_111 = "test111";
	static final String TEST_1111 = "test1111";

	static final String T_0 = "0";			// test0...
	static final String T_1 = "1";			// test1...

	static final Map<String, List<Name>> GROUPS = Map.of(
			UP_ALL, List.of(Name.SET_UP_ALL_X,
					Name.SET_UP_ALL_XX,
					Name.SET_UP_ALL_XXX,
					Name.SET_UP_ALL_XXXX),
			DOWN_ALL, List.of(Name.TEAR_DOWN_ALL_XXXX,
					Name.TEAR_DOWN_ALL_XXX,
					Name.TEAR_DOWN_ALL_XX,
					Name.TEAR_DOWN_ALL_X),
			UP_BATCH_0, List.of(Name.SET_UP_BATCH_0,
					Name.SET_UP_BATCH_00,
					Name.SET_UP_BATCH_000,
					Name.SET_UP_BATCH_0000),
			DOWN_BATCH_0, List.of(Name.TEAR_DOWN_BATCH_0000,
					Name.TEAR_DOWN_BATCH_000,
					Name.TEAR_DOWN_BATCH_00,
					Name.TEAR_DOWN_BATCH_0),
			UP_BATCH_1, List.of(Name.SET_UP_BATCH_1,
					Name.SET_UP_BATCH_11,
					Name.SET_UP_BATCH_111,
					Name.SET_UP_BATCH_1111),
			DOWN_BATCH_1, List.of(Name.TEAR_DOWN_BATCH_1111,
					Name.TEAR_DOWN_BATCH_111,
					Name.TEAR_DOWN_BATCH_11,
					Name.TEAR_DOWN_BATCH_1),
			UP_0, List.of(Name.SET_UP_0,
					Name.SET_UP_00,
					Name.SET_UP_000,
					Name.SET_UP_0000),
			DOWN_0, List.of(Name.TEAR_DOWN_0000,
					Name.TEAR_DOWN_000,
					Name.TEAR_DOWN_00,
					Name.TEAR_DOWN_0),
			UP_1, List.of(Name.SET_UP_1,
					Name.SET_UP_11,
					Name.SET_UP_111,
					Name.SET_UP_1111),
			DOWN_1, List.of(Name.TEAR_DOWN_1111,
					Name.TEAR_DOWN_111,
					Name.TEAR_DOWN_11,
					Name.TEAR_DOWN_1));

	static class FirstDescendant
	{
		private static void addTestElement(Map<String, List<Name>> tests,
								String key,
								Name element)
		{
			Optional.ofNullable(tests.get(key))
				.ifPresent(Support.<Name, Consumer<List<Name>>>
								partialApply(
					element_ -> value -> value.add(element_),
					element));
		}

		static void addTest0Element(Map<String, List<Name>> tests,
								Name element)
		{
			addTestElement(tests, T_0, element);
		}

		static void addTest1Element(Map<String, List<Name>> tests,
								Name element)
		{
			addTestElement(tests, T_1, element);
		}

		public static void setUpBatch0(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_0);
		}

		public static void tearDownBatch0(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_0);
		}

		public static void setUpBatch1(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_1);
		}

		public static void tearDownBatch1(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_1);
		}

		public static void setUpBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public static void tearDownBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public void setUp0(List<Name> callers)
		{
			callers.add(Name.SET_UP_0);
		}

		public void tearDown0(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_0);
		}

		public void setUp1(List<Name> callers)
		{
			callers.add(Name.SET_UP_1);
		}

		public void tearDown1(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_1);
		}

		public void setUp2()
		{
			throw new AssertionError();
		}

		public void tearDown2()
		{
			throw new AssertionError();
		}

		public void test0(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_0);
			addTest0Element(tests, Name.TEST_0);
		}

		public void test1(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_1);
			addTest1Element(tests, Name.TEST_1);
		}

		public void test2()
		{
			throw new AssertionError();
		}

		public static void setUpAll(List<Name> callers)
		{
			callers.add(Name.SET_UP_ALL_X);
		}

		public static void tearDownAll(List<Name> obtained,
						Map<String, List<Name>> tests,
						boolean ordered)
		{
			obtained.add(Name.TEAR_DOWN_ALL_X);
			final List<Name> tests0 = tests.getOrDefault(T_0,
								List.of());
			final List<Name> tests1 = tests.getOrDefault(T_1,
								List.of());

			/*
			 * Dynamic assembling allows for any (41) permutation:
			 *	-Date.some=.+?Hierarchical@test[01]\{1,4\}? \
			 *	-Date.tokens=:@@
			 */
			final List<Name> expected = Stream
				.of(GROUPS.get(UP_ALL).stream(), ((ordered)
					? Stream.of(names0(tests0),
							names1(tests1))
					: Stream.of(names1(tests1),
							names0(tests0)))
						.flatMap(Function.identity()),
					GROUPS.get(DOWN_ALL).stream())
				.flatMap(Function.identity())
				.collect(Collectors.toUnmodifiableList());

			try {
				assert expected.equals(obtained) :
							String.format(
								"%n%s !=%n%s",
								expected,
								obtained);
			} catch (final AssertionError e) {
				throw new DeferredAssertionError(e);
			}
		}

		private static Stream<Name> names0(List<Name> tests0)
		{
			return (tests0.isEmpty())
				? Stream.empty()
				: names(List.of(GROUPS.get(UP_BATCH_0),
						GROUPS.get(UP_0),
						tests0,
						GROUPS.get(DOWN_0),
						GROUPS.get(DOWN_BATCH_0)));
		}

		private static Stream<Name> names1(List<Name> tests1)
		{
			return (tests1.isEmpty())
				? Stream.empty()
				: names(List.of(GROUPS.get(UP_BATCH_1),
						GROUPS.get(UP_1),
						tests1,
						GROUPS.get(DOWN_1),
						GROUPS.get(DOWN_BATCH_1)));
		}

		private static Stream<Name> names(List<List<Name>> names)
		{
			return Stream.of(names.get(0)
						.stream(),
					names.get(2)
						.stream()
						.flatMap(Support.<List<Name>,
							Function<List<Name>,
								Function<Name,
									Stream<Name>>>>
								partialApply(
							head -> tail -> name -> Stream
								.of(head.stream(),
									Stream.of(
										name),
									tail.stream())
								.flatMap(Function
									.identity()),
							names.get(1))
								.apply(names
									.get(3))),
					names.get(4)
						.stream())
				.flatMap(Function.identity());
		}
	}

	static class SecondDescendant extends FirstDescendant
	{
		public static void setUpBatch0(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_00);
		}

		public static void tearDownBatch0(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_00);
		}

		public static void setUpBatch1(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_11);
		}

		public static void tearDownBatch1(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_11);
		}

		public static void setUpBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public static void tearDownBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public void setUp00(List<Name> callers)
		{
			callers.add(Name.SET_UP_00);
		}

		public void tearDown00(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_00);
		}

		public void setUp11(List<Name> callers)
		{
			callers.add(Name.SET_UP_11);
		}

		public void tearDown11(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_11);
		}

		public void setUp22()
		{
			throw new AssertionError();
		}

		public void tearDown22()
		{
			throw new AssertionError();
		}

		public void test00(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_00);
			addTest0Element(tests, Name.TEST_00);
		}

		public void test11(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_11);
			addTest1Element(tests, Name.TEST_11);
		}

		public void test22()
		{
			throw new AssertionError();
		}

		public static void setUpAll(List<Name> callers)
		{
			callers.add(Name.SET_UP_ALL_XX);
		}

		public static void tearDownAll(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_ALL_XX);
		}
	}

	static class ThirdDescendant extends SecondDescendant
	{
		public static void setUpBatch0(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_000);
		}

		public static void tearDownBatch0(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_000);
		}

		public static void setUpBatch1(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_111);
		}

		public static void tearDownBatch1(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_111);
		}

		public static void setUpBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public static void tearDownBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public void setUp000(List<Name> callers)
		{
			callers.add(Name.SET_UP_000);
		}

		public void tearDown000(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_000);
		}

		public void setUp111(List<Name> callers)
		{
			callers.add(Name.SET_UP_111);
		}

		public void tearDown111(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_111);
		}

		public void setUp222()
		{
			throw new AssertionError();
		}

		public void tearDown222()
		{
			throw new AssertionError();
		}

		public void test000(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_000);
			addTest0Element(tests, Name.TEST_000);
		}

		public void test111(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_111);
			addTest1Element(tests, Name.TEST_111);
		}

		public void test222()
		{
			throw new AssertionError();
		}

		public static void setUpAll(List<Name> callers)
		{
			callers.add(Name.SET_UP_ALL_XXX);
		}

		public static void tearDownAll(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_ALL_XXX);
		}
	}

	static class FourthDescendant extends ThirdDescendant
						implements Testable
	{
		public static void setUpBatch0(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_0000);
		}

		public static void tearDownBatch0(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_0000);
		}

		public static void setUpBatch1(List<Name> callers)
		{
			callers.add(Name.SET_UP_BATCH_1111);
		}

		public static void tearDownBatch1(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_BATCH_1111);
		}

		public static void setUpBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public static void tearDownBatch()
		{
			throw new DeferredAssertionError(new AssertionError());
		}

		public void setUp0000(List<Name> callers)
		{
			callers.add(Name.SET_UP_0000);
		}

		public void tearDown0000(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_0000);
		}

		public void setUp1111(List<Name> callers)
		{
			callers.add(Name.SET_UP_1111);
		}

		public void tearDown1111(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_1111);
		}

		public void setUp2222()
		{
			throw new AssertionError();
		}

		public void tearDown2222()
		{
			throw new AssertionError();
		}

		public void test0000(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_0000);
			addTest0Element(tests, Name.TEST_0000);
		}

		public void test1111(List<Name> callers,
						Map<String, List<Name>> tests)
		{
			callers.add(Name.TEST_1111);
			addTest1Element(tests, Name.TEST_1111);
		}

		public void test2222()
		{
			throw new AssertionError();
		}

		public static void setUpAll(List<Name> callers)
		{
			callers.add(Name.SET_UP_ALL_XXXX);
		}

		public static void tearDownAll(List<Name> callers)
		{
			callers.add(Name.TEAR_DOWN_ALL_XXXX);
		}

		public static Instantiable<FourthDescendant> instantiable()
		{
			final boolean ordered = ThreadLocalRandom
				.current()
				.nextBoolean();
			final Prefixable prefixable0 = new DefaultPrefix(TEST_0,
				Set.of(UP_0, DOWN_0, UP_BATCH_0, DOWN_BATCH_0));
			final Prefixable prefixable1 = new DefaultPrefix(TEST_1,
				Set.of(UP_1, DOWN_1, UP_BATCH_1, DOWN_BATCH_1));
			final Set<Prefixable> prefixables = new LinkedHashSet<>(
								(ordered)
				? List.of(prefixable0, prefixable1)
				: List.of(prefixable1, prefixable0));
			return Instantiable.<FourthDescendant>newBuilder(
					Set.of(FourthDescendant::new))
				.arguments(arguments(ordered))
				.executionPolicy(ExecutionPolicy.SEQUENTIAL)
				.prefixables(prefixables)
				.reportable(AteTester.TEST_REPORTER
					.apply(Reportable.newBuilder()
						.otherResultReporter(
							Reporters.otherResultReporter()
								.apply(OtherResult
									.RENDITION_REVERSE_VIDEO))
						.testResultReporter(
							Reporters.testResultReporter()
								.apply(TestResult
									.RENDITION_RED_YELLOW_GREEN))
						.tearDownAllResultReporter(
							Reporters.deferredTearDownAllResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))

						/* Against *#setUpBatch. */
						.setUpBatchResultReporter(
							Reporters.deferredSetUpBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))

						/* Against *#tearDownBatch. */
						.tearDownBatchResultReporter(
							Reporters.deferredTearDownBatchResultReporter()
								.apply(AteTester
									.DEFERRED_RESULT_REGISTRY))
						.build()))
				.build();
		}

		private static Map<String, Collection<List<?>>> arguments(
							boolean condition)
		{
			/*
			 * The number (96) of all test (8) and life cycle (88)
			 * method invocations.
			 */
			final List<Name> callers = new ArrayList<>(96);
			final Map<String, List<Name>> tests = Map.of(
						T_0, new ArrayList<>(4),
						T_1, new ArrayList<>(4));
			final Collection<List<?>> shared = List.of(
							List.of(callers,
								tests));
			final Collection<List<?>> common = List.of(
							List.of(callers));
			return Map.ofEntries(
				Map.entry(TEST_0, shared),
				Map.entry(TEST_00, shared),
				Map.entry(TEST_000, shared),
				Map.entry(TEST_0000, shared),

				Map.entry(TEST_1, shared),
				Map.entry(TEST_11, shared),
				Map.entry(TEST_111, shared),
				Map.entry(TEST_1111, shared),

				Map.entry(UP_0, common),
				Map.entry(UP_00, common),
				Map.entry(UP_000, common),
				Map.entry(UP_0000, common),

				Map.entry(DOWN_0, common),
				Map.entry(DOWN_00, common),
				Map.entry(DOWN_000, common),
				Map.entry(DOWN_0000, common),

				Map.entry(UP_1, common),
				Map.entry(UP_11, common),
				Map.entry(UP_111, common),
				Map.entry(UP_1111, common),

				Map.entry(DOWN_1, common),
				Map.entry(DOWN_11, common),
				Map.entry(DOWN_111, common),
				Map.entry(DOWN_1111, common),

				Map.entry(UP_BATCH_0, common),
				Map.entry(DOWN_BATCH_0, common),

				Map.entry(UP_BATCH_1, common),
				Map.entry(DOWN_BATCH_1, common),

				Map.entry(UP_ALL, common),
				Map.entry(DOWN_ALL, List.of(
						List.of(callers),
						List.of(callers,
							tests,
							condition))));
		}
	}

	enum Name
	{
		SET_UP_ALL_X,		TEAR_DOWN_ALL_X,
		SET_UP_ALL_XX,		TEAR_DOWN_ALL_XX,
		SET_UP_ALL_XXX,		TEAR_DOWN_ALL_XXX,
		SET_UP_ALL_XXXX,	TEAR_DOWN_ALL_XXXX,

		SET_UP_BATCH_0,		TEAR_DOWN_BATCH_0,
		SET_UP_BATCH_00,	TEAR_DOWN_BATCH_00,
		SET_UP_BATCH_000,	TEAR_DOWN_BATCH_000,
		SET_UP_BATCH_0000,	TEAR_DOWN_BATCH_0000,

		SET_UP_BATCH_1,		TEAR_DOWN_BATCH_1,
		SET_UP_BATCH_11,	TEAR_DOWN_BATCH_11,
		SET_UP_BATCH_111,	TEAR_DOWN_BATCH_111,
		SET_UP_BATCH_1111,	TEAR_DOWN_BATCH_1111,

		SET_UP_0,		TEAR_DOWN_0,
		SET_UP_00,		TEAR_DOWN_00,
		SET_UP_000,		TEAR_DOWN_000,
		SET_UP_0000,		TEAR_DOWN_0000,

		SET_UP_1,		TEAR_DOWN_1,
		SET_UP_11,		TEAR_DOWN_11,
		SET_UP_111,		TEAR_DOWN_111,
		SET_UP_1111,		TEAR_DOWN_1111,

		TEST_0, TEST_00, TEST_000, TEST_0000,
		TEST_1, TEST_11, TEST_111, TEST_1111
	}
}
