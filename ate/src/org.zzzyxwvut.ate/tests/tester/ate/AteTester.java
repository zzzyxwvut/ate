package tester.ate;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.configuration.SummarisingRecorder;
import org.zzzyxwvut.ate.internal.BootStrappedTestLitmusNidus;
import org.zzzyxwvut.ate.internal.ClassMethodNameFilterTests;
import org.zzzyxwvut.ate.internal.ConfigurableMergerTests;
import org.zzzyxwvut.ate.internal.InterfaceHierarchyWalkerTests;
import org.zzzyxwvut.ate.internal.Support;
import org.zzzyxwvut.ate.support.TestablePath;

import tester.ate.InheritedAndGroupedNidus.LongByteCalcTestable;
import tester.ate.PrototypableNidus.InstanceCountingTests;

public class AteTester
{
	public static final LifeCycleMethodResultRegistry DEFERRED_RESULT_REGISTRY =
					new LifeCycleMethodResultRegistry(
						DeferredAssertionError.class);
	public static final UnaryOperator<Reportable> META_REPORTER;
	public static final UnaryOperator<Reportable> TEST_REPORTER;

	static {
		final ClassLoader loader = AteTester.class.getClassLoader();
		List.of(BootStrappedTestLitmusNidus.class, /* The host class. */
				BootStrappedTestLitmusNidus.ClientTests.class,
				BootStrappedTestLitmusNidus.PatronTests.class,
				ClassMethodNameFilterTests.class,
				ConfigurableMergerTests.class,
				InterfaceHierarchyWalkerTests.class)
			.forEach(Support.<ClassLoader, Consumer<Class<?>>>
								partialApply(
				loader_ -> klass -> loader_
					.setClassAssertionStatus(
							klass.getName(),
							true),
				loader));
		loader.setPackageAssertionStatus("tester.ate", true);
		final TwoWayReporter reporter = new TwoWayReporter(
				new TestablePath(TestablePathTests.class,
								"tests")
			.createSiblingDirectories("results-data")
			.orElseThrow(() -> new IllegalStateException(
				"Failed to create a \"results-data\" directory")));
		META_REPORTER = reporter.metaReporter();
		TEST_REPORTER = reporter.testReporter();
	}

	/*
	 * COLLECT Results FOR:
	 *	InheritedAndGroupedNidus$LongByteCalcTestable,
	 *	RepeatableTesterNidus$SingletonTests,
	 *	RepeatableTesterNidus$PrototypeTests
	 *
	 * (Some arbitrary "happens-before" program order is maintained
	 * between the execution of SingletonTests and PrototypeTests, and
	 * between the processing of their results and the result of execution
	 * of LongByteCalcTestable.)
	 */
	static final Map<Class<? extends Testable>, Queue<Result>> TALLY =
							new LinkedHashMap<>();

	private AteTester() { /* No instantiation. */ }

	private static Configurable configurable()
	{
		return Configurable.newBuilder()
			.reportable(TEST_REPORTER
				.apply(Reportable.newBuilder()
					.otherResultReporter(
						stream -> testClass -> result ->
								stream.format(
						"%n(\033[7m%s\033[0m)"
						+ "%n\033[7mNon-matching prefix:\033[0m %s%n",
						testClass.getName(),
						result.testMethodNamePrefix()
							.orElseThrow()))
					.testResultReporter(
						stream -> testClass -> result ->
								stream.format(
						"%n(\033[1;33m%s#%s\033[0m)"
						+ "%n\033[1;4;32mPASSED: %d\033[0m,"
						+ " \033[1;4;31mFAILED: %d\033[0m%n",
						testClass.getName(),
						result.testMethodNamePrefix()
							.orElseThrow(),
						result.successTotal(),
						result.failureTotal()))
					.build()))
			.build();
	}

	private static boolean runLifeCycleMethodResultTests()
	{
		LifeCycleMethodResultTests.main();
		return LifeCycleMethodResultTests.success;
	}	/* [29?, 31?, 33?] */

	private static boolean runAll()
	{
		return Set.<BooleanSupplier>of(() -> Tester.newBuilder(
			/*  2 */	Set.of(BootStrappedTestLitmusNidus
							.ClientTests.class,
			/*  4 */		BootStrappedTestLitmusNidus
							.PatronTests.class,
			/* 52 */		ClassMethodNameFilterTests.class,
			/*  4 */		ConfigurableMergerTests.class,
			/*  4 x 2 */		HierarchicalExecutionOrderNidus
							.FourthDescendant.class,
			/* 16 x 2 */		InstanceCountingTests.class,
			/*  4 */		InterfaceHierarchyWalkerTests.class,
			/*  8 x 4 */		LongByteCalcTestable.class,
			/* 20 + 0 + 0 */	MethodCycleAndCandidateTests.class,
			/* 35 */		OverloadedTestMethodTests.class,
			/*  3 x [2, 4] x 2 */	OverriddenTestMethodNidus
							.FourthDescendant.class,
			/*  8 */		ReportableTests.class,
			/*  4 */		TestablePathTests.class))
					.executionPolicy(
						ExecutionPolicy.CONCURRENT)
					.configurable(configurable())
					.continueOnFailure()

					/*
					 * SILENCE WARNINGS FOR:
					 *	ClassMethodNameFilterTests,
					 *	HierarchicalExecutionOrderNidus,
					 *	OverloadedTestMethodTests,
					 *	OverriddenTestMethodNidus
					 */
					.suppressUnassignability()
					.verifyAssertion()
					.build()
					.runAndReport(),
				() -> {
			/*  5 */	ArgumentableCardinalityTests.main();
					return ArgumentableCardinalityTests.success;
				},
				() -> {
			/*  2 */	ArgumentableFailureTests.main();
					return ArgumentableFailureTests.success;
				},
				() -> {
			// 2 x 8 x [2, 4]
			/* [32, 64] */	RepeatableTesterNidus.main();
					return RepeatableTesterNidus.success;
				},
				() -> {
			// Continue: (((5 + 2 + 1) + (4 + 3 + 1)) x 2) x 2 == 64
			// Abort (max): (5 x 2) x 2 + 1 + [1, 4] == [22, 25]
			// Abort (min): (1 x 1) x 2 == 2?
			/* [66?, 89] */	TestFailureNidus.main();
					return TestFailureNidus.success;
				},
				() -> {
			/*  2 */	ПотоковыеПробы.main();
					return ПотоковыеПробы.success;
				})
			.stream()	/* Prefer an unordered source. */
			.map(BooleanSupplier::getAsBoolean)
			.reduce(true, (left, right) -> left && right)
					& runLifeCycleMethodResultTests();
	}

	public static void main(String... args)
	{
		final boolean success;

		try {
			try {
				success = runAll();
			} finally {
				TestResultSummary.report(
					System.err,
					TestResultSummary
						.TEST_RENDITION_REVERSE_VIDEO,
					SummarisingRecorder.summarise(TALLY));
			}
		} finally {
			TALLY.clear();
			DEFERRED_RESULT_REGISTRY.expungeAllEntries();
		}

		System.exit((success) ? 0 : 1);
	}
}
