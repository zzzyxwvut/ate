package tester.ate;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.ClassMethodNameOracle;
import org.zzzyxwvut.ate.internal.ResultFriend;

class ArgumentableFailureTests implements Testable
{
	static boolean success = false;

	private static final String PREFIX_BOUND = "testBound";
	private static final String PREFIX_NON_BACKED = "testNonBacked";

	private static volatile boolean eligible = false;

	public void testNonBackedParameter(Void stub)
	{
		throw new UnsupportedOperationException();
	}

	public void testBoundParameter(Status status)
	{
		eligible = true;
		status.printStream().println("The other shape...");
	}

	public static Instantiable<ArgumentableFailureTests> instantiable()
	{
		return Instantiable.<ArgumentableFailureTests>newBuilder(
					Set.of(ArgumentableFailureTests::new))
			.reportable(AteTester.META_REPORTER
				.apply(Result.resultor()
					.apply(Result.renderer()
						.apply(Map.of(
							OtherResult.class,
							OtherResult.RENDITION_REVERSE_VIDEO,
							TestResult.class,
							TestResult.RENDITION_REVERSE_VIDEO)))))
			.printStream(new PrintStream(
					OutputStream.nullOutputStream()))
			.prefixables(new LinkedHashSet<>(
				List.of(new DefaultPrefix(PREFIX_BOUND),
					new DefaultPrefix(PREFIX_NON_BACKED))))
			.executionPolicy(ExecutionPolicy.SEQUENTIAL)
			.arguments(Map.of())
			.build();
	}	/* Verify Configurable/Instantiable builder compatibility. */

	public static void main(String... args)
	{
		success = false;
		eligible = false;
		boolean passing = true;

		try {
			passing = Tester.newBuilder(Set.of(
					ArgumentableFailureTests.class))
				.verifyAssertion()
				.build()
				.runAndReport();
		} catch (final Throwable expected) {
			passing = false;
			assert (expected instanceof IllegalStateException);
			AteTester.META_REPORTER
				.apply(stream -> testClass -> result -> stream
					.format("%n%s: \033[1;7;32mPASSED\033[0m%n",
						testClass.getCanonicalName()))
				.apply(System.err)
				.apply(ArgumentableFailureTests.class)
				.accept(ResultFriend
					.getResultable()
					.newTestResult(
						PREFIX_NON_BACKED,
						Map.of(MethodKind.TEST,
							List.of(expected)),
						1,
						0));
		} finally {
			final ClassMethodNameOracle oracle =
					ClassMethodNameOracle.newInstance(
						ArgumentableFailureTests.class);
			final boolean runBound = oracle.isRequestedMethod(
						"testBoundParameter");
			final boolean runNonBacked = oracle.isRequestedMethod(
						"testNonBackedParameter");
			final boolean confirmedRun = eligible;

			/*
			 * Always run the above tester and succeed with any of
			 * the following alternatives:
			 * (*) pick _both_ PREFIX_BOUND and PREFIX_NON_BACKED
			 *	with IllegalStateException (the default setup),
			 * (*) pick PREFIX_BOUND (e.g. -Date.some=...),
			 * (*) pick PREFIX_NON_BACKED with IllegalStateException
			 *	(e.g. -Date.some=...),
			 * (*) skip this class or pick an undefined method
			 *	name prefix (e.g. -Date.some=...).
			 */
			passing = ((runBound && confirmedRun
						&& runNonBacked && !passing)
				|| (runBound && confirmedRun && passing)
				|| (runNonBacked && !confirmedRun && !passing)
				|| (!runBound && !confirmedRun
						&& !runNonBacked && passing));
			success = passing;
		}

		if (!passing)
			throw new AssertionError();
	}
}
