package tester.ate;

import static tester.ate.TestFailureNidus.TestMethodName.DOWN_0;
import static tester.ate.TestFailureNidus.TestMethodName.DOWN_ALL_0;
import static tester.ate.TestFailureNidus.TestMethodName.DOWN_BATCH_0;
import static tester.ate.TestFailureNidus.TestMethodName.ERRORS;
import static tester.ate.TestFailureNidus.TestMethodName.TEST_1_PREFIX;
import static tester.ate.TestFailureNidus.TestMethodName.TEST_2_PREFIX;
import static tester.ate.TestFailureNidus.TestMethodName.TF1_0;
import static tester.ate.TestFailureNidus.TestMethodName.TF1_1;
import static tester.ate.TestFailureNidus.TestMethodName.TF2_0;
import static tester.ate.TestFailureNidus.TestMethodName.TF2_1;
import static tester.ate.TestFailureNidus.TestMethodName.TF2_2;
import static tester.ate.TestFailureNidus.TestMethodName.UP_0;
import static tester.ate.TestFailureNidus.TestMethodName.UP_ALL_0;
import static tester.ate.TestFailureNidus.TestMethodName.UP_BATCH_0;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.SetUpAllResult;
import org.zzzyxwvut.ate.Result.SetUpBatchResult;
import org.zzzyxwvut.ate.Result.TearDownAllResult;
import org.zzzyxwvut.ate.Result.TearDownBatchResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.ClassMethodNameOracle;
import org.zzzyxwvut.ate.internal.ResultFriend.Resultable;
import org.zzzyxwvut.ate.internal.ResultFriend;
import org.zzzyxwvut.ate.internal.Support;

class TestFailureNidus
{
	static boolean success = false;

	enum Concurrent
	{
		TRUE(true), FALSE(false);

		private final boolean value;

		private Concurrent(boolean value) { this.value = value; }

		boolean value()	{ return value; }
	}

	enum TestMethodName
	{
		UP_ALL_0("setUpAll0"),
		DOWN_ALL_0("tearDownAll0"),
		UP_BATCH_0("setUpBatch0"),
		DOWN_BATCH_0("tearDownBatch0"),
		UP_0("setUp0"),
		DOWN_0("tearDown0"),

		TS1__("testFirstSuccess_"),
		TS1_0("testFirstSuccess0"),
		TS1_1("testFirstSuccess1"),
		TS1_2("testFirstSuccess2"),
		TS1_3("testFirstSuccess3"),
		TS1_4("testFirstSuccess4"),
		TF1_0("testFirstFailure0"),
		TF1_1("testFirstFailure1"),

		TS2__("testSecondSuccess_"),
		TS2_0("testSecondSuccess0"),
		TS2_1("testSecondSuccess1"),
		TS2_2("testSecondSuccess2"),
		TS2_3("testSecondSuccess3"),
		TF2_0("testSecondFailure0"),
		TF2_1("testSecondFailure1"),
		TF2_2("testSecondFailure2"),

		EMPTY_TEST_PREFIX(""),
		TEST_1_PREFIX("testFirst"),
		TEST_2_PREFIX("testSecond");

		static final Map<TestMethodName, Error> ERRORS =
						Collections.unmodifiableMap(
								errors()
			.stream()
			.collect(Collectors.toMap(
				Function.identity(),
				AssertionError::new,
				Support.uoeThrower(),
				() -> new EnumMap<>(TestMethodName.class))));

		static final Map<Class<? extends Result>,
					Function<Concurrent,
					Function<Map<TestMethodName, Result>,
					Predicate<Result>>>>
						ABORTING_STRATEGY = Map.of(
							TestResult.class,
			concurrent -> objectives -> result -> {
				final TestResult obtained = (TestResult) result;
				final TestResult possible = (TestResult)
							objectives.get(
					matchTestMethodNamePrefix(obtained
						.testMethodNamePrefix()
						.orElseThrow()));
				return (((Objects.requireNonNullElseGet(
							obtained.errors()
								.get(MethodKind
									.TEST),
							List::of)
						.isEmpty())
							|| ((concurrent.value())
						? obtained.failureTotal() > 0
							&& obtained.failureTotal()
									<= possible
								.failureTotal()
						: obtained.failureTotal() == 1))
					&& Objects.checkIndex(
						obtained.successTotal(),
						possible.successTotal() + 1)
							== obtained.successTotal()
					&& !(Objects.requireNonNullElseGet(
							obtained.errors()
								.get(MethodKind
									.SET_UP),
							List::of)
						.isEmpty())
					&& !(Objects.requireNonNullElseGet(
							obtained.errors()
								.get(MethodKind
									.TEAR_DOWN),
							List::of)
						.isEmpty()));
			},

						SetUpBatchResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.SET_UP_BATCH,
					objectives.get(UP_BATCH_0),
					result),

						TearDownBatchResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.TEAR_DOWN_BATCH,
					objectives.get(DOWN_BATCH_0),
					result),

						SetUpAllResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.SET_UP_ALL,
					objectives.get(UP_ALL_0),
					result),

						TearDownAllResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.TEAR_DOWN_ALL,
					objectives.get(DOWN_ALL_0),
					result),

						OtherResult.class,
			concurrent -> objectives -> result -> true);

		static final Map<Class<? extends Result>,
					Function<Concurrent,
					Function<Map<TestMethodName, Result>,
					Predicate<Result>>>>
						CONTINUING_STRATEGY = Map.of(
							TestResult.class,
			concurrent -> objectives -> result -> {
				final TestResult obtained = (TestResult) result;
				final TestResult expected = (TestResult)
							objectives.get(
					matchTestMethodNamePrefix(obtained
						.testMethodNamePrefix()
						.orElseThrow()));
				return (expected.failureTotal()
						== obtained.failureTotal()
					&& expected.successTotal()
						== obtained.successTotal()
					&& containsAllErrors(MethodKind.SET_UP,
								expected,
								obtained)
					&& ((Objects.requireNonNullElseGet(
							obtained.errors()
								.get(MethodKind
									.TEST),
							List::of)
						.isEmpty())
							|| containsAllErrors(
								MethodKind.TEST,
								expected,
								obtained))
					&& containsAllErrors(MethodKind.TEAR_DOWN,
								expected,
								obtained));
			},

						SetUpBatchResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.SET_UP_BATCH,
					objectives.get(UP_BATCH_0),
					result),

						TearDownBatchResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.TEAR_DOWN_BATCH,
					objectives.get(DOWN_BATCH_0),
					result),

						SetUpAllResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.SET_UP_ALL,
					objectives.get(UP_ALL_0),
					result),

						TearDownAllResult.class,
			concurrent -> objectives -> result ->
				containsAllErrors(MethodKind.TEAR_DOWN_ALL,
					objectives.get(DOWN_ALL_0),
					result),

						OtherResult.class,
			concurrent -> objectives -> result -> true);

		private static final Resultable RESULTABLE = ResultFriend
							.getResultable();
		private static final Map<TestMethodName, Result> RESULTS;

		static {
			final Map<TestMethodName, Result> results =
					new EnumMap<>(TestMethodName.class);
			results.put(UP_ALL_0, RESULTABLE.newSetUpAllResult(
				Map.of(MethodKind.SET_UP_ALL,
					List.of(ERRORS.get(UP_ALL_0)))));
			results.put(DOWN_ALL_0, RESULTABLE.newTearDownAllResult(
				Map.of(MethodKind.TEAR_DOWN_ALL,
					List.of(ERRORS.get(DOWN_ALL_0)))));
			results.put(UP_BATCH_0, RESULTABLE
							.newSetUpBatchResult(
				"test...",
				Map.of(MethodKind.SET_UP_BATCH,
					List.of(ERRORS.get(UP_BATCH_0)))));
			results.put(DOWN_BATCH_0, RESULTABLE
							.newTearDownBatchResult(
				"test...",
				Map.of(MethodKind.TEAR_DOWN_BATCH,
					List.of(ERRORS.get(DOWN_BATCH_0)))));

			/* See #matchTestMethodNamePrefix(String). */
			results.put(EMPTY_TEST_PREFIX,
						RESULTABLE.newTestResult(
							"", Map.of(), 0, 0));
			RESULTS = Collections.unmodifiableMap(results);
		}

		private final String name;

		private TestMethodName(String name)	{ this.name = name; }

		@Override
		public String toString()		{ return name; }

		private static List<TestMethodName> errors()
		{
			return List.of(UP_ALL_0, DOWN_ALL_0,
				UP_BATCH_0, DOWN_BATCH_0,
				UP_0, DOWN_0,
				TF1_0, TF1_1,
				TF2_0, TF2_1, TF2_2);
		}

		private static TestMethodName matchTestMethodNamePrefix(
								String prefix)
		{
			return (prefix.startsWith(TEST_1_PREFIX.toString()))
				? TEST_1_PREFIX
				: prefix.startsWith(TEST_2_PREFIX.toString())
					? TEST_2_PREFIX
					: EMPTY_TEST_PREFIX;
		}

		private static List<TestMethodName> firstSuccess()
		{
			return List.of(TS1_0, TS1_1, TS1_2, TS1_3, TS1_4, TS1__);
		}

		private static List<TestMethodName> firstFailure()
		{
			return List.of(TF1_0, TF1_1);
		}

		private static List<TestMethodName> secondSuccess()
		{
			return List.of(TS2_0, TS2_1, TS2_2, TS2_3, TS2__);
		}

		private static List<TestMethodName> secondFailure()
		{
			return List.of(TF2_0, TF2_1, TF2_2);
		}

		private static boolean containsAllErrors(MethodKind kind,
							Result expected,
							Result obtained)
		{
			final List<Throwable> expectedErrors =
						Objects.requireNonNullElseGet(
					expected.errors().get(kind),
					List::of);
			final List<Throwable> obtainedErrors =
						Objects.requireNonNullElseGet(
					obtained.errors().get(kind),
					List::of);
			return (expectedErrors.size() == obtainedErrors.size()
				&& expectedErrors.containsAll(obtainedErrors));
		}

		private static Set<TestMethodName> matchNames(
						ClassMethodNameOracle oracle,
						List<TestMethodName> names)
		{
			return names
				.stream()
				.filter(Support.<ClassMethodNameOracle,
						Predicate<TestMethodName>>
								partialApply(
					oracle_ -> name -> oracle_
						.isRequestedMethod(
							name.toString()),
					oracle))
				.collect(Collectors.toUnmodifiableSet());
		}

		static Map<TestMethodName, Result> objectives(
						ClassMethodNameOracle oracle,
								int instances)
		{
			final Set<TestMethodName> firstFailure = matchNames(
							oracle,
							firstFailure());
			final Set<TestMethodName> firstSuccess = matchNames(
							oracle,
							firstSuccess());
			final Set<TestMethodName> secondFailure = matchNames(
							oracle,
							secondFailure());
			final Set<TestMethodName> secondSuccess = matchNames(
							oracle,
							secondSuccess());
			final Map<TestMethodName, Result> objectives =
							new EnumMap<>(RESULTS);

			if (!firstFailure.isEmpty() || !firstSuccess.isEmpty()) {
				final int failureSize = firstFailure.size();
				final int successSize = firstSuccess.size();
				final List<Throwable> testErrors =
							new LinkedList<>();

				if (firstFailure.contains(TF1_0))
					testErrors.addAll(
						Collections.nCopies(
							instances,
							ERRORS.get(TF1_0)));

				if (firstFailure.contains(TF1_1))
					testErrors.addAll(
						Collections.nCopies(
							instances,
							ERRORS.get(TF1_1)));

				final Map.Entry<MethodKind, List<Throwable>>
								entry1 =
						Map.entry(MethodKind.SET_UP,
					Collections.nCopies(
						(failureSize + successSize)
								* instances,
						ERRORS.get(UP_0)));
				final Map.Entry<MethodKind, List<Throwable>>
								entry2 =
						Map.entry(MethodKind.TEAR_DOWN,
					Collections.nCopies(
						(failureSize + successSize)
								* instances,
						ERRORS.get(DOWN_0)));
				objectives.put(TEST_1_PREFIX,
						RESULTABLE.newTestResult(
					TEST_1_PREFIX.toString(),
						Collections.unmodifiableMap(
								new EnumMap<>(
							(testErrors.isEmpty())
					? Map.ofEntries(entry1, entry2)
					: Map.ofEntries(
						Map.entry(MethodKind.TEST,
							List.copyOf(testErrors)),
						entry1,
						entry2))),
					failureSize * instances,
					successSize * instances));
			}

			if (!secondFailure.isEmpty() || !secondSuccess.isEmpty()) {
				final int failureSize = secondFailure.size();
				final int successSize = secondSuccess.size();
				final List<Throwable> testErrors =
							new LinkedList<>();

				if (secondFailure.contains(TF2_0))
					testErrors.addAll(
						Collections.nCopies(
							instances,
							ERRORS.get(TF2_0)));

				if (secondFailure.contains(TF2_1))
					testErrors.addAll(
						Collections.nCopies(
							instances,
							ERRORS.get(TF2_1)));

				if (secondFailure.contains(TF2_2))
					testErrors.addAll(
						Collections.nCopies(
							instances,
							ERRORS.get(TF2_2)));

				final Map.Entry<MethodKind, List<Throwable>>
								entry1 =
						Map.entry(MethodKind.SET_UP,
					Collections.nCopies(
						(failureSize + successSize)
								* instances,
						ERRORS.get(UP_0)));
				final Map.Entry<MethodKind, List<Throwable>>
								entry2 =
						Map.entry(MethodKind.TEAR_DOWN,
					Collections.nCopies(
						(failureSize + successSize)
								* instances,
						ERRORS.get(DOWN_0)));
				objectives.put(TEST_2_PREFIX,
						RESULTABLE.newTestResult(
					TEST_2_PREFIX.toString(),
						Collections.unmodifiableMap(
								new EnumMap<>(
							(testErrors.isEmpty())
					? Map.ofEntries(entry1, entry2)
					: Map.ofEntries(
						Map.entry(MethodKind.TEST,
							List.copyOf(testErrors)),
						entry1,
						entry2))),
					failureSize * instances,
					successSize * instances));
			}

			return Collections.unmodifiableMap(objectives);
		}

		static boolean failing(ClassMethodNameOracle oracle)
		{
			return (oracle.isRequestedMethod(TF1_0.toString())
				|| oracle.isRequestedMethod(TF1_1.toString())
				|| oracle.isRequestedMethod(TF2_0.toString())
				|| oracle.isRequestedMethod(TF2_1.toString())
				|| oracle.isRequestedMethod(TF2_2.toString()));
		}
	}

	static class BaseTests
	{
		public static void setUpAll0()
		{
			throw ERRORS.get(UP_ALL_0);
		}

		public static void tearDownAll0()
		{
			throw ERRORS.get(DOWN_ALL_0);
		}

		public static void setUpBatch0()
		{
			throw ERRORS.get(UP_BATCH_0);
		}

		public static void tearDownBatch0()
		{
			throw ERRORS.get(DOWN_BATCH_0);
		}

		public void setUp0()	{ throw ERRORS.get(UP_0); }
		public void tearDown0()	{ throw ERRORS.get(DOWN_0); }

		/*
		 * XXX: Whenever test* methods are added or removed, or their
		 * prefix is changed, always review TestMethodName#{ERRORS,RESULTS}
		 * and TestMethodName#objectives(ClassMethodNameOracle, int).
		 */
		public void testFirstSuccess0() { }
		public void testFirstSuccess1() { }
		public void testFirstSuccess2() { }
		public void testFirstSuccess3() { }
		public void testFirstSuccess4() { }
		public void testSecondSuccess0() { }
		public void testSecondSuccess1() { }
		public void testSecondSuccess2() { }
		public void testSecondSuccess3() { }

		public void testFirstFailure0() { throw ERRORS.get(TF1_0); }
		public void testFirstFailure1() { throw ERRORS.get(TF1_1); }
		public void testSecondFailure0() { throw ERRORS.get(TF2_0); }
		public void testSecondFailure1() { throw ERRORS.get(TF2_1); }
		public void testSecondFailure2() { throw ERRORS.get(TF2_2); }
	}

	static class SuccessTests extends BaseTests implements Testable
	{
		public void testFirstSuccess_() { }
		public void testSecondSuccess_() { }
	}

	static <T extends Testable> Instantiable<T> newInstantiable(
					ExecutionPolicy executionPolicy,
					Reportable reportable,
					Set<Supplier<? extends T>> testables)
	{
		return Instantiable
			.<T>newBuilder(testables)
			.executionPolicy(executionPolicy)
			.prefixables(new LinkedHashSet<>(List.of(
				new DefaultPrefix(TEST_1_PREFIX.toString()),
				new DefaultPrefix(TEST_2_PREFIX.toString()))))
			.printStream(new PrintStream(
					OutputStream.nullOutputStream()))
			.reportable(AteTester.META_REPORTER
				.apply(reportable))
			.build();
	}

	static <T> Function<String, Consumer<T>> messager()
	{
		return format -> object -> System.err.format(format,
							object.toString());
	}

	static Function<Map<Class<? extends Result>,
					Function<Concurrent,
					Function<Map<TestMethodName, Result>,
					Predicate<Result>>>>,
				Function<Consumer<Result>,
				Function<Concurrent,
				Function<Map<TestMethodName, Result>,
				Reportable>>>> resultor()
	{
		return strategy -> messager -> concurrent -> objectives ->
					stream -> testClass -> result -> {
			messager.accept(result);
			assert Objects.requireNonNull(strategy.get(
							result.getClass()),
						"strategy")
				.apply(concurrent)
				.apply(objectives)
				.test(result);
		};
	}

	static class AbortOnFailureTests extends SuccessTests
	{
		static boolean failing3 = false;

		private static final Function<Map<TestMethodName, Result>,
								Reportable>
							ABORTING_RESULTOR3 =
						TestFailureNidus.resultor()
			.apply(TestMethodName.ABORTING_STRATEGY)
			.apply(TestFailureNidus.<Result>messager()
				.apply("AbortOnFailureTests: %s%n"))
			.apply(Concurrent.FALSE);
		private static final AbortOnFailureTests INSTANCE =
						new AbortOnFailureTests();

		public static Instantiable<AbortOnFailureTests> instantiable()
		{
			final ClassMethodNameOracle oracle =
							ClassMethodNameOracle
				.newInstance(AbortOnFailureTests.class);
			failing3 = TestMethodName.failing(oracle);
			return TestFailureNidus.newInstantiable(
				ExecutionPolicy.SEQUENTIAL,
				ABORTING_RESULTOR3
					.apply(TestMethodName.objectives(oracle,
									2)),
				Set.of(() -> INSTANCE, () -> INSTANCE));
		}
	}

	static class ConcurrentAbortOnFailureTests extends AbortOnFailureTests
	{
		static boolean failing2 = false;

		private static final Function<Map<TestMethodName, Result>,
								Reportable>
							ABORTING_RESULTOR2 =
						TestFailureNidus.resultor()
			.apply(TestMethodName.ABORTING_STRATEGY)
			.apply(TestFailureNidus.<Result>messager()
				.apply("ConcurrentAbortOnFailureTests: %s%n"))
			.apply(Concurrent.TRUE);
		private static final ConcurrentAbortOnFailureTests INSTANCE =
					new ConcurrentAbortOnFailureTests();

		public static Instantiable<AbortOnFailureTests> instantiable()
		{
			final ClassMethodNameOracle oracle =
							ClassMethodNameOracle
				.newInstance(ConcurrentAbortOnFailureTests.class);
			failing2 = TestMethodName.failing(oracle);
			return TestFailureNidus.newInstantiable(
				ExecutionPolicy.CONCURRENT,
				ABORTING_RESULTOR2
					.apply(TestMethodName.objectives(oracle,
									2)),
				Set.of(() -> INSTANCE, () -> INSTANCE));
		}
	}

	static class ContinueOnFailureTests extends SuccessTests
	{
		static boolean failing1 = false;

		private static final Function<Map<TestMethodName, Result>,
								Reportable>
							CONTINUING_RESULTOR1 =
						TestFailureNidus.resultor()
			.apply(TestMethodName.CONTINUING_STRATEGY)
			.apply(TestFailureNidus.<Result>messager()
				.apply("ContinueOnFailureTests: %s%n"))
			.apply(Concurrent.FALSE);
		private static final ContinueOnFailureTests INSTANCE =
						new ContinueOnFailureTests();

		public static Instantiable<ContinueOnFailureTests> instantiable()
		{
			final ClassMethodNameOracle oracle =
							ClassMethodNameOracle
				.newInstance(ContinueOnFailureTests.class);
			failing1 = TestMethodName.failing(oracle);
			return TestFailureNidus.newInstantiable(
				ExecutionPolicy.SEQUENTIAL,
				CONTINUING_RESULTOR1
					.apply(TestMethodName.objectives(oracle,
									2)),
				Set.of(() -> INSTANCE, () -> INSTANCE));
		}
	}

	static class ConcurrentContinueOnFailureTests extends
							ContinueOnFailureTests
	{
		static boolean failing0 = false;

		private static final Function<Map<TestMethodName, Result>,
								Reportable>
							CONTINUING_RESULTOR0 =
						TestFailureNidus.resultor()
			.apply(TestMethodName.CONTINUING_STRATEGY)
			.apply(TestFailureNidus.<Result>messager()
				.apply("ConcurrentContinueOnFailureTests: %s%n"))
			.apply(Concurrent.TRUE);
		private static final ConcurrentContinueOnFailureTests INSTANCE =
					new ConcurrentContinueOnFailureTests();

		public static Instantiable<ContinueOnFailureTests> instantiable()
		{
			final ClassMethodNameOracle oracle =
							ClassMethodNameOracle
				.newInstance(ConcurrentContinueOnFailureTests.class);
			failing0 = TestMethodName.failing(oracle);
			return TestFailureNidus.newInstantiable(
				ExecutionPolicy.CONCURRENT,
				CONTINUING_RESULTOR0
					.apply(TestMethodName.objectives(oracle,
									2)),
				Set.of(() -> INSTANCE, () -> INSTANCE));
		}
	}

	@SuppressWarnings("AssertWithSideEffects")
	private static void assertAsserting()
	{
		boolean assertable = false;
		assert assertable = true;

		if (!assertable)
			throw new AssertionError();
	}

	public static void main(String... args)
	{
		success = false;

		/*
		 * The following shenanigans allow to have all (four) tester
		 * factories run and reason about their results (or their
		 * absence), e.g.
		 *
		 * -Date.some=\(?\!.+?TestFailureNidus\)
		 * -Date.some=.+?AbortOnFailureTests@nonPrefix
		 */
		AbortOnFailureTests.failing3 = false;
		ConcurrentAbortOnFailureTests.failing2 = false;
		ContinueOnFailureTests.failing1 = false;
		ConcurrentContinueOnFailureTests.failing0 = false;
		assertAsserting();

		/* Run to completion concurrently. */
		final boolean runAll0 = Tester.newBuilder(Set.of(
					ConcurrentContinueOnFailureTests.class))
			.continueOnFailure()
			.renderOutputPlain()
			.build()
			.runAndReport();
		assert runAll0;

		/* Run to completion sequentially. */
		final boolean runAll1 = Tester.newBuilder(Set.of(
						ContinueOnFailureTests.class))
			.continueOnFailure()
			.renderOutputPlain()
			.build()
			.runAndReport();
		assert runAll1;

		/* Run until failure concurrently. */
		final boolean runAll2 = Tester.newBuilder(Set.of(
					ConcurrentAbortOnFailureTests.class))
			.renderOutputPlain()
			.build()
			.runAndReport();
		assert (ConcurrentAbortOnFailureTests.failing2)
			? !runAll2
			: runAll2;

		/* Run until failure sequentially. */
		final boolean runAll3 = Tester.newBuilder(Set.of(
						AbortOnFailureTests.class))
			.renderOutputPlain()
			.build()
			.runAndReport();
		assert (AbortOnFailureTests.failing3)
			? !runAll3
			: runAll3;

		if (AbortOnFailureTests.failing3
				|| ConcurrentAbortOnFailureTests.failing2
				|| ContinueOnFailureTests.failing1
				|| ConcurrentContinueOnFailureTests.failing0)
			TestFailureNidus.<String>messager()
				.apply("%n%s: \033[1;7;32mPASSED\033[0m%n")
				.accept(TestFailureNidus.class
							.getCanonicalName());

		success = true;
	}
}
