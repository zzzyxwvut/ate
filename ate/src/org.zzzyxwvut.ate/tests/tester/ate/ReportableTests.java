package tester.ate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;

import org.zzzyxwvut.ate.Result.Reportable.IncompleteReport;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.ResultFriend.Resultable;
import org.zzzyxwvut.ate.internal.ResultFriend;

class ReportableTests implements Testable
{
	private static final Resultable RESULTABLE = ResultFriend
							.getResultable();
	private static final PrintStream NULL_STREAM = new PrintStream(
					OutputStream.nullOutputStream());
	private static final Class<? extends Testable> THIS_CLASS =
						ReportableTests.class;

	Result otherResult;
	Result testResult;
	Result setUpAllResult;
	Result tearDownAllResult;
	Result setUpBatchResult;
	Result tearDownBatchResult;

	public void setUp()
	{
		/*
		 * Note that the _reports_ queue must be locally-scoped in
		 * order to make the test methods follow the concurrent
		 * execution policy.
		 */
		otherResult = RESULTABLE.newOtherResult("");
		testResult = RESULTABLE.newTestResult("", Map.of(), 0, 0);
		setUpAllResult = RESULTABLE.newSetUpAllResult(Map.of());
		tearDownAllResult = RESULTABLE.newTearDownAllResult(Map.of());
		setUpBatchResult = RESULTABLE.newSetUpBatchResult("",
								Map.of());
		tearDownBatchResult = RESULTABLE.newTearDownBatchResult("",
								Map.of());
	}

	public void testPassingWithAllReporters()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		reporter.accept(otherResult);
		assert reports.isEmpty();

		reporter.accept(testResult);
		assert reports.isEmpty();

		reporter.accept(setUpAllResult);
		assert reports.isEmpty();

		reporter.accept(tearDownAllResult);
		assert reports.isEmpty();

		reporter.accept(setUpBatchResult);
		assert reports.isEmpty();

		reporter.accept(tearDownBatchResult);
		assert reports.isEmpty();
	}

	public void testCatchingWithSetUpAllAndTearDownAllReporters()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.setUpAllResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException(
							"setUpAll");
			})
			.tearDownAllResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException(
							"tearDownAll");
			})
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		reporter.accept(setUpAllResult);
		assert !reports.isEmpty();
		final IncompleteReport setUpAllReport = reports.poll();
		assert reports.isEmpty()
			&& !setUpAllReport.printStreamInTrouble()
			&& "setUpAll".equals(setUpAllReport
						.reportingException()
						.getMessage())
			&& setUpAllReport.testClass() == THIS_CLASS
			&& setUpAllReport.result() == setUpAllResult;

		reporter.accept(tearDownAllResult);
		assert !reports.isEmpty();
		final IncompleteReport tearDownAllReport = reports.poll();
		assert reports.isEmpty()
			&& !tearDownAllReport.printStreamInTrouble()
			&& "tearDownAll".equals(tearDownAllReport
						.reportingException()
						.getMessage())
			&& tearDownAllReport.testClass() == THIS_CLASS
			&& tearDownAllReport.result() == tearDownAllResult;

		reporter.accept(otherResult);
		assert reports.isEmpty();

		reporter.accept(testResult);
		assert reports.isEmpty();

		reporter.accept(setUpBatchResult);
		assert reports.isEmpty();

		reporter.accept(tearDownBatchResult);
		assert reports.isEmpty();
	}

	public void testCatchingWithSetUpBatchAndTearDownBatchReporters()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.setUpBatchResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException(
							"setUpBatch");
			})
			.tearDownBatchResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException(
							"tearDownBatch");
			})
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		reporter.accept(setUpBatchResult);
		assert !reports.isEmpty();
		final IncompleteReport setUpBatchReport = reports.poll();
		assert reports.isEmpty()
			&& !setUpBatchReport.printStreamInTrouble()
			&& "setUpBatch".equals(setUpBatchReport
						.reportingException()
						.getMessage())
			&& setUpBatchReport.testClass() == THIS_CLASS
			&& setUpBatchReport.result() == setUpBatchResult;

		reporter.accept(tearDownBatchResult);
		assert !reports.isEmpty();
		final IncompleteReport tearDownBatchReport = reports.poll();
		assert reports.isEmpty()
			&& !tearDownBatchReport.printStreamInTrouble()
			&& "tearDownBatch".equals(tearDownBatchReport
						.reportingException()
						.getMessage())
			&& tearDownBatchReport.testClass() == THIS_CLASS
			&& tearDownBatchReport.result() == tearDownBatchResult;

		reporter.accept(otherResult);
		assert reports.isEmpty();

		reporter.accept(testResult);
		assert reports.isEmpty();

		reporter.accept(setUpAllResult);
		assert reports.isEmpty();

		reporter.accept(tearDownAllResult);
		assert reports.isEmpty();
	}

	public void testCatchingWithTestReporter()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.testResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException("test");
			})
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		reporter.accept(testResult);
		assert !reports.isEmpty();
		final IncompleteReport testReport = reports.poll();
		assert reports.isEmpty()
			&& !testReport.printStreamInTrouble()
			&& "test".equals(testReport
						.reportingException()
						.getMessage())
			&& testReport.testClass() == THIS_CLASS
			&& testReport.result() == testResult;

		reporter.accept(otherResult);
		assert reports.isEmpty();

		reporter.accept(setUpAllResult);
		assert reports.isEmpty();

		reporter.accept(tearDownAllResult);
		assert reports.isEmpty();

		reporter.accept(setUpBatchResult);
		assert reports.isEmpty();

		reporter.accept(tearDownBatchResult);
		assert reports.isEmpty();
	}

	public void testCatchingWithOtherReporter()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.otherResultReporter(stream -> testClass ->
								result -> {
				throw new IllegalArgumentException("other");
			})
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		reporter.accept(otherResult);
		assert !reports.isEmpty();
		final IncompleteReport otherReport = reports.poll();
		assert reports.isEmpty()
			&& !otherReport.printStreamInTrouble()
			&& "other".equals(otherReport
						.reportingException()
						.getMessage())
			&& otherReport.testClass() == THIS_CLASS
			&& otherReport.result() == otherResult;

		reporter.accept(testResult);
		assert reports.isEmpty();

		reporter.accept(setUpAllResult);
		assert reports.isEmpty();

		reporter.accept(tearDownAllResult);
		assert reports.isEmpty();

		reporter.accept(setUpBatchResult);
		assert reports.isEmpty();

		reporter.accept(tearDownBatchResult);
		assert reports.isEmpty();
	}

	@SuppressWarnings("ThrowableResultIgnored")
	public void testCatchingUnsupportedResult()
	{
		final Queue<IncompleteReport> reports = new ArrayDeque<>();
		final Consumer<Result> reporter = Reportable
			.newBuilder(reports)
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);
		final Result unsupportedResult = new UnsupportedResult();

		reporter.accept(unsupportedResult);
		assert !reports.isEmpty();
		final IncompleteReport unsupportedReport = reports.poll();
		assert reports.isEmpty()
			&& !unsupportedReport.printStreamInTrouble()
			&& unsupportedReport.reportingException() instanceof
						UnsupportedOperationException
			&& unsupportedReport.testClass() == THIS_CLASS
			&& unsupportedReport.result() == unsupportedResult;
	}

	public void testPropagatingUnsupportedOperationException()
	{
		final Consumer<Result> reporter = Reportable
			.newBuilder()
			.build()
			.apply(NULL_STREAM)
			.apply(THIS_CLASS);

		try {
			reporter.accept(new UnsupportedResult());
		} catch (final UnsupportedOperationException expected) {
			return;
		}

		throw new AssertionError();
	}

	@SuppressWarnings("try")
	public void testObservingClosedPrintStreamWithTestReporter()
	{
		class BespokePrintStream extends PrintStream
		{
			BespokePrintStream(OutputStream out,
						boolean autoFlush,
						Charset charset)
			{
				super(out, autoFlush, charset);
			}

			@Override
			protected void clearError() { super.clearError(); }
		}

		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new BespokePrintStream(
						baos,
						true,
						StandardCharsets.UTF_8)) {
			final Queue<IncompleteReport> reports =
							new ArrayDeque<>();
			final Consumer<Result> reporter = Reportable
				.newBuilder(reports)
				.testResultReporter(stream -> testClass ->
								result -> {
					stream.print(result.toString());

					if (stream.checkError()) {
						((BespokePrintStream) stream)
							.clearError();
						throw new IllegalStateException(
							"Input/Output failure");
					}
				})
				.build()
				.apply(ps)
				.apply(THIS_CLASS);

			reporter.accept(testResult);
			assert reports.isEmpty()
				&& testResult.toString()
					.equals(new String(
						baos.toByteArray(),
						StandardCharsets.UTF_8));
			baos.reset();

			/* Flip the _trouble_ via PrintStream#ensureOpen(). */
			ps.close();
			reporter.accept(testResult);
			assert !reports.isEmpty();
			final IncompleteReport testReport = reports.poll();
			assert reports.isEmpty()
				&& !testReport.printStreamInTrouble()
				&& "Input/Output failure".equals(testReport
							.reportingException()
							.getMessage())
				&& testReport.testClass() == THIS_CLASS
				&& testReport.result() == testResult;
		} catch (final IOException cause) {
			throw new AssertionError("Input/Output failure",
								cause);
		}
	}

	private static class UnsupportedResult extends Result
	{
		UnsupportedResult()		{ super(Map.of()); }

		@Override
		protected Optional<String> summary(String templet)
		{
			return Optional.empty();
		}

		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.empty();
		}
	}
}
