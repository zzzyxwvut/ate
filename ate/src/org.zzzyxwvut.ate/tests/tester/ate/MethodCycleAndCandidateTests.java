package tester.ate;

import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.ClassMethodNameOracle;

import tester.ate.LifeCycleMethodResultRegistry.Reporters;

class MethodCycleAndCandidateTests implements Testable, Status
{
	private static final Queue<String> ALL_TALLY =
						new ConcurrentLinkedDeque<>();
	private static final Queue<String> BATCH_TALLY =
						new ConcurrentLinkedDeque<>();
	private static final Queue<String> TALLY =
						new ConcurrentLinkedDeque<>();
	private static final MethodCycleAndCandidateTests INSTANCE =
					new MethodCycleAndCandidateTests();

	private static volatile boolean supported = false;

	private static boolean testWithAllSupportedModifiers = false;

	public static void setUpAll()		{ ALL_TALLY.offer("none"); }
	public static void setUpAll(String any)	{ ALL_TALLY.offer(any); }

	public static void tearDownAll()
	{
		try {
			if (testWithAllSupportedModifiers) {
				try {
					assert supported : "Skipped a \"public final"
						+ " synchronized strictfp\" method";
				} catch (final AssertionError e) {
					throw new DeferredAssertionError(e);
				}
			}
		} finally {
			testWithAllSupportedModifiers = false;
			supported = false;
		}
	}

	public static void tearDownAll(String stub, Set<String> expected)
	{
		ALL_TALLY.removeAll(expected);

		try {
			assert BATCH_TALLY.isEmpty() : BATCH_TALLY.size();
			assert ALL_TALLY.isEmpty() : ALL_TALLY.size();
		} catch (final AssertionError e) {
			throw new DeferredAssertionError(e);
		}
	}

	public static void setUpBatch()		{ BATCH_TALLY.offer("none"); }

	public static void tearDownBatch()
	{
		BATCH_TALLY.poll();

		try {
			assert TALLY.isEmpty() : TALLY.size();
		} catch (final AssertionError e) {
			throw new DeferredAssertionError(e);
		}
	}

	public void setUp()			{ TALLY.offer("none"); }
	public void setUp(String any)		{ TALLY.offer(any); }
	public void tearDown()			{ TALLY.poll(); }
	public void tearDown(String stub)	{ TALLY.poll(); }

	public void testConfirmingSetUpAll(Queue<String> expected)
	{
		assert expected.containsAll(ALL_TALLY) : String.format(
								"%s != %s",
								expected,
								ALL_TALLY);
	}

	public void testConfirmingSetUpBatch(Queue<String> expected)
	{
		assert expected.containsAll(BATCH_TALLY) : String.format(
								"%s != %s",
								expected,
								BATCH_TALLY);
	}

	public void testConfirmingSetUp(Queue<String> expected)
	{
		assert expected.containsAll(TALLY) : String.format(
								"%s != %s",
								expected,
								TALLY);
	}

	public void testVariableArgumentPassing(int expected,
							Integer... values)
	{	/* Null cannot be unboxed (JLS-11, §5.1.8). */
		assert (values == null) || expected == values.length :
			String.format("variable arguments: %s",
							(values != null)
						? Arrays.toString(values)
						: "<null>");
	}

	/* See https://openjdk.java.net/jeps/306. */
	@SuppressWarnings({ "BoxedValueEquality", "NumberEquality",
								"strictfp" })
	public final synchronized strictfp void testWithAllSupportedModifiers()
	{
		supported = true;
		assert Double.valueOf("NaN") != Double.valueOf("NaN");
	}

	@Override
	public PrintStream printStream()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Optional<String> testMethodNamePrefix()
	{
		throw new UnsupportedOperationException();
	}

	public static void testNotStaticMethod() { throw new AssertionError(); }

	static void setUpAllZ()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	static void tearDownAllZ()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	static void setUpZ()			{ throw new AssertionError(); }
	static void tearDownZ()			{ throw new AssertionError(); }
	public static void setUpY()		{ throw new AssertionError(); }
	public static void tearDownY()		{ throw new AssertionError(); }

	public native void testNotNativeMethod();

	public void setUpAllY()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	public void tearDownAllY()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	public void setUpBatchY()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	public void tearDownBatchY()
	{
		throw new DeferredAssertionError(new AssertionError());
	}

	public void doesNotStartWithTest()	{ throw new AssertionError(); }

	protected void testNotProtectedMethod()	{ throw new AssertionError(); }
	void testNotPackagePrivateMethod()	{ throw new AssertionError(); }
	private void testNotPrivateMethod()	{ throw new AssertionError(); }

	public static Instantiable<MethodCycleAndCandidateTests> instantiable()
	{
		testWithAllSupportedModifiers = ClassMethodNameOracle
			.newInstance(MethodCycleAndCandidateTests.class)
			.isRequestedMethod("testWithAllSupportedModifiers");
		return Instantiable.<MethodCycleAndCandidateTests>newBuilder(
								Set.of(
							() -> INSTANCE,
							() -> INSTANCE))
			.arguments(arguments())
			.executionPolicy(ExecutionPolicy.CONCURRENT)
			.prefixables(Set.of(new DefaultPrefix("test"),
				new DefaultPrefix("printStream"),
				new DefaultPrefix("testMethodNamePrefix")))
			.reportable(AteTester.TEST_REPORTER
				.apply(Reportable.newBuilder()
					.otherResultReporter(
						Reporters.otherResultReporter()
							.apply(OtherResult
								.RENDITION_REVERSE_VIDEO))
					.testResultReporter(
						Reporters.testResultReporter()
							.apply(TestResult
								.RENDITION_RED_YELLOW_GREEN))

					/* Against setUpAll[YZ]. */
					.setUpAllResultReporter(
						Reporters.deferredSetUpAllResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))
					.tearDownAllResultReporter(
						Reporters.deferredTearDownAllResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))

					/* Against setUpBatchY. */
					.setUpBatchResultReporter(
						Reporters.deferredSetUpBatchResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))
					.tearDownBatchResultReporter(
						Reporters.deferredTearDownBatchResultReporter()
							.apply(AteTester
								.DEFERRED_RESULT_REGISTRY))
					.build()))
			.build();
	}

	private static Map<String, Collection<List<?>>> arguments()
	{
		final List<Integer> none = new ArrayList<>(2);
		none.add(1);
		none.add(null);
		final List<Integer> nonePair = new ArrayList<>(3);
		nonePair.add(2);
		nonePair.add(null);
		nonePair.add(null);
		return Map.of("testVariableArgumentPassing",
				Set.of(none,
					nonePair,
					List.of(0),
					List.of(1, 1),
					List.of(2, 1, 2),
					List.of(3, 1, 2, 3)),
			"testConfirmingSetUpAll",
				Set.of(List.of(new ArrayDeque<>(
						List.of("set once",
							"set twice",
							"none")))),
			"testConfirmingSetUpBatch",
				Set.of(List.of(new ArrayDeque<>(
						List.of("none")))),
			"testConfirmingSetUp",
				Set.of(List.of(new ArrayDeque<>(
						List.of("set each once",
							"set each twice",
							"none")))),
			/* These are excluded from the test count. */
			"setUpAll",
				List.of(List.of("set once"),
					List.of("set twice")),
			"tearDownAll",
				List.of(List.of("for good",
					Set.of("set once",
						"set twice",
						"none"))),
			"setUp",
				List.of(List.of("set each once"),
					List.of("set each twice")),
			"tearDown",
				List.of(List.of("tear each once"),
					List.of("tear each twice")));
	}
}
