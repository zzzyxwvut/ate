/**
 * Defines testing facilities.
 *
 * @uses org.zzzyxwvut.ate.support.AccessibleUplooker
 */
module org.zzzyxwvut.ate
{
	exports org.zzzyxwvut.ate.configuration;
	exports org.zzzyxwvut.ate.support;
	exports org.zzzyxwvut.ate;

	uses org.zzzyxwvut.ate.support.AccessibleUplooker;
}
