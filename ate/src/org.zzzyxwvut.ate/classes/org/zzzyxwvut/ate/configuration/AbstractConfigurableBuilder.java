package org.zzzyxwvut.ate.configuration;

import java.io.PrintStream;
import java.util.Set;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.configuration.Configurable.Prefixable;

/**
 * A skeletal implementation of {@code Configurable.Builder}.
 * <p>
 * This class allows {@code null}-valued properties.
 *
 * @param <A> an {@code AbstractConfigurableBuilder}-derived class
 * @param <B> a {@code Configurable}
 */
abstract class AbstractConfigurableBuilder<A extends
					AbstractConfigurableBuilder<A, B>,
							B extends Configurable>
			implements Configurable.Builder<A, B>
{
	/** An execution policy for running the prefix-matching test methods. */
	ExecutionPolicy executionPolicy;

	/** A set of prefixes for method names to match. */
	Set<? extends Prefixable> prefixables;

	/** A stream to use for output. */
	PrintStream printStream;

	/** A reportable that shares some result or its absence. */
	Reportable reportable;

	/** Constructs a new {@code AbstractConfigurableBuilder} object. */
	AbstractConfigurableBuilder() { }

	/**
	 * Returns {@code this} instance.
	 *
	 * @return {@code this} instance
	 */
	abstract A self();

	@Override
	public A executionPolicy(ExecutionPolicy executionPolicy)
	{
		this.executionPolicy = executionPolicy;
		return self();
	}

	@Override
	public A prefixables(Set<? extends Prefixable> prefixables)
	{
		this.prefixables = prefixables;
		return self();
	}

	@Override
	public A printStream(PrintStream printStream)
	{
		this.printStream = printStream;
		return self();
	}

	@Override
	public A reportable(Reportable reportable)
	{
		this.reportable = reportable;
		return self();
	}
}
