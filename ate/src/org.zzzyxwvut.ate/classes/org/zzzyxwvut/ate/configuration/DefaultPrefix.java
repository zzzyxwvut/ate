package org.zzzyxwvut.ate.configuration;

import java.util.Objects;
import java.util.Set;

/** A default {@code Prefixable}. */
public class DefaultPrefix implements Configurable.Prefixable
{
	private final String testMethodNamePrefix;
	private final Set<String> lifeCycleMethodNamePrefixes;

	/**
	 * Constructs a new {@code DefaultPrefix} object.
	 *
	 * @param testMethodNamePrefix a prefix for test method names to match
	 * @param lifeCycleMethodNamePrefixes a set of prefixes for life cycle
	 *	method names to match
	 */
	public DefaultPrefix(String testMethodNamePrefix,
				Set<String> lifeCycleMethodNamePrefixes)
	{
		this.testMethodNamePrefix = Objects.requireNonNull(
						testMethodNamePrefix,
						"testMethodNamePrefix");
		this.lifeCycleMethodNamePrefixes = Objects.requireNonNull(
						lifeCycleMethodNamePrefixes,
						"lifeCycleMethodNamePrefixes");
	}

	/**
	 * Constructs a new {@code DefaultPrefix} object, using an empty set
	 * of prefixes for life cycle method names to match.
	 *
	 * @param testMethodNamePrefix a prefix for test method names to match
	 */
	public DefaultPrefix(String testMethodNamePrefix)
	{
		this(testMethodNamePrefix, Set.of());
	}

	@Override
	public String testMethodNamePrefix()
	{
		return testMethodNamePrefix;
	}

	@Override
	public Set<String> lifeCycleMethodNamePrefixes()
	{
		return lifeCycleMethodNamePrefixes;
	}

	@Override
	public String toString()
	{
		final String prefixes = lifeCycleMethodNamePrefixes.toString();
		return new StringBuilder(4 + testMethodNamePrefix.length()
							+ prefixes.length())
			.append("(")
			.append(testMethodNamePrefix)
			.append(", ")
			.append(prefixes)
			.append(")")
			.toString();
	}
}
