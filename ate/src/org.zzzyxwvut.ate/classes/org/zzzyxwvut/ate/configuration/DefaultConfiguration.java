package org.zzzyxwvut.ate.configuration;

import java.io.PrintStream;
import java.util.Set;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;

/**
 * A default {@code Configurable}.
 * <p>
 * This class allows {@code null}-valued properties.
 */
class DefaultConfiguration implements Configurable
{
	private final ExecutionPolicy executionPolicy;
	private final Set<? extends Prefixable> prefixables;
	private final PrintStream printStream;
	private final Reportable reportable;

	/**
	 * Constructs a new {@code DefaultConfiguration} object.
	 *
	 * @param executionPolicy an execution policy for running
	 *	the prefix-matching test methods
	 * @param prefixables a set of prefixes for method names to match
	 * @param printStream a stream to use for output
	 * @param reportable a reportable that shares some result or its
	 *	absence
	 */
	DefaultConfiguration(ExecutionPolicy executionPolicy,
					Set<? extends Prefixable> prefixables,
					PrintStream printStream,
					Reportable reportable)
	{
		this.executionPolicy = executionPolicy;
		this.prefixables = prefixables;
		this.printStream = printStream;
		this.reportable = reportable;
	}

	@Override
	public ExecutionPolicy executionPolicy()
	{
		return executionPolicy;
	}

	@Override
	public Set<? extends Prefixable> prefixables()	{ return prefixables; }

	@Override
	public PrintStream printStream()		{ return printStream; }

	@Override
	public Reportable reportable()			{ return reportable; }

	@Override
	public String toString()
	{
		return String.format("%s, %s, %s, %s",
					executionPolicy(), prefixables(),
					printStream(), reportable());
	}
}
