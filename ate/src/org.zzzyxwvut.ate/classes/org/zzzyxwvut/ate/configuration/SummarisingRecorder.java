package org.zzzyxwvut.ate.configuration;

import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.Support;

/**
 * This class defines a means to defer immediate publishing of obtained method
 * execution results and offers a way to process such results as a unit at
 * client's convenience.
 *
 * @see org.zzzyxwvut.ate.Result.Reportable
 */
public final class SummarisingRecorder
{
	private SummarisingRecorder() { /* No instantiation. */ }

	private static final Map<Class<? extends Result>, MethodKind>
							RESULT_KINDS = Map.of(
		Result.SetUpAllResult.class,		MethodKind.SET_UP_ALL,
		Result.TearDownAllResult.class,		MethodKind.TEAR_DOWN_ALL,
		Result.SetUpBatchResult.class,		MethodKind.SET_UP_BATCH,
		Result.TearDownBatchResult.class,	MethodKind.TEAR_DOWN_BATCH,
		Result.OtherResult.class,		MethodKind.OTHER,
		Result.TestResult.class,		MethodKind.TEST);

	/**
	 * Returns a functional interface that takes a functional interface
	 * that takes a test class and returns a new queue for this test class
	 * to collect method execution results in, and returns a functional
	 * interface that takes a mapping of test classes to queues of results
	 * and returns a functional interface that takes a print stream and
	 * returns a functional interface that takes a test class and returns
	 * a functional interface that takes some result and attempts to insert
	 * the applied result into a resolved queue, either existing or new,
	 * associated with the applied test class and does not return a value.
	 *
	 * @apiNote
	 * The value function of this function must be applied to a concurrent
	 * map for a concurrent class-testing execution policy.
	 * <p>
	 * For example,
	 * <blockquote><pre><code>
	 * final Map&lt;Class&lt;? extends Testable&gt;, Queue&lt;Result&gt;&gt; tally =
	 *					new ConcurrentHashMap&lt;&gt;();
	 * final Reportable recorder = SummarisingRecorder.recorder()
	 *	.apply(key -&gt; new ArrayDeque&lt;&gt;())
	 *	.apply(tally));
	 * </code></pre></blockquote>
	 *
	 * @return a curried function
	 * @see org.zzzyxwvut.ate.configuration
	 */
	public static Function<Function<Class<? extends Testable>,
							Queue<Result>>,
				Function<Map<Class<? extends Testable>,
							Queue<Result>>,
				Reportable>> recorder()
	{
		return valuer -> tally ->
				stream -> testClass -> result -> tally
			.computeIfAbsent(testClass, valuer)
			.offer(result);
	}

	/**
	 * Transforms the passed map of test classes to queues of method
	 * execution results into an unmodifiable minute view of a map of test
	 * classes to maps of method-name prefixes to maps of method kinds to
	 * lists of results.
	 *
	 * @param tally a map of test classes to queues of method execution
	 *	results
	 * @return an unmodifiable minute view of a map of method execution
	 *	results
	 * @see #recorder()
	 */
	public static Map<Class<? extends Testable>,
				Map<String, Map<MethodKind, List<Result>>>>
								summarise(
			Map<Class<? extends Testable>, Queue<Result>> tally)
	{
		Objects.requireNonNull(tally, "tally");
		return Collections.unmodifiableMap(tally
			.entrySet()
			.stream()
			.collect(Collectors.toMap(
				Map.Entry::getKey,
				entry -> Collections.unmodifiableMap(entry
					.getValue()
					.stream()
					.collect(Collectors.groupingBy(
						result -> result
							.testMethodNamePrefix()
							.orElse(""), /* *All */
						LinkedHashMap::new,
						Collectors.collectingAndThen(
							Collectors.groupingBy(
								result -> RESULT_KINDS
										.get(
										result
									.getClass()),
								() -> new EnumMap<>(
									MethodKind.class),
								Collectors.toUnmodifiableList()),
							Collections::unmodifiableMap)))),
				Support.uoeThrower(),	/* Class keys. */
				LinkedHashMap::new)));
	}
}
