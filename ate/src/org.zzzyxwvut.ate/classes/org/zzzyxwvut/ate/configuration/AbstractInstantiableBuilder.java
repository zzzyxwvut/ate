package org.zzzyxwvut.ate.configuration;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Testable;

/**
 * A skeletal implementation of {@code Instantiable.Builder}.
 *
 * @param <A> an {@code AbstractInstantiableBuilder}-derived class
 * @param <B> a {@code Configurable}
 * @param <C> a {@code Testable}
 */
abstract class AbstractInstantiableBuilder<A extends
					AbstractInstantiableBuilder<A, B, C>,
							B extends Configurable,
							C extends Testable>
			extends AbstractConfigurableBuilder<A, B>
			implements Instantiable.Builder<A, B, C>
{
	/** A set of test class instance suppliers. */
	final Set<Supplier<? extends C>> testables;

	/**
	 * A map of arguments, where the keys are the names of public test and
	 * life cycle methods with declared parameters and the values are
	 * a series of their arguments.
	 */
	Map<String, Collection<List<?>>> arguments = Map.of();

	/**
	 * Constructs a new {@code AbstractInstantiableBuilder} object.
	 *
	 * @param testables a set of test class instance suppliers
	 */
	AbstractInstantiableBuilder(Set<Supplier<? extends C>> testables)
	{
		this.testables = Objects.requireNonNull(testables, "testables");
	}

	@Override
	public A arguments(Map<String, Collection<List<?>>> arguments)
	{
		this.arguments = Objects.requireNonNull(arguments, "arguments");
		return self();
	}
}
