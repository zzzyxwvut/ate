package org.zzzyxwvut.ate.configuration;

import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;

/**
 * A default {@code Instantiable}.
 *
 * @param <T> a {@code Testable}
 */
class DefaultInstantiation<T extends Testable> extends DefaultConfiguration
						implements Instantiable<T>
{
	private final Set<Supplier<? extends T>> testables;
	private final Map<String, Collection<List<?>>> arguments;

	/**
	 * Constructs a new {@code DefaultInstantiation} object.
	 *
	 * @param executionPolicy an execution policy for running
	 *	the prefix-matching test methods
	 * @param prefixables a set of prefixes for method names to match
	 * @param printStream a stream to use for output
	 * @param reportable a reportable that shares some result or its
	 *	absence
	 * @param testables a set of test class instance suppliers
	 * @param arguments a map of arguments, where the keys are the names
	 *	of public test and life cycle methods with declared parameters
	 *	and the values are a series of their arguments
	 */
	DefaultInstantiation(ExecutionPolicy executionPolicy,
				Set<? extends Prefixable> prefixables,
				PrintStream printStream,
				Reportable reportable,
				Set<Supplier<? extends T>> testables,
				Map<String, Collection<List<?>>> arguments)
	{
		super(executionPolicy, prefixables, printStream, reportable);
		this.testables = Objects.requireNonNull(testables, "testables");
		this.arguments = Objects.requireNonNull(arguments, "arguments");
	}

	@Override
	public Map<String, Collection<List<?>>> arguments()
	{
		return arguments;
	}

	@Override
	public Set<Supplier<? extends T>> testables()
	{
		return testables;
	}

	@Override
	public String toString()
	{
		return String.format("%s, %s, %s, %s, %s, %s",
					executionPolicy(), prefixables(),
					printStream(), reportable(),
					arguments(), testables());
	}
}
