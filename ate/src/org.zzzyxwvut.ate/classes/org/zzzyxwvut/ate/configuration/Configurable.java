package org.zzzyxwvut.ate.configuration;

import java.io.PrintStream;
import java.util.Set;

import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;

/**
 * This interface declares configurable testing attributes.
 *
 * @see org.zzzyxwvut.ate.Tester.Builder#configurable(Configurable)
 */
public interface Configurable
{
	/**
	 * Returns an execution policy for running the prefix-matching
	 * test methods.
	 *
	 * @return an execution policy
	 */
	ExecutionPolicy executionPolicy();

	/**
	 * Returns a set of prefixes for method names to match, forming a batch
	 * for each prefix element. The implementation of the returned set of
	 * prefixes arranges the order of batch processing.
	 * <p>
	 * A candidate test method shall be rejected whenever its simple name
	 * <i>equals</i> the simple name of a method, inherited or overridden
	 * or overloaded, {@code public} or {@code protected}, and which is
	 * a member of either:
	 * <ul>
	 * <li>{@code java.lang.Object}, or
	 * <li>a type of the {@code org.zzzyxwvut.ate} package prefix
	 * </ul>
	 *
	 * @return a set of prefixes for method names to match
	 * @see java.util.LinkedHashSet
	 */
	Set<? extends Prefixable> prefixables();

	/**
	 * Returns a stream to use for output.
	 * <p>
	 * The life cycle management of this stream is the sole responsibility
	 * of a client.
	 * <p>
	 * The consensual use of a non-default reportable implementation,
	 * the {@link org.zzzyxwvut.ate.Tester.Builder#suppressThrowablePrinting()}
	 * and {@link org.zzzyxwvut.ate.Tester.Builder#suppressUnassignability()}
	 * options, secures for a client exclusive writing privileges to this
	 * stream.
	 *
	 * @return a stream to use for output
	 */
	PrintStream printStream();

	/**
	 * Returns a reportable that shares some result or its absence.
	 *
	 * @return a reportable that shares some result or its absence
	 */
	Reportable reportable();

	/** This interface declares prefixable attributes. */
	interface Prefixable
	{
		/**
		 * Returns a prefix for test method names to match.
		 *
		 * @return a prefix for test method names to match
		 */
		String testMethodNamePrefix();

		/**
		 * Returns a set of prefixes for life cycle method names to
		 * match.
		 * <p>
		 * Matching of {@code setUpAll} and {@code tearDownAll} is not
		 * supported. An empty set denotes that all found names match.
		 *
		 * @return a set of prefixes for life cycle method names to
		 *	match
		 */
		Set<String> lifeCycleMethodNamePrefixes();
	}

	/**
	 * A builder of {@code Configurable}.
	 *
	 * @param <A> a {@code Builder}-derived class
	 * @param <B> a {@code Configurable}
	 */
	interface Builder<A extends Builder<A, B>, B extends Configurable>
	{
		/**
		 * Sets an execution policy for running the prefix-matching
		 * test methods.
		 *
		 * @param executionPolicy an execution policy for running
		 *	the prefix-matching test methods
		 * @return {@code this} instance
		 */
		A executionPolicy(ExecutionPolicy executionPolicy);

		/**
		 * Sets a set of prefixes for method names to match.
		 *
		 * @param prefixables a set of prefixes for method names to
		 *	match
		 * @return {@code this} instance
		 */
		A prefixables(Set<? extends Prefixable> prefixables);

		/**
		 * Sets a stream to use for output.
		 *
		 * @param printStream a stream to use for output
		 * @return {@code this} instance
		 */
		A printStream(PrintStream printStream);

		/**
		 * Sets a reportable that shares some result or its absence.
		 *
		 * @param reportable a reportable that shares some result or
		 *	its absence
		 * @return {@code this} instance
		 */
		A reportable(Reportable reportable);

		/**
		 * Returns a new instance of {@code Configurable}.
		 *
		 * @return a new instance of {@code Configurable}
		 */
		B build();
	}

	/**
	 * Creates a new {@code Builder} object.
	 *
	 * @return a new {@code Builder} object
	 */
	static Builder<?, Configurable> newBuilder()
	{
		class DefaultBuilder extends
				AbstractConfigurableBuilder<DefaultBuilder,
								Configurable>
		{
			@Override
			public DefaultBuilder self()	{ return this; }

			@Override
			public Configurable build()
			{
				return new DefaultConfiguration(
						executionPolicy, prefixables,
						printStream, reportable);
			}
		}

		return new DefaultBuilder();
	}
}
