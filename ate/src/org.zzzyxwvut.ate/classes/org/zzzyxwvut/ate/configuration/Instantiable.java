package org.zzzyxwvut.ate.configuration;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Testable;

/**
 * A {@link Configurable} that manages the scope of test class instances and
 * arranges arguments for public methods with parameters.
 * <p>
 * Whenever an implementation of this interface is required for a test class,
 * implement in it a {@code public static} parameterless {@code instantiable}
 * method that returns an instance of the implementation, e.g.:
 * <blockquote><pre><code>
 * package org.group;
 *
 * import java.util.Set;
 *
 * import org.zzzyxwvut.ate.Testable;
 * import org.zzzyxwvut.ate.configuration.Instantiable;
 *
 * &#64;SuppressWarnings("exports")
 * public class SomeTests implements Testable
 * {
 *	private final int value;
 *
 *	private SomeTests(int value)	{ this.value = value; }
 *
 *	public static Instantiable&lt;SomeTests&gt; instantiable()
 *	{
 *		return Instantiable.&lt;SomeTests&gt;newBuilder(Set.of(
 *						() -&gt; new SomeTests(0),
 *						() -&gt; new SomeTests(1)))
 *			.build();
 *	}
 *
 *	// Other methods.
 * }</code></pre></blockquote>
 *
 * @param <T> the type of a test class
 */
public interface Instantiable<T extends Testable> extends Configurable
{
	/**
	 * Returns a map of arguments, where the keys are the names of public
	 * test and life cycle methods with declared parameters and the values
	 * are a series of their arguments.
	 * <p>
	 * (The {@link org.zzzyxwvut.ate.Status Status} type, declared as
	 * the <b>first</b> parameter of a managed method, is served by other
	 * means; passing of an object of this type explicitly, as the first
	 * element in a list of a collection value of a map returned by this
	 * method, implies a method signature with at least two {@code Status}
	 * parameters; so that the object passed as the first argument is
	 * bound to the second parameter, the second argument to the third
	 * parameter, and so on.)
	 * <p>
	 * The matching method will not be invoked with the supplied arguments
	 * when an argument is not valid for identity or widening conversion.
	 * In particular, care should be exercised for methods with primitive
	 * type parameters for no widening primitive conversion for their
	 * arguments is performed, only boxing.
	 * <p>
	 * Variable-arity and overloaded methods are supported.
	 * <p>
	 * Keys matching parameterless or non-existent methods are ignored.
	 *
	 * @return a map of arguments
	 */
	Map<String, Collection<List<?>>> arguments();

	/**
	 * Returns a set of test class instance suppliers.
	 *
	 * @return a set of test class instance suppliers
	 */
	Set<Supplier<? extends T>> testables();

	/**
	 * A builder of {@code Instantiable}.
	 *
	 * @param <A> an {@code Builder}-derived class
	 * @param <B> a {@code Configurable}
	 * @param <C> a {@code Testable}
	 */
	interface Builder<A extends Instantiable.Builder<A, B, C>,
							B extends Configurable,
							C extends Testable>
				extends Configurable.Builder<A, B>
	{
		/**
		 * Sets a map of arguments, where the keys are the names of
		 * public test and life cycle methods with declared parameters
		 * and the values are a series of their arguments.
		 *
		 * @param arguments a map of arguments
		 * @return {@code this} instance
		 */
		A arguments(Map<String, Collection<List<?>>> arguments);
	}

	/**
	 * Creates a new {@code Builder} object.
	 *
	 * @param <A> a {@code Testable}
	 * @param testables a set of test class instance suppliers
	 * @return a new {@code Builder} object
	 */
	static <A extends Testable>
		Instantiable.Builder<?, Instantiable<A>, A> newBuilder(
					Set<Supplier<? extends A>> testables)
	{
		class DefaultBuilder extends
				AbstractInstantiableBuilder<DefaultBuilder,
							Instantiable<A>,
							A>
		{
			DefaultBuilder(Set<Supplier<? extends A>> testables)
			{
				super(testables);
			}

			@Override
			public DefaultBuilder self()	{ return this; }

			@Override
			public Instantiable<A> build()
			{
				return new DefaultInstantiation<>(
						executionPolicy, prefixables,
						printStream, reportable,
						testables, arguments);
			}
		}

		return new DefaultBuilder(testables);
	}
}
