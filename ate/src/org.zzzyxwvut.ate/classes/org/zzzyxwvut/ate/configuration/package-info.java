/**
 * Provides a means of configuration of testing attributes.
 * <p>
 * Without explicit configuration, the following default values are obtained
 * for a test class:
 * <ul>
 * <li>a sequential execution policy for running the prefix-matching test
 * methods
 * <li>a single element set to match a {@code test} prefix for test method
 * names and an empty set for life cycle method names
 * <li>a {@link System#err} stream
 * <li>a reportable that sends an applied result to an applied stream with
 * {@link org.zzzyxwvut.ate.Result#resultor()}, rendering
 * {@code Result.OtherResult} objects in reverse video and
 * {@code Result.TestResult} objects in red-yellow-green colours
 * (unless {@link org.zzzyxwvut.ate.Tester.Builder#renderOutputPlain()} is
 * opted for)
 * <li>a single element test-class-instance supplier set that uses
 * a parameterless (else default) test class constructor
 * <li>an empty map of public method arguments
 * </ul>
 * <p>
 * Particular values can be set with instances of {@link Configurable} and
 * {@link Instantiable}.
 * <p>
 * Every non-{@code null} property value of a {@code Configurable} instance
 * prevails over its corresponding default value.
 * <p>
 * Every non-{@code null} property value of an {@code Instantiable} instance
 * prevails over its corresponding {@code Configurable} or default value.
 */
package org.zzzyxwvut.ate.configuration;
