package org.zzzyxwvut.ate;

import java.io.PrintStream;
import java.util.Optional;

/**
 * This interface exposes a few details about the existing state of testing
 * affairs. Any test and life cycle method that declares its <b>first</b>
 * parameter of this type shall be served with an instance of it.
 */
public interface Status
{
	/**
	 * Returns an output stream used for testing.
	 *
	 * @return an output stream used for testing
	 */
	PrintStream printStream();

	/**
	 * Returns an optional with the test method name prefix in progress,
	 * otherwise an empty optional.
	 *
	 * @return an optional with the test method name prefix in progress,
	 *	otherwise an empty optional
	 */
	Optional<String> testMethodNamePrefix();
}
