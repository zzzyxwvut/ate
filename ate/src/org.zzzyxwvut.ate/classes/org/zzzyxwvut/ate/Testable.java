package org.zzzyxwvut.ate;

/** This interface lays claim to conformable classes. */
public interface Testable
{
	/** This enumeration specifies supported execution policies. */
	enum ExecutionPolicy
	{
		/** A hint to prefer a sequential execution. */
		SEQUENTIAL,

		/** A hint to prefer a concurrent execution. */
		CONCURRENT
	}
}
