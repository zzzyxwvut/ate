/** Provides members that lend indirect support to testing facilities. */
package org.zzzyxwvut.ate.support;
