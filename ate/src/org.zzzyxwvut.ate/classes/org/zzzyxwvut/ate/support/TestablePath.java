package org.zzzyxwvut.ate.support;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.Support;

/** This class manages creation of directories relative to a test class. */
public final class TestablePath
{
	private static final FileAttribute<?>[] DIRECTORY_ATTRIBUTES =
						(FileSystems.getDefault()
			.supportedFileAttributeViews()
			.contains("posix"))
		? new FileAttribute<?>[] {
			PosixFilePermissions.asFileAttribute(
				PosixFilePermissions.fromString("rwxr-x---"))
		}
		: new FileAttribute<?>[0];

	private final Optional<Path> parentDirectoryPath;

	/**
	 * Constructs a new {@code TestablePath} object.
	 *
	 * @param testClass a test class to use for path walking
	 * @param parentDirectoryName the name of an existing directory
	 *	between the passed class and the root of the filesystem
	 * @param offset how many of toward-root {@code parentDirectoryName}
	 *	matches, if any, to pass over
	 */
	public TestablePath(Class<? extends Testable> testClass,
						String parentDirectoryName,
						int offset)
	{
		class ToZeroReducer
		{
			private int value;

			ToZeroReducer(int value)
			{
				this.value = (value < 0)
					? 0
					: value;
			}

			boolean reduce()
			{
				return ((value == 0)
					|| ((value -= (value > 0)
							? 1
							: 0)
						< 0));
			}
		}

		Objects.requireNonNull(testClass, "testClass");
		verifyNonNullOrBlank(parentDirectoryName,
						"parentDirectoryName");
		parentDirectoryPath = Stream.<Class<?>>iterate(testClass,
						Class::getEnclosingClass)
			.takeWhile(Objects::nonNull)
			.reduce((enclosedClass, enclosingClass) ->
							enclosingClass)
			.map(topClass -> topClass
				.getResource(topClass.getSimpleName()
					.concat(".class"))) /* Exit _null_. */
			.flatMap(Support.<String,
					Function<ToZeroReducer,
					Function<URL, Optional<Path>>>>
								partialApply(
				parentDirName -> matcher -> topURL -> Stream
					.<Path>iterate(toPath(topURL),
							Path::getParent)
					.takeWhile(Predicate
						.<Path>not(Objects::isNull)
						.and(Support.<String,
							Function<ToZeroReducer,
							Predicate<Path>>>
								partialApply(
							dirName -> matcher_ ->
									path ->
								!(dirName.equals(
									Optional.ofNullable(
										path.getFileName())
									.map(Path::toString)
									.orElse(""))
								&& matcher_
									.reduce()),
							parentDirName)
								.apply(matcher)))
					.reduce((childPath, parentPath) ->
								parentPath)
					.map(Path::getParent),
				parentDirectoryName)
					.apply(new ToZeroReducer(offset)));
	}

	/**
	 * Constructs a new {@code TestablePath} object with an offset of
	 * {@code 0}.
	 *
	 * @param testClass a test class to use for path walking
	 * @param parentDirectoryName the name of an existing directory
	 *	between the passed class and the root of the filesystem
	 */
	public TestablePath(Class<? extends Testable> testClass,
						String parentDirectoryName)
	{
		this(testClass, parentDirectoryName, 0);
	}

	private static String verifyNonNullOrBlank(String value,
							String parameterName)
	{
		return Optional.of(Objects.requireNonNull(value,
							parameterName))
			.filter(Predicate.not(String::isBlank))
			.orElseThrow(() -> new IllegalArgumentException(
							"Blank name"));
	}

	private static Path toPath(URL url)
	{
		final String pathName = url.getPath();
		return Path.of(("jar".equals(url.getProtocol())
					&& pathName.startsWith("file:"))
				? pathName.substring("file:".length())
				: pathName)
			.normalize();
	}

	/**
	 * Attempts to create a sibling directory (or a {@code sibling/child}
	 * series of directories) of a valid {@link #parentDirectoryPath()}
	 * directory.
	 * <pre>
	 * For example, with the {@code reports/pending} argument, assuming
	 * that the parent directory path is:
	 *	<code>${ROOT_PREFIX}/project/build/tests/modules/</code>
	 *
	 * the {@code reports/pending} directories are to be created at:
	 *	<code>${ROOT_PREFIX}/project/build/tests/</code>
	 * </pre>
	 *
	 * @param siblingDirectoryName the name of a sibling directory (or
	 *	a {@code sibling/child} series of directories) to create
	 * @return an optional with the path to the rightmost created
	 *	directory, otherwise an empty optional if the parent directory
	 *	path is invalid
	 * @throws UncheckedIOException if an I/O error occurs
	 */
	public Optional<Path> createSiblingDirectories(
						String siblingDirectoryName)
	{
		verifyNonNullOrBlank(siblingDirectoryName,
						"siblingDirectoryName");
		return parentDirectoryPath
			.map(Support.<String, UnaryOperator<Path>>partialApply(
				siblingDirName -> parentDirPath -> {
					try {
						return Files.createDirectories(
							parentDirPath
								.resolveSibling(
									siblingDirName),
							DIRECTORY_ATTRIBUTES);
					} catch (final IOException e) {
						throw new UncheckedIOException(
									e);
					}
				}, siblingDirectoryName));
	}

	/**
	 * Returns an optional with the path to a parent directory, otherwise
	 * an empty optional if the parent directory path is invalid.
	 *
	 * @return an optional with the path to a parent directory, otherwise
	 *	an empty optional if the parent directory path is invalid
	 */
	public Optional<Path> parentDirectoryPath()
	{
		return parentDirectoryPath;
	}

	@Override
	public String toString()
	{
		return parentDirectoryPath.toString();
	}
}
