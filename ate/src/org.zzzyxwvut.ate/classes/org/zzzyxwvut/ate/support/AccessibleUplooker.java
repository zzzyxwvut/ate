package org.zzzyxwvut.ate.support;

import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodHandles;

/**
 * A service that exports a look-up object that shall be used for creation of
 * method handles of every test class of the module.
 * <p>
 * Any implementation must be declared in a non-exportable package and its
 * usage be recorded with the {@code provides} directive in the module
 * declaration.
 * <p>
 * A sample implementation:
 * <blockquote><pre><code>
 * package org.group.internal;
 *
 * import java.lang.invoke.MethodHandles.Lookup;
 * import java.lang.invoke.MethodHandles;
 * import org.zzzyxwvut.ate.support.AccessibleUplooker;
 *
 * public class TestUplooker extends AccessibleUplooker
 * {
 *	&#64;Override
 *	public Lookup exportableLookup()
 *	{
 *		return MethodHandles.lookup()
 *			.dropLookupMode(Lookup.PROTECTED);
 *	}
 * }
 * </code></pre></blockquote>
 * <p>
 * A sample module declaration:
 * <blockquote><pre><code>
 * module org.group
 * {
 *	requires static org.zzzyxwvut.ate;
 *
 *	// Reach out for the test classes of a non-exportable package.
 *	opens org.group.internal to
 *		org.zzzyxwvut.ate;
 *
 *	provides org.zzzyxwvut.ate.support.AccessibleUplooker with
 *		org.group.internal.TestUplooker;
 * }</code></pre></blockquote>
 *
 * @see java.util.ServiceLoader
 */
public abstract class AccessibleUplooker
{
	/** Constructs a new {@code AccessibleUplooker} object. */
	protected AccessibleUplooker() { }

	/**
	 * Exports a lookup object.
	 *
	 * @implSpec
	 * Only the {@code Lookup.PUBLIC} and {@code Lookup.MODULE} modes are
	 * retained.
	 *
	 * @return a lookup object
	 */
	public Lookup exportableLookup()
	{
		return MethodHandles.lookup()
			.dropLookupMode(Lookup.PACKAGE);
	}
}
