package org.zzzyxwvut.ate.internal;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;

/** A default {@code Tester.Builder}. */
public final class DefaultTesterBuilder implements Tester.Builder
{
	/** A set of test classes. */
	final Set<Class<? extends Testable>> classes;

	/** A set of the predicative options of {@code Tester.Builder}. */
	final Set<Option> options = EnumSet.noneOf(Option.class);

	/** An execution policy for class testing. */
	ExecutionPolicy executionPolicy = ExecutionPolicy.SEQUENTIAL;

	/** An optional with configurable testing attributes. */
	Optional<Configurable> configurable = Optional.empty();

	/**
	 * Constructs a new {@code DefaultTesterBuilder} object.
	 *
	 * @param classes a set of test classes
	 */
	public DefaultTesterBuilder(Set<Class<? extends Testable>> classes)
	{
		this.classes = Objects.requireNonNull(classes, "classes");
	}

	/**
	 * Sets an execution policy for class testing, else
	 * {@link ExecutionPolicy#SEQUENTIAL} is used.
	 *
	 * @param executionPolicy {@inheritDoc}
	 */
	@Override
	public DefaultTesterBuilder executionPolicy(
					ExecutionPolicy executionPolicy)
	{
		this.executionPolicy = Objects.requireNonNull(executionPolicy,
							"executionPolicy");
		return this;
	}

	@Override
	public DefaultTesterBuilder configurable(Configurable configurable)
	{
		this.configurable = Optional.of(Objects.requireNonNull(
							configurable,
							"configurable"));
		return this;
	}

	@Override
	public DefaultTesterBuilder continueOnFailure()
	{
		options.add(Option.CONTINUE_ON_FAILURE);
		return this;
	}

	@Override
	public DefaultTesterBuilder renderOutputPlain()
	{
		options.add(Option.RENDER_OUTPUT_PLAIN);
		return this;
	}

	@Override
	public DefaultTesterBuilder suppressThrowablePrinting()
	{
		options.add(Option.SUPPRESS_THROWABLE_PRINTING);
		return this;
	}

	@Override
	public DefaultTesterBuilder suppressUnassignability()
	{
		options.add(Option.SUPPRESS_UNASSIGNABILITY);
		return this;
	}

	@Override
	public DefaultTesterBuilder verifyAssertion()
	{
		options.add(Option.VERIFY_ASSERTION);
		return this;
	}

	@Override
	public Tester build()
	{
		return new DefaultTester(classes,
					executionPolicy,
					configurable,
					Collections.unmodifiableSet(
						EnumSet.copyOf(options)));
	}
}
