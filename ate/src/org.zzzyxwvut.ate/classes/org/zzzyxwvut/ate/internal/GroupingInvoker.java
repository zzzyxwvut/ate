package org.zzzyxwvut.ate.internal;

import java.io.PrintStream;
import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.internal.adt.ReadOnlyMap;

/**
 * This class arranges instances of {@link MHAA} and {@link Method} into
 * designated set lists according to supported method categories.
 */
final class GroupingInvoker
{
	private static final Set<Testable> TESTABLE_SENTINEL = Set.of(
							new Testable() { });

	private final OneStockMHAAs setUpAlls;
	private final OneStockMHAAs tearDownAlls;
	private final OneStockMHAAs setUpBatches;
	private final OneStockMHAAs tearDownBatches;
	private final OneStockMHAAs setUps;
	private final OneStockMHAAs tearDowns;
	private final List<Set<Method>> candidateMethods;
	private final Function<List<Set<Method>>, List<Set<TestableMHAA>>>
						candidateMethodUnfolder;

	/**
	 * Constructs a new {@code GroupingInvoker} object.
	 *
	 * @param printStream a stream to use for output
	 * @param testables a set of test class instance suppliers
	 * @param arguments a map of arguments, where the keys are the names
	 *	of public test and life cycle methods with declared parameters
	 *	and the values are a series of their arguments
	 * @param methods a map of classified methods
	 * @param unassignableWarner a functional interface that takes
	 *	a method and returns a functional interface that takes
	 *	a list of arguments and does not return a value
	 * @param unreflector a functional interface that takes a method and
	 *	returns a method handle
	 * @param orderedTestMethods whether to take test methods in order
	 * @throws IllegalStateException if a method handle for a life cycle
	 *	method cannot be made or required argument(s) cannot be
	 *	obtained
	 */
	GroupingInvoker(PrintStream printStream,
			Set<Supplier<? extends Testable>> testables,
			ReadOnlyMap<String, Collection<List<?>>> arguments,
			ReadOnlyMap<MethodKind, List<Set<Method>>> methods,
			Function<Method, Consumer<List<?>>> unassignableWarner,
			Function<Method, MethodHandle> unreflector,
			boolean orderedTestMethods)
	{
		Objects.requireNonNull(printStream, "printStream");
		Objects.requireNonNull(testables, "testables");
		Objects.requireNonNull(arguments, "arguments");
		Objects.requireNonNull(methods, "methods");
		Objects.requireNonNull(unassignableWarner,
						"unassignableWarner");
		Objects.requireNonNull(unreflector, "unreflector");
		final Function<Function<Function<List<?>,
						Function<Testable,
							TestableMHAA>>,
					Function<List<?>,
						Stream<TestableMHAA>>>,
				Function<Method, Stream<TestableMHAA>>>
							singleMethodUnfolder =
							singleMethodUnfolder()
			.apply(printStream)
			.apply(arguments)
			.apply(unreflector)
			.apply(method -> () -> new IllegalStateException(
						Support.signature(method)
				.concat(": required argument(s) not found")))
			.apply(argumentRejector()
				.apply(unassignableWarner));
		final Function<List<Set<Method>>, List<Set<MHAA>>> unfolder =
							GroupingInvoker
			.<MHAA>descendantMethodUnfolder()
			.apply(GroupingInvoker.<MHAA>siblingMethodUnfolder()
				.apply(lifeCycleMethodUnfolder()
					.apply(Support.toUnmodifiableOrderedSet())
					.apply(singleMethodUnfolder
						.apply(GroupingInvoker
							.<Testable>instanceGenerator()
							.apply(Function.identity())
							.apply(TESTABLE_SENTINEL)))));
		candidateMethodUnfolder = GroupingInvoker
			.<TestableMHAA>descendantMethodUnfolder()
			.apply(GroupingInvoker.<TestableMHAA>siblingMethodUnfolder()
				.apply(testMethodUnfolder()
					.apply(Support.toUnmodifiableSet(
							orderedTestMethods))
					.apply(singleMethodUnfolder
						.apply(GroupingInvoker
							.<Supplier<? extends Testable>>
									instanceGenerator()
							.apply(supplier -> Objects
								.requireNonNull(
									supplier.get(),
									"testable"))
							.apply(testables)))));
		final List<Set<MHAA>> setUpAlls_ = unfolder
			.apply(methods.get(MethodKind.SET_UP_ALL));
		setUpAlls = new OneStockMHAAs(unity(setUpAlls_),
							setUpAlls_);
		final List<Set<MHAA>> tearDownAlls_ = unfolder
			.apply(methods.get(MethodKind.TEAR_DOWN_ALL));
		tearDownAlls = new OneStockMHAAs(unity(tearDownAlls_),
							tearDownAlls_);
		final List<Set<MHAA>> setUpBatches_ = unfolder
			.apply(methods.get(MethodKind.SET_UP_BATCH));
		setUpBatches = new OneStockMHAAs(unity(setUpBatches_),
							setUpBatches_);
		final List<Set<MHAA>> tearDownBatches_ = unfolder
			.apply(methods.get(MethodKind.TEAR_DOWN_BATCH));
		tearDownBatches = new OneStockMHAAs(unity(tearDownBatches_),
							tearDownBatches_);
		final List<Set<MHAA>> setUps_ = unfolder
			.apply(methods.get(MethodKind.SET_UP));
		setUps = new OneStockMHAAs(unity(setUps_), setUps_);
		final List<Set<MHAA>> tearDowns_ = unfolder
			.apply(methods.get(MethodKind.TEAR_DOWN));
		tearDowns = new OneStockMHAAs(unity(tearDowns_), tearDowns_);
		candidateMethods = methods.get(MethodKind.TEST);
	}

	private static <E> Optional<E> unity(
			Collection<? extends Collection<E>> descendants)
	{
		final Collection<E> siblings;
		return (descendants.size() == 1
				&& Objects.requireNonNullElseGet(
						siblings = descendants
								.iterator()
								.next(),
						List::of)
					.size() == 1)
			? Optional.ofNullable(siblings.iterator()
							.next())
			: Optional.empty();
	}	/* Assume _descendants_ to be an unmodifiable collection. */

	private static Function<Method,
				Function<MethodHandle,
				Function<Optional<PrintStream>,
				Function<List<?>,
				Function<Testable, TestableMHAA>>>>>
								handleMaker()
	{
		return method -> methodHandle -> statusStream -> arguments ->
								testable ->
							new TestableMHAA(
							testable,
							(arguments.isEmpty()
						&& !method.isVarArgs())
			? MHAA.newMHAA(method.getName(),
				methodHandle,
				statusStream)
			: MHAA.newMHAA(method.getName(),
				methodHandle,
				statusStream,
				arguments,
							(method.isVarArgs())
				? Optional.of(method.getParameterTypes()
					[method.getParameterCount() - 1])
				: Optional.empty()));
	}

	private static <E> Function<Function<E, Testable>,
				Function<Set<E>,
				Function<Function<List<?>,
					Function<Testable, TestableMHAA>>,
				Function<List<?>, Stream<TestableMHAA>>>>>
							instanceGenerator()
	{
		return mapper -> items -> handleMaker -> arguments -> items
			.stream()
			.map(handleMaker
				.apply(arguments)
				.compose(mapper));
	}

	private static Function<Function<Method, Consumer<List<?>>>,
				Function<Method,
				Predicate<List<?>>>> argumentRejector()
	{
		return unassignableWarner -> method -> arguments -> {
			unassignableWarner
				.apply(method)
				.accept(arguments);
			return true;
		};
	}

	private static Function<Predicate<List<?>>,
				Function<Predicate<List<?>>,
				Predicate<List<?>>>> argumentVerifier()
	{
		return argumentRejector -> parameterMatcher -> arguments ->
					((parameterMatcher.test(arguments))
				? Optional.<List<?>>empty()
				: Optional.<List<?>>of(arguments))
			.filter(argumentRejector)
			.isEmpty();
	}

	private static Function<PrintStream,
				Function<ReadOnlyMap<String,
							Collection<List<?>>>,
				Function<Function<Method, MethodHandle>,
				Function<Function<Method,
						Supplier<RuntimeException>>,
				Function<Function<Method, Predicate<List<?>>>,
				Function<Function<Function<List<?>,
						Function<Testable,
							TestableMHAA>>,
					Function<List<?>,
						Stream<TestableMHAA>>>,
				Function<Method, Stream<TestableMHAA>>>>>>>>
							singleMethodUnfolder()
	{
		return printStream -> arguments -> unreflector ->
					argumentThrower -> argumentRejector ->
					instanceGenerator -> method -> {
			final MethodParameter methodParameter = MethodParameter
				.newInstance(method);
			final Function<List<?>, Stream<TestableMHAA>>
								generator =
							instanceGenerator
				.apply(handleMaker()
					.apply(method)
					.apply(unreflector
						.apply(method))
					.apply((methodParameter.withStatus())
						? Optional.of(printStream)
						: Optional.empty()));
			return (methodParameter.ineligible())
					? generator
						.apply(List.of())
					: Optional
				.of(arguments.get(method.getName()))
				.filter(Predicate.not(Collection::isEmpty))
				.orElseThrow(argumentThrower
					.apply(method))
				.stream()
				.filter(argumentVerifier()
					.apply(argumentRejector
						.apply(method))
					.apply(methodParameter
						.matcher()))
				.flatMap(generator);
		};
	}

	private static Function<Collector<TestableMHAA, ?, Set<TestableMHAA>>,
				Function<Function<Method, Stream<TestableMHAA>>,
				Function<Method, Set<TestableMHAA>>>>
						testMethodUnfolder()
	{
		return collector -> singleMethodUnfolder -> method ->
						singleMethodUnfolder
			.apply(method)
			.collect(collector);
	}

	private static Function<Collector<TestableMHAA, ?, Set<TestableMHAA>>,
				Function<Function<Method, Stream<TestableMHAA>>,
				Function<Method, Set<MHAA>>>>
						lifeCycleMethodUnfolder()
	{
		return stub -> singleMethodUnfolder -> method ->
						singleMethodUnfolder
			.apply(method)
			.map(TestableMHAA::mhaa)

			/*
			 * The following set collector runs in couples with
			 * SingleClassTester.LifeCycleRunner#lifeCycleRunner().
			 */
			.collect(Support.toUnmodifiableOrderedSet());
	}

	private static <E> Function<Function<Method, Set<E>>,
				Function<Set<Method>, Stream<Set<E>>>>
						siblingMethodUnfolder()
	{
		return methodUnfolder -> methods -> methods
			.stream()
			.map(methodUnfolder);
	}

	private static <E> Function<Function<Set<Method>, Stream<Set<E>>>,
				Function<List<Set<Method>>, List<Set<E>>>>
						descendantMethodUnfolder()
	{
		return siblingMethodUnfolder -> methods -> methods
			.stream()
			.flatMap(siblingMethodUnfolder)
			.collect(Collectors.toUnmodifiableList());
	}

	/**
	 * Produces a pair set list of a test class instance and a method
	 * handle, if permissible, from the passed set list of methods, along
	 * with the collected series of their assignable method arguments, if
	 * required.
	 *
	 * @param methods an ordered set list of test methods
	 * @return a pair set list of a test class instance and a method
	 *	handle with its arguments, if any; otherwise an empty list,
	 *	if the passed set list is empty
	 * @throws IllegalStateException if a method handle cannot be made or
	 *	required argument(s) cannot be obtained
	 */
	List<Set<TestableMHAA>> unfoldEach(List<Set<Method>> methods)
	{
		Objects.requireNonNull(methods, "methods");
		return candidateMethodUnfolder.apply(methods);
	}

	/**
	 * Returns the pair of an optional with the only {@code setUpAll}
	 * method handle with its arguments, if any, and the set list of
	 * {@code setUpAll} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code setUpAll}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code setUpAll} method handles with their arguments, if any
	 */
	OneStockMHAAs setUpAlls()		{ return setUpAlls; }

	/**
	 * Returns the pair of an optional with the only {@code tearDownAll}
	 * method handle with its arguments, if any, and the set list of
	 * {@code tearDownAll} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code tearDownAll}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code tearDownAll} method handles with their arguments, if
	 *	any
	 */
	OneStockMHAAs tearDownAlls()		{ return tearDownAlls; }

	/**
	 * Returns the pair of an optional with the only {@code setUpBatch}
	 * method handle with its arguments, if any, and the set list of
	 * {@code setUpBatch} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code setUpBatch}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code setUpBatch} method handles with their arguments, if any
	 */
	OneStockMHAAs setUpBatches()		{ return setUpBatches; }

	/**
	 * Returns the pair of an optional with the only {@code tearDownBatch}
	 * method handle with its arguments, if any, and the set list of
	 * {@code tearDownBatch} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code tearDownBatch}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code tearDownBatch} method handles with their arguments, if
	 *	any
	 */
	OneStockMHAAs tearDownBatches()		{ return tearDownBatches; }

	/**
	 * Returns the pair of an optional with the only {@code setUp}
	 * method handle with its arguments, if any, and the set list of
	 * {@code setUp} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code setUp}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code setUp} method handles with their arguments, if any
	 */
	OneStockMHAAs setUps()			{ return setUps; }

	/**
	 * Returns the pair of an optional with the only {@code tearDown}
	 * method handle with its arguments, if any, and the set list of
	 * {@code tearDown} method handles with their arguments, if any.
	 *
	 * @return the pair of an optional with the only {@code tearDown}
	 *	method handle with its arguments, if any, and the set list of
	 *	{@code tearDown} method handles with their arguments, if any
	 */
	OneStockMHAAs tearDowns()		{ return tearDowns; }

	/**
	 * Returns the set list of candidate test methods.
	 *
	 * @return the set list of candidate test methods
	 */
	List<Set<Method>> candidateMethods()	{ return candidateMethods; }

	/**
	 * A pair of an optional with a categorised life cycle method handle
	 * with its arguments, if any, and a set list of categorised life
	 * cycle method handles with their arguments, if any.
	 */
	record OneStockMHAAs(Optional<MHAA> one, List<Set<MHAA>> stock)
	{
		/**
		 * Constructs a new {@code OneStockMHAAs} object.
		 *
		 * @param one an optional with a categorised life cycle method
		 *	handle with its arguments, if any
		 * @param stock a set list of categorised life cycle method
		 *	handles with their arguments, if any
		 */
		OneStockMHAAs
		{
			Objects.requireNonNull(one, "one");
			Objects.requireNonNull(stock, "stock");
		}
	}

	/**
	 * A pair of a test class instance and a method handle with its
	 * arguments, if any.
	 */
	record TestableMHAA(Testable testable, MHAA mhaa)
	{
		/**
		 * Constructs a new {@code TestableMHAA} object.
		 *
		 * @param testable a test class instance
		 * @param mhaa a method handle with its arguments, if any
		 */
		TestableMHAA
		{
			Objects.requireNonNull(testable, "testable");
			Objects.requireNonNull(mhaa, "mhaa");
		}

		@Override
		public int hashCode()
		{
			return System.identityHashCode(this);
		}

		@Override
		public boolean equals(Object that)
		{
			return (this == that);
		}
	}
}
