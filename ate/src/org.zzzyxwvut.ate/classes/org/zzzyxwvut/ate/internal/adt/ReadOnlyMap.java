package org.zzzyxwvut.ate.internal.adt;

import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * A read-only map that permits neither a {@code null} key nor {@code null}
 * values and requires a default value for absent keys.
 *
 * @param <K> the type of keys
 * @param <V> the type of values
 */
public final class ReadOnlyMap<K, V>
{
	private final Map<? extends K, ? extends V> map;
	private final Supplier<? extends V> defaultValue;

	/**
	 * Constructs a new {@code ReadOnlyMap} object.
	 *
	 * @param map a map
	 * @param defaultValue the value to use for absent keys
	 */
	public ReadOnlyMap(Map<? extends K, ? extends V> map,
					Supplier<? extends V> defaultValue)
	{
		this.map = Objects.requireNonNull(map, "map");
		this.defaultValue = Objects.requireNonNull(defaultValue,
							"defaultValue");
	}

	/**
	 * Returns the value to which the passed key is mapped, otherwise
	 * the default value if the key is absent.
	 *
	 * @param key the key whose associated value is to be obtained
	 * @return the value to which the passed key is mapped, otherwise
	 *	the default value if the key is absent
	 */
	public V get(K key)
	{
		Objects.requireNonNull(key, "key");
		return Objects.requireNonNullElseGet(map.get(key),
							defaultValue);
	}

	@Override
	public String toString()	{ return map.toString(); }
}
