package org.zzzyxwvut.ate.internal;

import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToLongFunction;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Configurable.Prefixable;
import org.zzzyxwvut.ate.internal.GroupingInvoker.OneStockMHAAs;
import org.zzzyxwvut.ate.internal.GroupingInvoker.TestableMHAA;
import org.zzzyxwvut.ate.internal.ResultFriend.Resultable;

/** This class serves for invocation of methods of a test class. */
final class SingleClassTester
{
	private static final Resultable RESULTABLE = ResultFriend
							.getResultable();

	private final Set<? extends Prefixable> prefixables;
	private final Predicate<Set<? extends Prefixable>> runner;

	/**
	 * Constructs a new {@code SingleClassTester} object.
	 *
	 * @param context a test class context
	 */
	SingleClassTester(TestClassContext context)
	{
		Objects.requireNonNull(context, "context");
		final boolean inConcurrence = inConcurrence(
				context,
				EnumSet.of(ExecutionPolicy.CONCURRENT));
		final Consumer<Result> reporter = context.reporter();
		final Consumer<Throwable> errorPrinter = context.errorPrinter();
		final GroupingInvoker invoker = context.invoker();
		final List<Set<Method>> candidateMethods = invoker
			.candidateMethods();
		final Function<Boolean, TestRunner> testRunner =
			Support.<Function<Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>,
				Function<Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>,
								TestRunner>>,
				Function<Function<Boolean,
					Function<Optional<Prefixable>,
					Function<Function<Optional<String>,
						Function<MHAA,
							Stream<Throwable>>>,
							List<Throwable>>>>,
				Function<Function<Boolean,
					Function<Optional<Prefixable>,
					Function<Function<Optional<String>,
						Function<MHAA,
							Stream<Throwable>>>,
							List<Throwable>>>>,
				Function<Boolean, TestRunner>>>>
								partialApply(
			testRunner_ -> setUpRunner -> tearDownRunner ->
					treatSummarily -> prefixable ->
								testRunner_
				.apply(setUpRunner
					.apply(treatSummarily)
					.apply(prefixable))
				.apply(tearDownRunner
					.apply(treatSummarily)
					.apply(prefixable))
				.apply(prefixable),
			testRunner()
				.apply(errorPrinter))
				.apply(LifeCycleRunner.runner3(
					nonCandidate(candidateMethods,
						invoker.setUps().stock()),
					errorPrinter,
					invoker.setUps()))
				.apply(LifeCycleRunner.runner3(
					nonCandidate(candidateMethods,
						invoker.tearDowns().stock()),
					errorPrinter,
					invoker.tearDowns()));
		final Function<Function<String,
					Function<List<Throwable>, Result>>,
				Function<Function<Boolean,
					Function<Optional<Prefixable>,
							List<Throwable>>>,
				Function<Boolean,
				Consumer<Optional<Prefixable>>>>>
							lifeCycleReporter =
							lifeCycleReporter()
			.apply(reporter);
		runner = runner()
			.apply((nonCandidate(candidateMethods,
						invoker.setUpAlls()
								.stock()))
				? prefixable -> { }
				: lifeCycleReporter
					.apply(namePrefix -> errors -> RESULTABLE
							.newSetUpAllResult(
						toMap(of(MethodKind.SET_UP_ALL,
								errors))))
					.apply(LifeCycleRunner.runner2(
						false,
						errorPrinter,
						invoker.setUpAlls(),
						staticInvoker()))
					.apply(true))
			.apply((nonCandidate(candidateMethods,
						invoker.tearDownAlls()
								.stock()))
				? prefixable -> { }
				: lifeCycleReporter
					.apply(namePrefix -> errors -> RESULTABLE
							.newTearDownAllResult(
						toMap(of(MethodKind.TEAR_DOWN_ALL,
								errors))))
					.apply(LifeCycleRunner.runner2(
						false,
						errorPrinter,
						invoker.tearDownAlls(),
						staticInvoker()))
					.apply(true))
			.apply(Support.<List<Set<Method>>,
					Function<Prefixable, PrefixableMethods>>
								partialApply(
				candidates -> prefixable ->
						new PrefixableMethods(
								prefixable,
								candidates
					.stream()
					.map(Support.<Prefixable,
						UnaryOperator<Set<Method>>>
								partialApply(
							prefixable_ ->
							methods -> methods
						.stream()
						.filter(Support.<String,
								Predicate<Method>>
								partialApply(
							namePrefix -> method ->
									method
								.getName()
								.startsWith(
									namePrefix),
							prefixable_
								.testMethodNamePrefix()))
						.collect(Support
							.toUnmodifiableOrderedSet()),
						prefixable))
					.collect(Collectors.filtering(
						Predicate.not(Set::isEmpty),
						Collectors.toUnmodifiableList()))),
				candidateMethods))
			.apply(Support.<Predicate<Prefixable>,
						Predicate<PrefixableMethods>>
								partialApply(
				nonMatcher -> pair -> ((pair.testMethods()
								.isEmpty())
						? Optional.<Prefixable>of(
							pair.prefixable())
						: Optional.<Prefixable>empty())
					.filter(nonMatcher)
					.isEmpty(),
				Support.<Consumer<Result>, Predicate<Prefixable>>
								partialApply(
					reporter_ -> prefixable -> {
						reporter_.accept(
							RESULTABLE.newOtherResult(
									prefixable
								.testMethodNamePrefix()));
						return true;
					},
					reporter)))
			.apply(tester()
				.apply(reporter)
				.apply(invoker)
				.apply((nonCandidate(candidateMethods,
						invoker.setUpBatches()
								.stock()))
					? treatSummarily -> prefixable -> { }
					: lifeCycleReporter
						.apply(namePrefix -> errors ->
									RESULTABLE
								.newSetUpBatchResult(
							namePrefix,
							toMap(of(MethodKind
								.SET_UP_BATCH,
									errors))))
						.apply(LifeCycleRunner.runner2(
							false,
							errorPrinter,
							invoker.setUpBatches(),
							staticInvoker())))
				.apply((nonCandidate(candidateMethods,
						invoker.tearDownBatches()
								.stock()))
					? treatSummarily -> prefixable -> { }
					: lifeCycleReporter
						.apply(namePrefix -> errors ->
									RESULTABLE
								.newTearDownBatchResult(
							namePrefix,
							toMap(of(MethodKind
								.TEAR_DOWN_BATCH,
									errors))))
						.apply(LifeCycleRunner.runner2(
							false,
							errorPrinter,
							invoker.tearDownBatches(),
							staticInvoker())))
				.apply((context.continueOnFailure())
					? new CompletingBatchRunner(testRunner,
								inConcurrence)
					: new AbortingBatchRunner(testRunner,
								inConcurrence)));
		prefixables = context.prefixables();
	}

	private static boolean inConcurrence(TestClassContext context,
						Set<ExecutionPolicy> policy)
	{
		return policy.contains(context.executionPolicy());
	}

	private static boolean nonCandidate(Collection<?> c0, Collection<?> c1)
	{
		return (c0.isEmpty() || c1.isEmpty());
	}

////	private static List<MethodKindThrowables> of_(
////				MethodKind kind0, List<Throwable> errors0,
////				MethodKind kind1, List<Throwable> errors1,
////				MethodKind kind2, List<Throwable> errors2)
////	{
////		return Stream.of(new MethodKindThrowables(kind0, errors0))
////			.flatMap(pair0 -> Stream.of(new MethodKindThrowables(
////							kind1, errors1))
////				.flatMap(pair1 -> Stream.of(
////						new MethodKindThrowables(
////							kind2, errors2))
////					.flatMap(pair2 -> Stream.of(pair0,
////								pair1,
////								pair2))))
////			.filter(Predicate.not(pair -> pair.errors().isEmpty()))
////			.collect(Collectors.toUnmodifiableList());
////	}

	private static List<MethodKindThrowables> of(
				MethodKind kind0, List<Throwable> errors0,
				MethodKind kind1, List<Throwable> errors1,
				MethodKind kind2, List<Throwable> errors2)
	{
		return Stream.of(new MethodKindThrowables(kind0, errors0),
				new MethodKindThrowables(kind1, errors1),
				new MethodKindThrowables(kind2, errors2))
			.filter(Predicate.not(pair -> pair.errors().isEmpty()))
			.collect(Collectors.toUnmodifiableList());
	}

	private static List<MethodKindThrowables> of(MethodKind kind,
						List<Throwable> errors)
	{
		return (errors.isEmpty())
			? List.of()
			: List.of(new MethodKindThrowables(kind, errors));
	}

	private static Map<MethodKind, List<Throwable>> toMap(
					List<MethodKindThrowables> pairs)
	{
		return Support.toUnmodifiableEnumMap(
					pairs,
					MethodKind.class,
					MethodKindThrowables::kind,
					pair -> pair.errors().stream(),
					Collectors.toUnmodifiableList());
	}

	private static Function<Consumer<Result>,
				Function<Function<String,
					Function<List<Throwable>, Result>>,
				Function<Function<Boolean,
					Function<Optional<Prefixable>,
							List<Throwable>>>,
				Function<Boolean,
				Consumer<Optional<Prefixable>>>>>>
							lifeCycleReporter()
	{
		return reporter -> resultor -> runner -> treatSummarily ->
							prefixable -> reporter
			.accept(resultor
				.apply(prefixable
					.map(Prefixable::testMethodNamePrefix)
					.orElse(""))
				.apply(runner
					.apply(treatSummarily)
					.apply(prefixable)));
	}

	private static Function<Optional<String>,
				Function<MHAA, Stream<Throwable>>>
							staticInvoker()
	{
		return namePrefix -> mhaa -> MHAA
			.invokeStatic(mhaa, namePrefix)
			.stream();
	}

	private static Function<Testable,
				Function<Optional<String>,
				Function<MHAA, Stream<Throwable>>>>
							virtualInvoker()
	{
		return testable -> namePrefix -> mhaa -> MHAA
			.invokeVirtual(mhaa, testable, namePrefix)
			.stream();
	}

	private static Function<Consumer<Throwable>,
				Function<Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>,
				Function<Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>,
								TestRunner>>>
								testRunner()
	{
		return errorPrinter -> setUpRunner -> tearDownRunner ->
						prefixable -> pair -> {
			final Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>
							virtualInvoker =
							virtualInvoker()
				.apply(pair.testable());
			final List<Throwable> setUpErrors, tearDownErrors,
								testError;

			try {
				setUpErrors = setUpRunner
					.apply(virtualInvoker);
				final Optional<Throwable> error = MHAA
					.invokeVirtual(
						pair.mhaa(),
						pair.testable(),
						prefixable.map(Prefixable
							::testMethodNamePrefix));
				error.ifPresent(errorPrinter);
				testError = error
					.map(List::of)
					.orElseGet(List::of);
			} finally {
				tearDownErrors = tearDownRunner
					.apply(virtualInvoker);
			}

			return of(MethodKind.SET_UP, setUpErrors,
					MethodKind.TEST, testError,
					MethodKind.TEAR_DOWN, tearDownErrors);
		};
	}

	private static Function<Consumer<Result>,
				Function<GroupingInvoker,
				Function<Function<Boolean,
					Consumer<Optional<Prefixable>>>,
				Function<Function<Boolean,
					Consumer<Optional<Prefixable>>>,
				Function<BatchRunner,
				Predicate<PrefixableMethods>>>>>> tester()
	{
		return reporter -> invoker -> setUpBatchReporter ->
						tearDownBatchReporter ->
						batchRunner ->
						pair -> {
			final Prefixable prefixable = pair.prefixable();
			final boolean treatSummarily = prefixable
				.lifeCycleMethodNamePrefixes()
				.isEmpty();

			try {
				setUpBatchReporter
					.apply(treatSummarily)
					.accept(Optional.of(prefixable));
				final Result result = batchRunner.run(
					treatSummarily,
					prefixable,
					invoker.unfoldEach(pair
							.testMethods()));
				reporter.accept(result);
				return batchRunner.continueOnFailure(result);
			} finally {
				tearDownBatchReporter
					.apply(treatSummarily)
					.accept(Optional.of(prefixable));
			}
		};
	}

	private static Function<Consumer<Optional<Prefixable>>,
				Function<Consumer<Optional<Prefixable>>,
				Function<Function<Prefixable,
							PrefixableMethods>,
				Function<Predicate<PrefixableMethods>,
				Function<Predicate<PrefixableMethods>,
				Predicate<Set<? extends Prefixable>>>>>>>
								runner()
	{
		return setUpAllReporter -> tearDownAllReporter ->
						namePrefixMatchingMapper ->
						namePrefixNonMatcher ->
						tester ->
						prefixables -> {
			try {
				setUpAllReporter.accept(Optional.empty());
				return prefixables
					.stream()
					.map(namePrefixMatchingMapper)
					.filter(namePrefixNonMatcher)
					.allMatch(tester);
			} finally {
				tearDownAllReporter.accept(Optional.empty());
			}
		};
	}

	/**
	 * Runs batches of public prefix-matching ({@code test} by default)
	 * instance methods, along with any implemented life cycle methods,
	 * and reports errors, if any, for every batch to a stream.
	 *
	 * @return {@code true} when either all prefix-matching test methods
	 *	have been run or there are no prefix-matching test methods to
	 *	run, otherwise {@code false}
	 * @see org.zzzyxwvut.ate.Tester#runAndReport()
	 */
	boolean test()			{ return runner.test(prefixables); }

	/**
	 * A functional interface that takes an optional with prefixable
	 * attributes and returns a functional interface that takes a pair of
	 * a test class instance and a method handle with its arguments, if
	 * any, and returns a list of method-kind-and-throwable-list pairs.
	 */
	interface TestRunner extends Function<Optional<Prefixable>,
			Function<TestableMHAA, List<MethodKindThrowables>>> { }

	/**
	 * This class serves for running batches of public prefix-matching
	 * ({@code test} by default) instance methods, along with any
	 * implemented {@code setUp} and {@code tearDown} life cycle methods.
	 */
	private static abstract sealed class BatchRunner
	{
		private static final Resultable RESULTABLE = ResultFriend
							.getResultable();

		/** Constructs a new {@code BatchRunner} object. */
		BatchRunner() { }

		/**
		 * Returns whether to continue execution after failure.
		 *
		 * @param result a result summary for a run batch of test
		 *	methods
		 * @return whether to continue execution after failure
		 */
		abstract boolean continueOnFailure(Result result);

		/**
		 * Runs a batch of public prefix-matching test methods.
		 *
		 * @param treatSummarily whether processing of some set of
		 *	prefixes for life cycle method names to match should
		 *	be forgone
		 * @param prefixable prefixable attributes
		 * @param pairs a pair set list of a test class instance and
		 *	a method handle with its arguments, if any, matching
		 *	a test method name prefix
		 * @return the result summary for a run batch of test methods
		 */
		abstract Result run(boolean treatSummarily,
					Prefixable prefixable,
					List<Set<TestableMHAA>> pairs);

		/**
		 * Creates the result summary of test execution.
		 *
		 * @param testMethodNamePrefix the matched prefix for test
		 *	method names
		 * @param pairs a pair list of the method kind origin of
		 *	caught errors, if any, and the list of caught errors,
		 *	if any
		 * @param passer a functional interface that takes a total
		 *	count of failed tests and returns a total count of
		 *	passed tests
		 * @return the result summary of test execution
		 */
		static Result newTestResult(String testMethodNamePrefix,
					List<MethodKindThrowables> pairs,
					IntUnaryOperator passer)
		{
			Objects.requireNonNull(testMethodNamePrefix,
						"testMethodNamePrefix");
			Objects.requireNonNull(pairs, "pairs");
			Objects.requireNonNull(passer, "passer");
			final Map<MethodKind, List<Throwable>> errata =
						Support.toUnmodifiableEnumMap(
					pairs,
					MethodKind.class,
					MethodKindThrowables::kind,
					pair -> pair.errors().stream(),
					Collectors.toUnmodifiableList());
			final int failureTotal = Objects.requireNonNullElseGet(
						errata.get(MethodKind.TEST),
						List::of)
				.size();
			return RESULTABLE.newTestResult(
					testMethodNamePrefix,
					errata,
					failureTotal,
					passer.applyAsInt(failureTotal));
		}
	}

	/**
	 * A {@code BatchRunner} that aborts further execution of test methods
	 * on failure.
	 */
	private static final class AbortingBatchRunner extends BatchRunner
	{
		private final Function<Map<MethodKind,
						Queue<MethodKindThrowables>>,
					Function<Boolean,
					Function<Prefixable,
					ToLongFunction<Set<TestableMHAA>>>>>
								invoker;
		private final Supplier<Map<MethodKind,
						Queue<MethodKindThrowables>>>
							errorsSupplier;

		/**
		 * Constructs a new {@code AbortingBatchRunner} object.
		 *
		 * @param testRunner a partially-applied function for running
		 *	{@code setUp}, {@code test}, and {@code tearDown}
		 *	methods that returns a functional interface that takes
		 *	a value whether processing of some set of prefixes for
		 *	life cycle method names to match should be forgone and
		 *	returns a functional interface that takes an optional
		 *	with prefixable attributes and returns a functional
		 *	interface that takes a pair of a test class instance
		 *	and a method handle with its arguments, if any, and
		 *	returns a list of method-kind-and-throwable-list
		 *	pairs
		 * @param inConcurrence whether a concurrent execution policy
		 *	of {@code test} methods is preferred
		 */
		AbortingBatchRunner(Function<Boolean, TestRunner> testRunner,
							boolean inConcurrence)
		{
			Objects.requireNonNull(testRunner, "testRunner");
			errorsSupplier = (inConcurrence)
				? () -> errors(ConcurrentLinkedDeque::new)
				: () -> errors(ArrayDeque::new);
			invoker = (inConcurrence)
				? invoker()
					.apply(pairs -> Support
						.concurrentOrSequential(pairs,
									true))
					.apply(testRunner)
				: invoker()
					.apply(Set::stream)
					.apply(testRunner);
		}

		private static Map<MethodKind, Queue<MethodKindThrowables>>
								errors(
			Supplier<Queue<MethodKindThrowables>> pairsSupplier)
		{
			/*
			 * According to SingleClassTester#testRunner(), three
			 * kinds of method-origin errors are collectable by
			 * TestRunner: SET_UP, TEST, TEAR_DOWN.
			 */
			final Map<MethodKind, Queue<MethodKindThrowables>>
								errors =
					new EnumMap<>(MethodKind.class);
			errors.put(MethodKind.SET_UP, pairsSupplier.get());
			errors.put(MethodKind.TEST, pairsSupplier.get());
			errors.put(MethodKind.TEAR_DOWN, pairsSupplier.get());
			return Collections.unmodifiableMap(errors);
		}

		private static Function<Map<MethodKind,
						Queue<MethodKindThrowables>>,
					Predicate<List<MethodKindThrowables>>>
							errorCollector()
		{
			return errors -> pairs -> pairs
				.stream()
				.reduce(false,
					/*
					 * Collect all exceptions and signal
					 * to continue execution unless a TEST
					 * method-kind-origin exception has
					 * been processed.
					 * (Note that ascertaining non-emptiness
					 * of the TEST kind queue, instead of
					 * poisoning, cannot be applied to
					 * concurrent method execution in view
					 * of possibly having to process pairs
					 * with non-TEST-originated exceptions
					 * only, i.e. when test methods return
					 * normally, but some life cycle
					 * methods raise exceptions, and so
					 * requiring to treat such results as
					 * "passing".)
					 */
					Support.<Map<MethodKind,
							Queue<MethodKindThrowables>>,
						BiFunction<Boolean,
							MethodKindThrowables,
								Boolean>>
								partialApply(
						errors_ -> (value, pair) -> {
							errors_.get(pair.kind())
								.offer(pair);
							return (value
								|| (MethodKind.TEST
								== pair.kind()));
						}, /* A TEST error poison. */
						errors),
					Support.uoeThrower());
		}

		private static UnaryOperator<Predicate<List<MethodKindThrowables>>>
							nonErrorMatcher()
		{
			return errorCollector -> pairs -> ((pairs.isEmpty())
					? Optional.<List<MethodKindThrowables>>
								empty()
					: Optional.<List<MethodKindThrowables>>
								of(pairs))
				.filter(errorCollector)
				.isEmpty();
		}

		private static Function<Function<Set<TestableMHAA>,
							Stream<TestableMHAA>>,
					Function<Function<Boolean, TestRunner>,
					Function<Map<MethodKind,
							Queue<MethodKindThrowables>>,
					Function<Boolean,
					Function<Prefixable,
					ToLongFunction<Set<TestableMHAA>>>>>>>
								invoker()
		{
			return streamer -> testRunner ->
						errors -> treatSummarily ->
						prefixable -> pairs ->
								streamer
				.apply(pairs)
				.map(testRunner
					.apply(treatSummarily)
					.apply(Optional.of(prefixable)))
				.takeWhile(nonErrorMatcher()
					.apply(errorCollector()
						.apply(errors)))
				.count();
		}

		@Override
		boolean continueOnFailure(Result result)
		{
			Objects.requireNonNull(result, "result");
			return Optional.ofNullable(result
					.errors()
					.get(MethodKind.TEST))
				.filter(Predicate.not(List::isEmpty))
				.isEmpty();
		}

		@Override
		Result run(boolean treatSummarily,
					Prefixable prefixable,
					List<Set<TestableMHAA>> pairs)
		{
			Objects.requireNonNull(prefixable, "prefixable");
			Objects.requireNonNull(pairs, "pairs");
			final Map<MethodKind, Queue<MethodKindThrowables>>
						errors = errorsSupplier.get();

			/* Collections take half-of-_int_-size elements. */
			final int successTotal = (int) pairs
				.stream()
				.takeWhile(Support.<Map<MethodKind,
							Queue<MethodKindThrowables>>,
						Predicate<Set<TestableMHAA>>>
								partialApply(
					errors_ -> stub -> errors_
						.get(MethodKind.TEST)
						.isEmpty(),
					errors))
				.mapToLong(invoker
					.apply(errors)
					.apply(treatSummarily)
					.apply(prefixable))
				.sum();
			return newTestResult(prefixable
						.testMethodNamePrefix(),
				errors.values()
					.stream()
					.flatMap(Queue::stream)
					.collect(Collectors
						.toUnmodifiableList()),
				Support.<Integer, IntUnaryOperator>
								partialApply(
					successTotal_ -> stub -> successTotal_,
					successTotal));
		}
	}

	/**
	 * A {@code BatchRunner} that continues execution of test methods
	 * on failure.
	 */
	private static final class CompletingBatchRunner extends BatchRunner
	{
		private final Function<Boolean,
				Function<Prefixable,
				Function<Set<TestableMHAA>,
					Stream<List<MethodKindThrowables>>>>>
								invoker;

		/**
		 * Constructs a new {@code CompletingBatchRunner} object.
		 *
		 * @param testRunner a partially-applied function for running
		 *	{@code setUp}, {@code test}, and {@code tearDown}
		 *	methods that returns a functional interface that takes
		 *	a value whether processing of some set of prefixes for
		 *	life cycle method names to match should be forgone and
		 *	returns a functional interface that takes an optional
		 *	with prefixable attributes and returns a functional
		 *	interface that takes a pair of a test class instance
		 *	and a method handle with its arguments, if any, and
		 *	returns a list of method-kind-and-throwable-list
		 *	pairs
		 * @param inConcurrence whether a concurrent execution policy
		 *	of {@code test} methods is preferred
		 */
		CompletingBatchRunner(Function<Boolean, TestRunner> testRunner,
							boolean inConcurrence)
		{
			Objects.requireNonNull(testRunner, "testRunner");
			invoker = (inConcurrence)
				? concurrentInvoker()
					.apply(testRunner)
				: sequentialInvoker()
					.apply(testRunner);
		}

		private static Function<Function<Boolean, TestRunner>,
					Function<Boolean,
					Function<Prefixable,
					Function<Set<TestableMHAA>,
						Stream<List<MethodKindThrowables>>>>>>
							concurrentInvoker()
		{
			return testRunner -> treatSummarily ->
						prefixable -> pairs -> Support
				.concurrentOrSequential(pairs, true)
				.map(testRunner
					.apply(treatSummarily)
					.apply(Optional.of(prefixable)))
				.collect(Collectors.toUnmodifiableList())
				.stream();
		}

		private static Function<Function<Boolean, TestRunner>,
					Function<Boolean,
					Function<Prefixable,
					Function<Set<TestableMHAA>,
						Stream<List<MethodKindThrowables>>>>>>
							sequentialInvoker()
		{
			return testRunner -> treatSummarily ->
						prefixable -> pairs -> pairs
				.stream()
				.map(testRunner
					.apply(treatSummarily)
					.apply(Optional.of(prefixable)));
		}

		@Override
		boolean continueOnFailure(Result result)
		{
			Objects.requireNonNull(result, "result");
			return true;
		}

		@Override
		Result run(boolean treatSummarily,
					Prefixable prefixable,
					List<Set<TestableMHAA>> pairs)
		{
			Objects.requireNonNull(prefixable, "prefixable");
			Objects.requireNonNull(pairs, "pairs");
			return newTestResult(prefixable
						.testMethodNamePrefix(),
				pairs.stream()
					.flatMap(Support.<Function<Set<TestableMHAA>,
							Stream<List<MethodKindThrowables>>>,
						Function<Set<TestableMHAA>,
							Stream<MethodKindThrowables>>>
								partialApply(
						invoker_ -> pairs_ -> invoker_
							.apply(pairs_)
							.flatMap(List::stream),
						invoker.apply(treatSummarily)
							.apply(prefixable)))
					.collect(Collectors
						.toUnmodifiableList()),
				Support.<Integer, IntUnaryOperator>
								partialApply(
				total -> failureTotal -> total - failureTotal,
				(int) pairs.stream()
					.flatMap(Set::stream)
					.count()));
		}	/* Collections take half-of-_int_-size elements. */
	}

	/** This class serves for running implemented life cycle methods. */
	private static final class LifeCycleRunner
	{
		private LifeCycleRunner()	{ /* No instantiation. */ }

		private static Function<Consumer<Throwable>,
				Function<Boolean,
				Function<PrefixMatcher,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
				Function<OneClassMHAAs, Stream<Throwable>>>>>>>
							lifeCycleRunner()
		{
			return errorPrinter -> stub -> matcher -> prefixable ->
						invoker -> pair -> matcher
				.apply(prefixable
					.map(Prefixable
						::lifeCycleMethodNamePrefixes)
					.orElseGet(Set::of))
				.apply(pair)

				/*
				 * See CompletingBatchRunner#concurrentInvoker()
				 * for a taste of nested concurrent gymnastics.
				 */
				.flatMap(invoker
					.apply(prefixable
						.map(Prefixable
							::testMethodNamePrefix)))
				.peek(errorPrinter); /* Process sequentially. */
		}

		private static Function<Function<PrefixMatcher,
					Function<Optional<Prefixable>,
					Function<Function<Optional<String>,
						Function<MHAA,
							Stream<Throwable>>>,
					Function<OneClassMHAAs,
							Stream<Throwable>>>>>,
				Function<List<Set<MHAA>>,
				Function<PrefixMatcher,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>>>>>
						descendantLifeCycleRunner()
		{
			return lifeCycleRunner -> mhaas -> matcher ->
						prefixable -> invoker -> mhaas
				.stream()
				.flatMap(lifeCycleRunner
					.apply(matcher)
					.apply(prefixable)
					.apply(invoker)
					.compose(mhaas_ -> new OneClassMHAAs(
							Optional.<MHAA>empty(),
							mhaas_)))
				.collect(Collectors.toUnmodifiableList());
		}

		private static Function<Function<PrefixMatcher,
					Function<Optional<Prefixable>,
					Function<Function<Optional<String>,
						Function<MHAA,
							Stream<Throwable>>>,
					Function<OneClassMHAAs,
							Stream<Throwable>>>>>,
				Function<OneClassMHAAs,
				Function<PrefixMatcher,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>>>>>
						siblingLifeCycleRunner()
		{
			return lifeCycleRunner -> pair -> matcher ->
						prefixable -> invoker ->
							lifeCycleRunner
				.apply(matcher)
				.apply(prefixable)
				.apply(invoker)
				.apply(pair)
				.collect(Collectors.toUnmodifiableList());
		}

		private static PrefixMatcherKindLifeCycleRunner runner(
					boolean nonCandidate,
					Consumer<Throwable> errorPrinter,
					OneStockMHAAs pair)
		{
			final boolean withOneValueValue = pair.one()
				.isPresent();
			return new PrefixMatcherKindLifeCycleRunner(
							(withOneValueValue)
				? PrefixMatcherKind.ONE
				: PrefixMatcherKind.CLASS,
							(nonCandidate)
				? matcher -> prefixable -> invoker -> List.of()
				: (withOneValueValue)
					? siblingLifeCycleRunner()
						.apply(lifeCycleRunner()
							.apply(errorPrinter)
							.apply(false))
						.apply(new OneClassMHAAs(
								pair.one(),
								Set.of()))
					: descendantLifeCycleRunner()
						.apply(lifeCycleRunner()
							.apply(errorPrinter)
							.apply(false))
						.apply(pair.stock()));
		}

		private static Function<PrefixMatcherKind,
				Function<Function<PrefixMatcher,
					Function<Optional<Prefixable>,
					Function<Function<Optional<String>,
						Function<MHAA,
							Stream<Throwable>>>,
						List<Throwable>>>>,
				Function<Boolean,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
						List<Throwable>>>>>>
				treatSummarilyForPrefixMatcherSubstitutor()
		{
			return prefixMatcherKind -> runner -> treatSummarily ->
									runner
						.apply((treatSummarily)
				? prefixMatcherKind.summaryMatcher()
				: prefixMatcherKind.filteringMatcher());
		}

		/**
		 * Partially applies a life-cycle-method-runner function to
		 * some of the passed arguments and returns a functional
		 * interface that takes a value whether processing of some set
		 * of prefixes for life cycle method names to match should be
		 * forgone and returns a functional interface that takes
		 * an optional with prefixable attributes and returns
		 * a functional interface that takes a functional interface
		 * that takes an optional with a test method name prefix and
		 * returns a functional interface that takes a method handle
		 * with its arguments, if any, and returns a stream of errors,
		 * if any; and returns a list of errors, if any.
		 *
		 * @apiNote
		 * The return type of this method is suited for the context of
		 * running of instance life cycle methods.
		 *
		 * @param nonCandidate whether a no-op implementation of
		 *	a life-cycle-method-runner function is preferred
		 * @param errorPrinter a functional interface that takes
		 *	an error and does not return a value
		 * @param pair a pair of an optional with a categorised life
		 *	cycle method handle with its arguments, if any, and
		 *	a set of categorised life cycle method handles with
		 *	their arguments, if any
		 * @return a curried function
		 */
		static Function<Boolean,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
							List<Throwable>>>>
								runner3(
				boolean nonCandidate,
				Consumer<Throwable> errorPrinter,
				OneStockMHAAs pair)
		{
			Objects.requireNonNull(errorPrinter, "errorPrinter");
			Objects.requireNonNull(pair, "pair");
			final PrefixMatcherKindLifeCycleRunner runnerPair =
				runner(nonCandidate, errorPrinter, pair);
			return treatSummarilyForPrefixMatcherSubstitutor()
				.apply(runnerPair.prefixMatcherKind())
				.apply(runnerPair.lifeCycleRunner());
		}

		/**
		 * Partially applies a life-cycle-method-runner function to
		 * some of the passed arguments and returns a functional
		 * interface that takes a value whether processing of some set
		 * of prefixes for life cycle method names to match should be
		 * forgone and returns a functional interface that takes
		 * an optional with prefixable attributes and returns a list
		 * of errors, if any.
		 *
		 * @apiNote
		 * The return type of this method is suited for the context of
		 * running of static life cycle methods.
		 *
		 * @param nonCandidate whether a no-op implementation of
		 *	a life-cycle-method-runner function is preferred
		 * @param errorPrinter a functional interface that takes
		 *	an error and does not return a value
		 * @param pair a pair of an optional with a categorised life
		 *	cycle method handle with its arguments, if any, and
		 *	a set of categorised life cycle method handles with
		 *	their arguments, if any
		 * @param invoker a functional interface that takes
		 *	an optional with a test method name prefix and returns
		 *	a functional interface that takes a method handle with
		 *	its arguments, if any, and returns a stream of errors,
		 *	if any
		 * @return a curried function
		 */
		static Function<Boolean,
				Function<Optional<Prefixable>, List<Throwable>>>
								runner2(
				boolean nonCandidate,
				Consumer<Throwable> errorPrinter,
				OneStockMHAAs pair,
				Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>
								invoker)
		{
			Objects.requireNonNull(errorPrinter, "errorPrinter");
			Objects.requireNonNull(pair, "pair");
			Objects.requireNonNull(invoker, "invoker");
			final PrefixMatcherKindLifeCycleRunner runnerPair =
				runner(nonCandidate, errorPrinter, pair);
			return Support.<Function<Optional<String>,
						Function<MHAA, Stream<Throwable>>>,
					Function<Function<Optional<Prefixable>,
						Function<Function<Optional<String>,
							Function<MHAA,
								Stream<Throwable>>>,
							List<Throwable>>>,
					Function<Optional<Prefixable>,
							List<Throwable>>>>
								partialApply(
					invoker_ -> runner -> prefixable ->
									runner
						.apply(prefixable)
						.apply(invoker_),
					invoker)
				.<Boolean>compose(
					treatSummarilyForPrefixMatcherSubstitutor()
						.apply(runnerPair
							.prefixMatcherKind())
						.apply(runnerPair
							.lifeCycleRunner()));
		}

		/**
		 * A functional interface that takes a set of prefixes for
		 * method names to match and returns a functional interface
		 * that takes a pair of an optional with a categorised method
		 * handle with its arguments, if any, and a set of categorised
		 * method handles with their arguments, if any, and returns
		 * a stream of categorised method handles with their
		 * arguments, if any.
		 *
		 * @see Prefixable#lifeCycleMethodNamePrefixes()
		 */
		private interface PrefixMatcher extends Function<Set<String>,
				Function<OneClassMHAAs, Stream<MHAA>>> { }

		/**
		 * This enumeration specifies kinds of prefix-matching
		 * strategies.
		 */
		private enum PrefixMatcherKind
		{
			/**
			 * The prefix matcher kind of the {@code one} pair
			 * value.
			 */
			ONE
			{
				@Override
				PrefixMatcher summaryMatcher()
				{
					return namePrefixes -> pair ->
								pair.one()
						.stream();
				}

				@Override
				PrefixMatcher filteringMatcher()
				{
					return namePrefixes -> pair ->
								namePrefixes
						.stream()
						.filter(Support.<String,
							Predicate<String>>
								partialApply(
							name -> name::startsWith,
							pair.one().orElseThrow(
								IllegalStateException
									::new)
								.methodName))
						.map(Support.<MHAA,
							Function<String, MHAA>>
								partialApply(
							mhaa -> stub -> mhaa,
							pair.one().orElseThrow(
								IllegalStateException
									::new)))
						.findAny()
						.stream();
				}
			},

			/**
			 * The prefix matcher kind of the {@code klass} pair
			 * value.
			 */
			CLASS
			{
				@Override
				PrefixMatcher summaryMatcher()
				{
					return namePrefixes -> pair ->
								pair.klass()
						.stream();
				}

				@Override
				PrefixMatcher filteringMatcher()
				{
					return namePrefixes -> pair ->
								pair.klass()
						.stream()
						.filter(Support.<Set<String>,
							Predicate<MHAA>>
								partialApply(
							prefixes -> mhaa -> prefixes
									.stream()
									.anyMatch(
								Support.<String,
								Predicate<String>>
									partialApply(
								name -> name
									::startsWith,
								mhaa.methodName)),
							namePrefixes));
				}
			};

			/**
			 * Returns a {@link PrefixMatcher} that makes a stream
			 * out of either the {@code one} or the {@code klass}
			 * applied pair value and forgoes processing
			 * the applied set of prefixes for life cycle method
			 * names to match.
			 *
			 * @return a curried function
			 */
			abstract PrefixMatcher summaryMatcher();

			/**
			 * Returns a {@link PrefixMatcher} that makes a stream
			 * out of either the {@code one} or the {@code klass}
			 * applied pair value and then discards each
			 * categorised method handle with its arguments, if
			 * any, whose method name fails to match any of
			 * prefixes from the applied set of prefixes for life
			 * cycle method names to match.
			 *
			 * @return a curried function
			 */
			abstract PrefixMatcher filteringMatcher();
		}

		private record OneClassMHAAs(Optional<MHAA> one,
							Set<MHAA> klass)
		{
			/**
			 * Constructs a new {@code OneClassMHAAs} object.
			 *
			 * @param one an optional with a categorised life
			 *	cycle method handle with its arguments, if any
			 * @param klass a set of categorised life cycle method
			 *	handles with their arguments, if any
			 */
			OneClassMHAAs
			{
				Objects.requireNonNull(one, "one");
				Objects.requireNonNull(klass, "klass");
			}
		}

		private record PrefixMatcherKindLifeCycleRunner(
					PrefixMatcherKind prefixMatcherKind,
					Function<PrefixMatcher,
				Function<Optional<Prefixable>,
				Function<Function<Optional<String>,
					Function<MHAA, Stream<Throwable>>>,
						List<Throwable>>>>
							lifeCycleRunner)
		{
			/**
			 * Constructs a new {@code PrefixMatcherKindLifeCycleRunner}
			 * object.
			 *
			 * @param prefixMatcherKind kinds of prefix-matching
			 *	strategies
			 * @param lifeCycleRunner a partially-applied
			 *	life-cycle-method-runner function that returns
			 *	a functional interface that takes
			 *	a {@link PrefixMatcher} function and returns
			 *	a functional interface that takes an optional
			 *	with prefixable attributes and returns
			 *	a functional interface that takes a functional
			 *	interface that takes an optional with a test
			 *	method name prefix and returns a functional
			 *	interface that takes a method handle with its
			 *	arguments, if any, and returns a stream of
			 *	errors, if any; and returns a list of errors,
			 *	if any
			 */
			PrefixMatcherKindLifeCycleRunner
			{
				Objects.requireNonNull(prefixMatcherKind,
							"prefixMatcherKind");
				Objects.requireNonNull(lifeCycleRunner,
							"lifeCycleRunner");
			}
		}
	}

	private record PrefixableMethods(Prefixable prefixable,
						List<Set<Method>> testMethods)
	{
		/**
		 * Constructs a new {@code PrefixableMethods} object.
		 *
		 * @param prefixable prefixable attributes
		 * @param testMethods a set list of test methods
		 */
		PrefixableMethods
		{
			Objects.requireNonNull(prefixable, "prefixable");
			Objects.requireNonNull(testMethods, "testMethods");
		}
	}

	private record MethodKindThrowables(MethodKind kind,
						List<Throwable> errors)
	{
		/**
		 * Constructs a new {@code MethodKindThrowables} object.
		 *
		 * @param kind a method kind origin of caught errors, if any
		 * @param errors a list of caught errors, if any
		 */
		MethodKindThrowables
		{
			Objects.requireNonNull(kind, "kind");
			Objects.requireNonNull(errors, "errors");
		}

		@Override
		public int hashCode()
		{
			return System.identityHashCode(this);
		}

		@Override
		public boolean equals(Object that)
		{
			return (this == that);
		}
	}
}
