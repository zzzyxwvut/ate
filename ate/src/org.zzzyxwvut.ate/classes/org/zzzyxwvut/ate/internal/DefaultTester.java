package org.zzzyxwvut.ate.internal;

import java.util.Formatter;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;
import org.zzzyxwvut.ate.configuration.Configurable;

/**
 * A {@code Tester} that serves for arranging a group of classes for testing.
 */
final class DefaultTester implements Tester
{
	/* See ECMA-48 CSI sequences in console_codes(4). */
	private static final String UNASSIGNABLE_YELLOW =
					"\033[1;7;33mWARNING\033[0m: %s <-- "
				+ "(<org.zzzyxwvut.ate.Status, >%s): "
				+ "unassignable argument(s)%n";
	private static final String UNASSIGNABLE_PLAIN = "WARNING: %s <-- "
				+ "(<org.zzzyxwvut.ate.Status, >%s): "
				+ "unassignable argument(s)%n";

	private final Set<Class<? extends Testable>> classes;
	private final Predicate<Set<Class<? extends Testable>>> runner;

	/**
	 * Constructs a new {@code DefaultTester} object.
	 *
	 * @param testClasses a set of test classes
	 * @param executionPolicy an execution policy for class testing
	 * @param configurable an optional with configurable testing attributes
	 * @param options a set of predicative options
	 * @throws java.util.IllegalFormatException if a malformed templet was
	 *	assigned to the value of the {@link Tester#PROPERTY_TEMPLET_UNASSIGNABLE}
	 *	property value
	 */
	DefaultTester(Set<Class<? extends Testable>> testClasses,
					ExecutionPolicy executionPolicy,
					Optional<Configurable> configurable,
					Set<Option> options)
	{
		Objects.requireNonNull(testClasses, "testClasses");
		Objects.requireNonNull(executionPolicy, "executionPolicy");
		Objects.requireNonNull(configurable, "configurable");
		Objects.requireNonNull(options, "options");
		final String templet = Optional
			.ofNullable(System.getProperty(
					Tester.PROPERTY_TEMPLET_UNASSIGNABLE))
			.orElseGet(Support.<Set<Option>, Supplier<String>>
								partialApply(
				options_ -> () -> (options_.contains(
						Option.RENDER_OUTPUT_PLAIN))
					? UNASSIGNABLE_PLAIN
					: UNASSIGNABLE_YELLOW,
				options));

		/* DefaultTester.UNASSIGNABLE_PLAIN =~ "%s %s". */
		new Formatter(new StringBuilder(128))
			.format(templet, ".", ".");
		final ClassMethodNameFilter filter = new ClassMethodNameFilter(
				System.getProperty(Tester.PROPERTY_SOME),
				System.getProperty(Tester.PROPERTY_TOKENS));
		classes = ((filter.useDefaultSelection())
				? testClasses
					.stream()
				: testClasses
					.stream()
					.filter(Support.<ClassMethodNameFilter,
						Predicate<Class<? extends Testable>>>
								partialApply(
						filter_ -> filter_
							::isRequestedClass,
						filter)))
			.collect(Support.toUnmodifiableSet(executionPolicy
					== ExecutionPolicy.SEQUENTIAL));
		runner = runner()
			.apply(tester()
				.apply(templet)
				.apply(filter)
				.apply(options)
				.apply(configurable))
			.apply(executionPolicy
					== ExecutionPolicy.CONCURRENT);
	}

	private static Function<String,
				Function<ClassMethodNameFilter,
				Function<Set<Option>,
				Function<Optional<Configurable>,
				Function<Class<? extends Testable>, Boolean>>>>>
								tester()
	{
		return unassignableTemplet -> filter -> options ->
						configurable -> testClass ->
							new SingleClassTester(
				new TestClassContext(testClass, configurable,
							options, filter,
							unassignableTemplet))
			.test();
	}

	private static Function<Function<Class<? extends Testable>, Boolean>,
				Function<Boolean,
				Predicate<Set<Class<? extends Testable>>>>>
								runner()
	{
		return tester -> inConcurrence -> classes -> Support
			.concurrentOrSequential(classes, inConcurrence)
			.map(tester)
			.reduce(true, (left, right) -> left && right);
	}

	@Override
	public boolean runAndReport()	{ return runner.test(classes); }
}
