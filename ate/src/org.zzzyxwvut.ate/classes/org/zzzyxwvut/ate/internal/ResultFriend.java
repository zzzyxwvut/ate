package org.zzzyxwvut.ate.internal;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result;

/** A friend class of {@code Result}. */
public final class ResultFriend
{
	private static Resultable resultable;

	private ResultFriend() { /* No instantiation. */ }

	/**
	 * Stores the passed instance of {@code Resultable}.
	 *
	 * @param resultable an instance of {@code Resultable}
	 */
	public static void setResultable(Resultable resultable)
	{
		Objects.requireNonNull(resultable, "resultable");

		if (ResultFriend.resultable == null)
			ResultFriend.resultable = resultable;
	}

	/**
	 * Returns the stored instance of {@code Resultable}.
	 *
	 * @return the stored instance of {@code Resultable}
	 */
	public static Resultable getResultable()
	{
		if (!ResultHolder.INITIALISED)
			throw new IllegalStateException("My mind is going...");

		return resultable;
	}

	/** A collection of {@code Result} factory methods. */
	public interface Resultable
	{
		/**
		 * Creates a new {@code OtherResult} object with an empty
		 * errors map.
		 *
		 * @param testMethodNamePrefix a non-matching prefix of test
		 *	method names
		 * @return a new {@code OtherResult} object
		 */
		Result.OtherResult newOtherResult(String testMethodNamePrefix);

		/**
		 * Creates a new {@code SetUpAllResult} object.
		 *
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @return a new {@code SetUpAllResult} object
		 */
		Result.SetUpAllResult newSetUpAllResult(
				Map<MethodKind, List<Throwable>> errors);

		/**
		 * Creates a new {@code TearDownAllResult} object.
		 *
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @return a new {@code TearDownAllResult} object
		 */
		Result.TearDownAllResult newTearDownAllResult(
				Map<MethodKind, List<Throwable>> errors);

		/**
		 * Creates a new {@code SetUpBatchResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @return a new {@code SetUpBatchResult} object
		 */
		Result.SetUpBatchResult newSetUpBatchResult(
				String testMethodNamePrefix,
				Map<MethodKind, List<Throwable>> errors);

		/**
		 * Creates a new {@code TearDownBatchResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @return a new {@code TearDownBatchResult} object
		 */
		Result.TearDownBatchResult newTearDownBatchResult(
				String testMethodNamePrefix,
				Map<MethodKind, List<Throwable>> errors);

		/**
		 * Creates a new {@code TestResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @param failureTotal a number of failed name-matching tests
		 * @param successTotal a number of passed name-matching tests
		 * @return a new {@code TestResult} object
		 */
		Result.TestResult newTestResult(String testMethodNamePrefix,
					Map<MethodKind, List<Throwable>> errors,
					int failureTotal,
					int successTotal);
	}

	private static final class ResultHolder
	{
		/** Whether the client class was initialised. */
		static final boolean INITIALISED;

		static {
			try {
				INITIALISED = Modifier.isAbstract(
							Class.forName(
					"org.zzzyxwvut.ate.Result",
					true,
					ResultFriend.class.getClassLoader())
						.getModifiers());
			} catch (final ClassNotFoundException e) {
				throw new ExceptionInInitializerError(e);
			}
		}

		private ResultHolder() { /* No instantiation. */ }
	}
}
