package org.zzzyxwvut.ate.internal;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.Tester;

/** This class applies a name filter to classes and methods. */
final class ClassMethodNameFilter
{
	private static final int TOKEN_COUNT = 3;
	private static final Function<String,
				Predicate<Entry<PatternKey, List<Pattern>>>>
						REQUESTED_METHODS_MATCHER =
							ClassMethodNameFilter
				.<Entry<PatternKey, List<Pattern>>>matcher()
		.apply(entry -> entry.getKey().pattern);
	private static final Function<String, Predicate<PatternKey>>
						IS_REQUESTED_CLASS_MATCHER =
				ClassMethodNameFilter.<PatternKey>matcher()
		.apply(key -> key.pattern);
	private static final Function<String,
				Function<String, Map<PatternKey, List<Pattern>>>>
								CACHE_MAPPER =
								parser()
		.apply(Support.<Function<String,
				Function<String, Map<PatternKey,
							List<Pattern>>>>,
				Function<String,
				Function<String, Map<PatternKey,
							List<Pattern>>>>>
								partialApply(
			parser -> tokens -> some -> Optional
				.ofNullable(some)
				.filter(Predicate.not(String::isBlank))
				.map(parser
					.apply(tokens))
				.orElseGet(Map::of),
			parser().apply(mapper())));
	private static final Function<String,
				Function<String, Map<PatternKey, List<Pattern>>>>
								PARSER;
	private static final DelegatingMapValueMap<String, PatternKey,
							List<Pattern>> CACHE;

	static {
		final int limit = Integer.getInteger(
					Tester.PROPERTY_CACHE_LIMIT, 32);
		final boolean cacheless = (limit < LRUCacheMap.MINIMUM_CAPACITY
				|| limit > LRUCacheMap.MAXIMUM_CAPACITY);
		PARSER = parser()
			.apply((cacheless)
				? mapper()
				: cacheer());
		CACHE = new DelegatingMapValueMap<>((cacheless)
			? Map.of()
			: new LRUCacheMap<>(limit));
	} /* LRUCacheMap<String, UnmodifiableMap<PatternKey, List<Pattern>>> */

	private final Map<PatternKey, List<Pattern>> patterns;

	/**
	 * Constructs a new {@code ClassMethodNameFilter} object.
	 *
	 * @param someValue a string representation of a subset of test
	 *	classes and their test methods, else {@code null}
	 * @param tokensValue tokens to use as delimiters for parsing of
	 *	the passed test-class-subset string, else {@code null}
	 * @see org.zzzyxwvut.ate.Tester#PROPERTY_SOME
	 * @see org.zzzyxwvut.ate.Tester#PROPERTY_TOKENS
	 */
	ClassMethodNameFilter(String someValue, String tokensValue)
	{
		patterns = Optional.ofNullable(someValue)
			.filter(Predicate.not(String::isBlank))
			.map(PARSER
				.apply(tokensValue))
			.orElseGet(Map::of);
	}	/* Eschew from having a null cache key. */

	private static Function<Function<String,
					Function<String, Map<PatternKey,
							List<Pattern>>>>,
				Function<String,
				Function<String, Map<PatternKey,
							List<Pattern>>>>>
								parser()
	{
		return mapper -> tokensValue -> someValue -> mapper
			.apply(tokensValue)
			.apply(someValue);
	}

	private static Queue<Pattern> tokens()
	{
		return new ArrayDeque<>(List.of(Pattern.compile(":"),
						Pattern.compile("@"),
						Pattern.compile(",")));
	}	/* Supply #TOKEN_COUNT elements. */

	private static Function<Pattern,
				Function<Pattern,
				Function<String, List<String>>>> valuer()
	{
		return leftToken -> rightToken -> text -> rightToken
			.splitAsStream(leftToken
				.splitAsStream(text)
				.filter(Predicate.not(String::isBlank))
				.skip(1L)	/* Drop the key prefix. */
				.collect(Collectors.joining(
						leftToken.pattern())))
			.filter(Predicate.not(String::isBlank))
			.collect(Collector.of(
				LinkedList<String>::new,
				Deque::offerFirst, /* Right-to-left order. */
				Support.uoeThrower(),
				Collections::unmodifiableList));
	}

	private static Function<Predicate<String>,
				Function<Pattern,
				Function<String, PatternKey>>> keyer()
	{
		return tester -> token -> text -> token
			.splitAsStream(text)
			.filter(Predicate.<String>not(String::isBlank)
				.and(tester))
			.findFirst()
			.map(PatternKey::newPatternKey)
			.orElse(PatternKey.EMPTY);
	}

	private static Map<PatternKey, List<Pattern>> doParse(
					Pattern token,
					Function<String, PatternKey> keyer,
					Function<String, List<String>> valuer,
					String text)
	{
		record PatternKeyValues(PatternKey patternKey,
						List<String> values) { }

		class PatternCollector
		{
			private final Map<PatternKey, List<String>> patterns =
							new LinkedHashMap<>();

			PatternCollector() { }

			void collect(PatternKeyValues pair)
			{
				patterns.merge(
					pair.patternKey(),
					pair.values(),
					(oldValue, newValue) -> {
						final List<String> value =
							new LinkedList<>(
								oldValue);
						value.addAll(newValue);
						return value;
					});
			}

			Map<PatternKey, List<Pattern>> toMap()
			{
				return patterns
					.entrySet()
					.stream()
					.collect(Collectors.toMap(
						Entry::getKey,
						entry -> entry
							.getValue()
							.stream()
							.distinct()
							.map(Pattern::compile)
							.collect(Collectors
								.toUnmodifiableList()),
						Support.uoeThrower(),
						LinkedHashMap::new));
			}
		}

		return Collections.unmodifiableMap(token
			.splitAsStream(text)
			.filter(Predicate.not(String::isBlank))
			.map(Support.<Function<String, PatternKey>,
					Function<Function<String, List<String>>,
					Function<String, PatternKeyValues>>>
								partialApply(
				keyer_ -> valuer_ -> word -> {
					final PatternKey key = keyer_.apply(
									word);
					return (PatternKey.EMPTY.equals(key))
						? new PatternKeyValues(
							PatternKey.EMPTY,
							List.of())
						: new PatternKeyValues(
							key,
							valuer_.apply(word));
				},
				keyer)
					.apply(valuer))
			.collect(Collector.of(
				LinkedList<PatternKeyValues>::new,
				Deque::offerFirst, /* Right-to-left order. */
				Support.uoeThrower(),
				pairs -> pairs
					.stream()
					.filter(pair -> !PatternKey.EMPTY
							.equals(pair
								.patternKey()))
					.collect(Collector.of(
						PatternCollector::new,
						PatternCollector::collect,
						Support.uoeThrower(),
						PatternCollector::toMap)))));
	}

	private static Map<PatternKey, List<Pattern>> parse(
							String propertyValue,
							Queue<Pattern> tokens)
	{
		/* Retrieve #TOKEN_COUNT elements. */
		final Pattern token0 = tokens.remove();
		final Pattern token1 = tokens.remove();
		final Pattern token2 = tokens.remove();
		return doParse(token0,
			keyer().apply(Support.<Pattern,
						Function<Pattern,
						Predicate<String>>>
								partialApply(
					token1_ -> token2_ -> text ->
						!(text.startsWith(token1_
							.pattern())
						|| text.startsWith(token2_
							.pattern())),
					token1)	/* Reject "-Date.some=,,". */
						.apply(token2))
				.apply(token1),
			valuer().apply(token1)
				.apply(token2),
			propertyValue);
	}

	private static Function<String,
				Function<String,
					Map<PatternKey, List<Pattern>>>>
								mapper()
	{
		return tokensValue -> someValue -> parse(someValue, Optional
				.ofNullable(tokensValue)
				.filter(tokens -> tokens.codePoints().count()
							== TOKEN_COUNT)
				.<Queue<Pattern>>map(tokens -> tokens
					.codePoints()
					.mapToObj(p -> Pattern
						.compile(Character.toString(p)))
					.collect(Collectors
						.toCollection(ArrayDeque::new)))
				.orElseGet(ClassMethodNameFilter::tokens));
	}

	private static Function<String,
				Function<String,
					Map<PatternKey, List<Pattern>>>>
								cacheer()
	{
		return tokensValue -> someValue -> CACHE.computeIfAbsent(
						someValue,
						CACHE_MAPPER
							.apply(tokensValue));
	}

	private static <T> Function<Function<T, Pattern>,
				Function<String,
				Predicate<T>>> matcher()
	{
		return patterner -> klassName -> patternable -> patterner
			.apply(patternable)
			.matcher(klassName)
			.lookingAt();
	}

	/**
	 * Returns an optional list of requested method patterns for the passed
	 * {@code testClass}, if that class component was parsed.
	 * <p>
	 * An empty list denotes that every method of the class is requested.
	 *
	 * @param testClass a test class
	 * @return an optional with the requested method patterns of the class,
	 *	otherwise an empty optional
	 */
	Optional<List<Pattern>> requestedMethods(
					Class<? extends Testable> testClass)
	{
		Objects.requireNonNull(testClass, "testClass");
		return (patterns.isEmpty())
			? Optional.of(List.of())
			: patterns.entrySet()
				.stream()
				.filter(REQUESTED_METHODS_MATCHER
					.apply(testClass.getName()))
				.map(Entry::getValue)
				.findFirst();
	}

	/**
	 * Tests whether the name of the passed {@code testClass} was parsed
	 * as a component.
	 *
	 * @param testClass a test class
	 * @return whether the name of the passed class name was requested
	 */
	boolean isRequestedClass(Class<? extends Testable> testClass)
	{
		Objects.requireNonNull(testClass, "testClass");
		return (patterns.isEmpty() || patterns
			.keySet()
			.stream()
			.anyMatch(IS_REQUESTED_CLASS_MATCHER
				.apply(testClass.getName())));
	}

	/**
	 * Tests whether no parsed components were assigned.
	 *
	 * @return whether no parsed components were assigned
	 */
	boolean useDefaultSelection()	{ return patterns.isEmpty(); }

	@Override
	public String toString()	{ return patterns.toString(); }

	/**
	 * This class adapts instances of {@code Pattern} so that their
	 * regular expressions can be used as map keys.
	 */
	static final class PatternKey
	{
		/**
		 * The empty regular expression instance of {@code PatternKey}.
		 */
		static final PatternKey EMPTY = new PatternKey("");

		/** A compiled representation of a regular expression. */
		final Pattern pattern;

		private PatternKey(String regex)
		{
			pattern = Pattern.compile(regex);
		}

		/**
		 * Creates a {@code PatternKey} object.
		 *
		 * @param regex a regular expression
		 * @return a {@code PatternKey} object
		 */
		static PatternKey newPatternKey(String regex)
		{
			Objects.requireNonNull(regex, "regex");
			return (regex.isEmpty())
				? PatternKey.EMPTY
				: new PatternKey(regex);
		}

		@Override
		public int hashCode()
		{
			return pattern.pattern().hashCode();
		}

		@Override
		public boolean equals(Object that)
		{
			return ((this == that)
				|| ((that instanceof PatternKey pk)
				&& pattern.pattern()
					.equals(pk.pattern.pattern())));
		}

		@Override
		public String toString()	{ return pattern.pattern(); }
	}

	/**
	 * A least-recently-used cache map.
	 * <p>
	 * <strong>This implementation is not synchronized.</strong>
	 *
	 * @param <K> the type of keys
	 * @param <V> the type of values
	 */
	static final class LRUCacheMap<K, V> extends LinkedHashMap<K, V>
	{
		/* See java.util.HashMap#MAXIMUM_CAPACITY */
		static final int MAXIMUM_CAPACITY = 0x40000000;
		static final int MINIMUM_CAPACITY = 2;

		private static final long serialVersionUID = 1L;

		private final int fixedCapacity;

		/**
		 * Constructs a new {@code LRUCacheMap} object.
		 * <p>
		 * The passed capacity value is subject to reduction:
		 * <ol>
		 * <li>round down to the nearest power of two value that is
		 * less than or equal to the passed capacity value
		 * <li>subtract one from the intermediate result
		 * </ol>
		 *
		 * @param capacity a power of two map capacity value
		 * @throws IndexOutOfBoundsException if the passed capacity
		 *	value is out of bounds [2, 0x40000001)
		 */
		LRUCacheMap(int capacity)
		{
			super(((Objects.checkFromToIndex(MINIMUM_CAPACITY,
							capacity,
							MAXIMUM_CAPACITY)
						== MINIMUM_CAPACITY)
					? floorPowerOf2(capacity)
					: MINIMUM_CAPACITY),
				1.0f, true);
			fixedCapacity = floorPowerOf2(capacity) - 1;
		}

		private static int floorPowerOf2(int value)
		{
			if (value < 0)
				throw new IndexOutOfBoundsException(
					"Out of bounds: [0, 0x7fffffff]");

			return 0x80000000 >>> Integer.numberOfLeadingZeros(
								value);
		}	/* See HD 3-2. */

		/**
		 * Returns the fixed capacity of this map.
		 *
		 * @return the fixed capacity of this map
		 * @see #LRUCacheMap(int)
		 */
		int capacity()			{ return fixedCapacity; }

		/**
		 * {@inheritDoc}
		 *
		 * @implSpec
		 * This implementation evicts the current least-recently-used
		 * element from this map whenever newly added elements exceed
		 * the capacity.
		 *
		 * @see #capacity()
		 */
		@Override
		protected boolean removeEldestEntry(Entry<K, V> eldest)
		{
			return (size() > fixedCapacity);
		}

		@Override
		public String toString()
		{
			return String.format("(%d:%d) %s", size(), capacity(),
							super.toString());
		}
	}

	/**
	 * A delegating map suitable for caching.
	 *
	 * @param <K> the type of keys
	 * @param <K1> the type of value map's keys
	 * @param <V1> the type of value map's values
	 */
	static final class DelegatingMapValueMap<K, K1, V1>
	{
		private final Map<K, Map<K1, V1>> map;

		/**
		 * Constructs a new {@code DelegatingMapValueMap} object.
		 * <p>
		 * The passed map shall be wrapped in a synchronised map.
		 *
		 * @param map a map suitable for caching
		 */
		DelegatingMapValueMap(Map<K, Map<K1, V1>> map)
		{
			this.map = Collections.synchronizedMap(
					Objects.requireNonNull(map, "map"));
		}

		/**
		 * Delegates value computation to the namesake method of
		 * the delegated map.
		 *
		 * @param key a key to associate with the computed value
		 * @param mapper a functional interface that takes an object
		 *	key and returns a computed map value, after applying
		 *	the key
		 * @return a value computed by the namesake method of
		 *	the delegated map
		 */
		Map<K1, V1> computeIfAbsent(K key,
			Function<? super K, ? extends Map<K1, V1>> mapper)
		{
			return map.computeIfAbsent(
				Objects.requireNonNull(key, "key"),
				Objects.requireNonNull(mapper, "mapper"));
		}

		/** Removes all of the mappings from the delegated map. */
		void clear()			{ map.clear(); }

		@Override
		public String toString()	{ return map.toString(); }
	}
}
