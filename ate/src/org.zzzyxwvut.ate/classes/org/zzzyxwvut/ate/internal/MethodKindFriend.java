package org.zzzyxwvut.ate.internal;

import java.lang.reflect.Method;
import java.util.Objects;

import org.zzzyxwvut.ate.Result.MethodKind;

/** A friend class of {@code Result.MethodKind}. */
public final class MethodKindFriend
{
	private static MethodKindable methodKindable;

	private MethodKindFriend() { /* No instantiation. */ }

	/**
	 * Stores the passed instance of {@code MethodKindable}.
	 *
	 * @param methodKindable an instance of {@code MethodKindable}
	 */
	public static void setMethodKindable(MethodKindable methodKindable)
	{
		Objects.requireNonNull(methodKindable, "methodKindable");

		if (MethodKindFriend.methodKindable == null)
			MethodKindFriend.methodKindable = methodKindable;
	}

	/**
	 * Returns the stored instance of {@code MethodKindable}.
	 *
	 * @return the stored instance of {@code MethodKindable}
	 */
	public static MethodKindable getMethodKindable()
	{
		if (!MethodKindHolder.INITIALISED)
			throw new IllegalStateException("My mind is going...");

		return methodKindable;
	}

	/** This interface exposes some auxiliary method kind attributes. */
	public interface MethodKindable
	{
		/**
		 * Returns whether method instances that are assignable to
		 * the passed method kind and are implemented across a type
		 * hierarchy require to be processed in their ascending order.
		 *
		 * @param kind a method kind
		 * @return whether method instances that are assignable to
		 *	the passed method kind and are implemented across
		 *	a type hierarchy require to be processed in their
		 *	ascending order
		 */
		boolean inAscendingOrder(MethodKind kind);

		/**
		 * Returns a life cycle method kind for the passed method, if
		 * its name prefix matches, otherwise {@code MethodKind.OTHER}.
		 *
		 * @param method a method
		 * @return a life cycle method kind for the passed method, if
		 *	its name prefix matches, otherwise
		 *	{@code MethodKind.OTHER}
		 */
		MethodKind lifeCycleMethodKindOrOther(Method method);
	}

	private static final class MethodKindHolder
	{
		/** Whether the client enumeration was initialised. */
		static final boolean INITIALISED;

		static {
			try {
				INITIALISED = Class.forName(
					"org.zzzyxwvut.ate.Result$MethodKind",
					true,
					MethodKindFriend.class.getClassLoader())
						.isEnum();
			} catch (final ClassNotFoundException e) {
				throw new ExceptionInInitializerError(e);
			}
		}

		private MethodKindHolder() { /* No instantiation. */ }
	}
}
