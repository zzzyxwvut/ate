package org.zzzyxwvut.ate.internal.adt;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * This class expresses control transfers of tail-recursive method calls in
 * terms of iteration.
 * <p>
 * For example,
 * <blockquote><pre><code>
 * package org.combinatorics;
 *
 * import java.math.BigInteger;
 * import java.util.Objects;
 * import java.util.function.Supplier;
 * import org.zzzyxwvut.ate.internal.adt.Trampoline;
 *
 * public final class Factorial
 * {
 *	private Factorial() { }
 *
 *	private static Trampoline&lt;BigInteger&gt; f(BigInteger number,
 *							BigInteger product)
 *	{
 *		class F implements Supplier&lt;Trampoline&lt;BigInteger&gt;&gt;
 *		{
 *			private final BigInteger number;
 *			private final BigInteger product;
 *
 *			F(BigInteger number, BigInteger product)
 *			{
 *				this.number = number;
 *				this.product = product;
 *			}
 *
 *			&#64;Override
 *			public Trampoline&lt;BigInteger&gt; get()
 *			{
 *				return Factorial.f(
 *					number.subtract(BigInteger.ONE),
 *					product.multiply(number));
 *			}
 *		}
 *
 *		return (BigInteger.ZERO.equals(number))
 *			? Trampoline.land(product)
 *			: Trampoline.bounce(new F(number, product));
 *	}
 *
 *	public static BigInteger factorial(BigInteger number)
 *	{
 *		Objects.requireNonNull(number, "number");
 *
 *		if (BigInteger.ZERO.compareTo(number) &gt; 0)
 *			throw new ArithmeticException("negative number");
 *
 *		return Trampoline.mount(f(number, BigInteger.ONE));
 *	}
 * }</code></pre></blockquote>
 *
 * @param <T> the type of a routine result
 */
public abstract class Trampoline<T>
{
	/** Constructs a new {@code Trampoline} object. */
	Trampoline() { }

	/**
	 * Returns whether there is a pending routine for this trampoline.
	 *
	 * @return whether there is a pending routine for this trampoline
	 */
	abstract boolean bounceable();

	/**
	 * Returns a trampoline after forcing the pending routine for this
	 * trampoline.
	 *
	 * @return a trampoline
	 */
	abstract Trampoline<T> bounce();

	/**
	 * Returns a base routine result.
	 *
	 * @return a base routine result
	 */
	abstract T land();

	/**
	 * Returns a base routine result after performing an initial routine
	 * and each applicable pending routine in turn: by dint of forcing
	 * the immediate routine and connecting it with another routine,
	 * pending or base, one at a time.
	 *
	 * @param <U> the type of a routine result
	 * @param routine a trampoline with an initial routine
	 * @return a base routine result
	 */
	public static <U> U mount(Trampoline<U> routine)
	{
		return Objects.requireNonNull(routine, "routine")
			.land();
	}

	/**
	 * Returns a trampoline with a pending routine after connecting
	 * the current routine, initial or forced, with the pending one.
	 *
	 * @param <U> the type of a routine result
	 * @param routine a trampoline with a pending routine
	 * @return a trampoline with a pending routine
	 */
	public static <U> Trampoline<U> bounce(Supplier<Trampoline<U>> routine)
	{
		class BT<U> extends Trampoline<U>
		{
			private final Supplier<Trampoline<U>> routine;

			BT(Supplier<Trampoline<U>> routine)
			{
				this.routine = routine;
			}

			@Override
			boolean bounceable()	{ return true; }

			@Override
			Trampoline<U> bounce()	{ return routine.get(); }

			@Override
			U land()
			{
				Trampoline<U> trampoline = this;

				do {
					trampoline = trampoline.bounce();
				} while (trampoline.bounceable());

				return trampoline.land();
			}
		}

		Objects.requireNonNull(routine, "routine");
		return new BT<>(routine);
	}

	/**
	 * Returns a trampoline with a base routine result.
	 *
	 * @param <U> the type of a routine result
	 * @param result a base routine result
	 * @return a trampoline with a base routine result
	 */
	public static <U> Trampoline<U> land(U result)
	{
		class LT<U> extends Trampoline<U>
		{
			private final U result;

			LT(U result)		{ this.result = result; }

			@Override
			boolean bounceable()	{ return false; }

			@Override
			Trampoline<U> bounce()
			{
				throw new UnsupportedOperationException();
			}

			@Override
			U land()		{ return result; }
		}

		Objects.requireNonNull(result, "result");
		return new LT<>(result);
	}
}
