package org.zzzyxwvut.ate.internal;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Status;

/**
 * This class manages verification of the assignment compatibility of pairs
 * of formal parameter types of a method and its reference-type arguments.
 *
 * @jls 15.12.4.2 Evaluate Arguments
 */
final class MethodParameter
{
	private static final Map<Class<?>, Class<?>> BOXED_PRIMITIVES =
				Map.of(Boolean.TYPE,	Boolean.class,
					Byte.TYPE,	Byte.class,
					Character.TYPE,	Character.class,
					Double.TYPE,	Double.class,
					Float.TYPE,	Float.class,
					Integer.TYPE,	Integer.class,
					Long.TYPE,	Long.class,
					Short.TYPE,	Short.class);

	private final Predicate<List<?>> matcher;
	private final boolean ineligible;
	private final boolean withStatus;

	private MethodParameter(Predicate<List<?>> matcher,
							boolean ineligible,
							boolean withStatus)
	{
		this.matcher = matcher;
		this.ineligible = ineligible;
		this.withStatus = withStatus;
	}

	private static Function<Step,
				Function<MethodParameterAssignable,
				Function<Iterator<Class<?>>,
				Function<Iterator<?>,
				Supplier<Step>>>>> parameterCollator()
	{
		return varArgsStep -> assignable -> params -> args -> () ->
					(args.hasNext() && params.hasNext())
			? (assignable.is(params.next(),
						args.next(),
						!args.hasNext()
							&& !params.hasNext()))
				? Step.NEXT		/* Continue. */
				: Step.FALSE		/* An unassignable. */
			: (!args.hasNext() && !params.hasNext())
				? Step.TRUE		/* All assignable. */
				: (!args.hasNext()
						&& assignable.tryLastParameter(
									params))
					? varArgsStep	/* Too few? */
					: Step.FALSE;	/* Too many. */
	}

	private static Function<Function<Iterator<Class<?>>,
					Function<Iterator<?>, Supplier<Step>>>,
				Function<MethodParameterAssignable,
				Predicate<List<?>>>> parameterMatcher()
	{
		return collator -> assignable -> arguments -> Stream
			.generate(collator
				.apply(assignable
					.iterator(arguments.size()))
				.apply(arguments.iterator()))
			.dropWhile(Step::next)
			.findFirst()
			.flatMap(Step::value)
			.orElse(false);
	}

	/**
	 * Returns whether the queried method declares no formal parameters or
	 * whether the only formal parameter is of type {@link Status}.
	 *
	 * @return whether the queried method declares no formal parameters or
	 *	whether the only formal parameter is of type {@code Status}
	 */
	boolean ineligible()			{ return ineligible; }

	/**
	 * Returns a functional interface that takes a list of method
	 * arguments and returns a value whether the applied method arguments
	 * are well matched in number and in assignment compatibility with
	 * the formal parameters of the queried method.
	 *
	 * @return a curried function
	 */
	Predicate<List<?>> matcher()		{ return matcher; }

	/**
	 * Returns whether the first formal parameter type, if any,
	 * of the queried method is {@link Status}.
	 *
	 * @return whether the first formal parameter type, if any,
	 *	of the queried method is {@code Status}
	 */
	boolean withStatus()			{ return withStatus; }

	/**
	 * Creates a new {@code MethodParameter} object.
	 *
	 * @param method a queried method
	 * @return a new {@code MethodParameter} object
	 */
	static MethodParameter newInstance(Method method)
	{
		class FAMPA extends AMPA
		{
			FAMPA(Method method)	{ super(method); }

			@Override
			public boolean is(Class<?> parameter,
							Object argument,
							boolean isLastPair)
			{
				return (argument == null
					|| parameter.isInstance(argument));
			}

			@Override
			public Iterator<Class<?>> iterator(int cardinality)
			{
				if (ineligible()) {
					throw new IllegalStateException();
				} else if (cardinality > (TOTAL_LENGTH
							- statusPlace)) {
					/*
					 * According to the implementation of
					 * GroupingInvoker#singleMethodUnfolder()
					 * (see its unreflector), the queried
					 * method _parameters_ are within
					 * total-length limits; considering
					 * that the method in hand may have
					 * been overloaded, do not outright
					 * reject the arguments whose length
					 * is unequal to the arity.
					 */
					throw new IllegalArgumentException(
							"Too many arguments");
				}

				/*
				 * Steer clear of the _all-assignable_ branch
				 * of MethodParameter#parameterCollator().
				 */
				return (cardinality > 0 && cardinality
							!= parameters.size())
					? Collections.emptyIterator()
					: parameters.iterator();
			}

			@Override
			public boolean tryLastParameter(
						Iterator<Class<?>> params)
			{
				return false;
			}
		}

		class VAMPA extends AMPA
		{
			VAMPA(Method method)	{ super(method); }

			@Override
			public boolean is(Class<?> parameter,
							Object argument,
							boolean isLastPair)
			{
				return (argument == null
					|| parameter.isInstance(argument)

					/*
					 * Allow for variable-arity arguments
					 * explicitly passed as an array unit
					 * and a _component_ type standing for
					 * the parameter (see iterator(int)).
					 */
					|| (isLastPair && Optional.ofNullable(argument
								.getClass()
								.getComponentType())
						.filter(Support.<Class<?>,
								Predicate<Class<?>>>
									partialApply(
							param -> param
								::isAssignableFrom,
							parameter))
						.isPresent()));
			}

			@Override
			public Iterator<Class<?>> iterator(int cardinality)
			{
				if (ineligible())
					throw new IllegalStateException();

				final int arity = Math.max(1,
							parameters.size());
				final int cardDiff = Math.abs(cardinality)
								- arity;

				/*
				 * Steer clear of the _all-assignable_ branch
				 * of MethodParameter#parameterCollator().
				 */
				return (cardinality > 0 && cardDiff < -1)
					? Collections.emptyIterator()

					/*
					 * Consider argument passing distinctly
					 * or array-collectively, and pair boxed
					 * parameters and distinct arguments,
					 * e.g., (int...) == (int[]),
					 * (Integer.class, Integer.class).
					 */
					: Stream.concat(parameters.subList(
							0,
							arity - 1)
									.stream(),
								Collections.nCopies(
							Math.max(1, 1 + cardDiff),
							boxParameter(parameters
								.get(arity - 1)
								.getComponentType()))
									.stream())
						.iterator();
			}

			@Override
			public boolean tryLastParameter(
						Iterator<Class<?>> params)
			{
				params.next();
				return (!params.hasNext());
			}
		}

		Objects.requireNonNull(method, "method");
		final MethodParameterAssignable assignable =
							(method.isVarArgs())
			? new VAMPA(method)
			: new FAMPA(method);
		return new MethodParameter(
			parameterMatcher()
				.apply(parameterCollator()
					.apply((method.isVarArgs())
						? Step.TRUE
						: Step.FALSE)
					.apply(assignable))
				.apply(assignable),
			assignable.ineligible(),
			assignable.withStatus());
	}

	/** A skeletal implementation of {@code MethodParameterAssignable}. */
	private static abstract non-sealed class AMPA implements
						MethodParameterAssignable
	{
		/**
		 * A list of formal parameter types of a queried method, whose
		 * first parameter type, iff declared as {@link Status}, shall
		 * be omitted and whose all primitive parameter types, if any,
		 * shall be represented as boxed parameter types.
		 * <p>
		 * For proper type comparison, the elements of this list
		 * should be accessed indirectly, via a copy produced by
		 * an {@link #iterator(int) iterator}.
		 */
		final List<Class<?>> parameters;

		/**
		 * An ordinal position of a {@link Status} parameter, if any,
		 * of a queried method; either {@code 0} or {@code 1}.
		 */
		final short statusPlace;

		/**
		 * Constructs a new {@code AMPA} object.
		 *
		 * @param method a queried method
		 */
		AMPA(Method method)
		{
			final Class<?>[] params = method.getParameterTypes();
			statusPlace = (short) ((params.length > 0
					&& Status.class.isAssignableFrom(
								params[0]))
				? 1
				: 0);
			parameters = (params.length > 0)
				? Arrays.stream(params)
					.skip(statusPlace)
					.map(AMPA::boxParameter)
					.collect(Collectors
							.toUnmodifiableList())
				: List.of();
		}

		@Override
		public final boolean ineligible() { return parameters.isEmpty(); }

		@Override
		public final boolean withStatus() { return (statusPlace == 1); }

		/**
		 * Converts the passed primitive type of a formal parameter of
		 * a method to its corresponding reference type, otherwise
		 * returns the passed type.
		 *
		 * @param type a formal parameter type of a method
		 * @return the corresponding reference type converted from
		 *	the passed primitive type of a formal parameter of
		 *	a method, otherwise the passed type
		 */
		static Class<?> boxParameter(Class<?> type)
		{
			return (type.isPrimitive())
				? BOXED_PRIMITIVES.get(type)
				: type;
		}
	}

	/**
	 * This interface exposes methods for verification of the assignment
	 * compatibility of pairs of formal parameter types of a method and
	 * its reference-type arguments.
	 */
	private sealed interface MethodParameterAssignable
	{
		/**
		 * The total length of method parameters.
		 * <p>
		 * See arity limits for
		 * {@link java.lang.invoke.MethodHandle method handles}.
		 *
		 * @jvms 4.11 Limitations of the Java Virtual Machine
		 */
		static final short TOTAL_LENGTH = 255;

		/**
		 * Returns whether a queried method declares no formal
		 * parameters or whether the only formal parameter is of type
		 * {@link Status}.
		 *
		 * @return whether a queried method declares no formal
		 *	parameters or whether the only formal parameter is
		 *	of type {@code Status}
		 */
		boolean ineligible();

		/**
		 * Returns whether the passed method argument is assignable to
		 * the passed formal parameter type.
		 *
		 * @param parameter a formal parameter type of a method
		 * @param argument a method argument
		 * @param isLastPair whether <i>both</i> the passed formal
		 *	parameter type is the last formal parameter type of
		 *	a method and the passed method argument is the last
		 *	available one
		 * @return whether the passed method argument is assignable to
		 *	the passed formal parameter type
		 * @see #iterator(int)
		 */
		boolean is(Class<?> parameter, Object argument,
							boolean isLastPair);

		/**
		 * Returns an iterator over the list of formal parameter types
		 * of a queried method.
		 *
		 * @param cardinality the number of available method arguments
		 * @return an iterator over the list of formal parameter types
		 *	of a queried method
		 * @throws IllegalStateException if this instance is ineligible
		 * @see #ineligible()
		 */
		Iterator<Class<?>> iterator(int cardinality);

		/**
		 * Returns whether an only element can be obtained from
		 * the passed iterator.
		 *
		 * @param params an iterator over the list of formal parameter
		 *	types of a queried method
		 * @return whether an only element can be obtained from
		 *	the passed iterator
		 * @see #iterator(int)
		 */
		boolean tryLastParameter(Iterator<Class<?>> params);

		/**
		 * Returns whether the first formal parameter type, if any,
		 * of a queried method is {@link Status}.
		 *
		 * @return whether the first formal parameter type, if any,
		 *	of a queried method is {@code Status}
		 */
		boolean withStatus();
	}

	/** This enumeration specifies abstract iteration steps. */
	private enum Step
	{
		/** The next step. */
		NEXT(null),

		/** The false step. */
		FALSE(Boolean.FALSE),

		/** The true step. */
		TRUE(Boolean.TRUE);

		private final Optional<Boolean> value;

		private Step(Boolean value)
		{
			this.value = Optional.ofNullable(value);
		}

		/**
		 * Returns whether this instance is {@link NEXT}.
		 *
		 * @return whether this instance is {@code NEXT}
		 */
		boolean next()			{ return value.isEmpty(); }

		/**
		 * Returns the empty optional for {@link NEXT}, otherwise
		 * the optional with this step value.
		 *
		 * @return the empty optional for {@code NEXT}, otherwise
		 *	the optional with this step value
		 */
		Optional<Boolean> value()	{ return value; }
	}
}
