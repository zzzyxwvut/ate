package org.zzzyxwvut.ate.internal;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader.Provider;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Result.MethodKind;
import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.Testable;
import org.zzzyxwvut.ate.configuration.Configurable.Prefixable;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.configuration.DefaultPrefix;
import org.zzzyxwvut.ate.configuration.Instantiable;
import org.zzzyxwvut.ate.internal.MethodKindFriend.MethodKindable;
import org.zzzyxwvut.ate.internal.adt.ReadOnlyMap;
import org.zzzyxwvut.ate.internal.adt.Trampoline;
import org.zzzyxwvut.ate.support.AccessibleUplooker;

/** This class encompasses a test class context. */
final class TestClassContext
{
	/**
	 * A comparator of types that arranges their left-to-right order
	 * by observing their parent-to-child relation and treats unrelated
	 * types as ordered.
	 */
	static final Comparator<? super Class<?>> TYPE_COMPARATOR =
							(left, right) ->
		(left == right)		/* Honour identity conversion. */
			? 0
			: (left.isAssignableFrom(right))
				? -1	/* "Begin with the beginning." */
////				: (right.isAssignableFrom(left))
////					? 1
					: 1;	/* Honour _equals_. */

	/**
	 * A comparator of methods that arranges their left-to-right order
	 * according to their declaration origin.
	 */
	static final Comparator<? super Method> METHOD_COMPARATOR =
							(left, right) ->
			TYPE_COMPARATOR.compare(left.getDeclaringClass(),
						right.getDeclaringClass());

	private static final String INSTANTIABLE_METHOD_NAME = "instantiable";
	private static final String ROOT_PACKAGE_NAME = Testable.class
		.getPackageName();
	private static final Function<Class<? extends Testable>,
				Function<List<Pattern>,
					Map<MethodKind, List<Set<Method>>>>>
							METHODS_SELECTOR;

	static {
		try {
			METHODS_SELECTOR = methodsSelector((new Testable() {
					private void uoe()
					{
						throw new UnsupportedOperationException();
					}
				})
				.getClass()
				.getDeclaredMethod("uoe"));
		} catch (final NoSuchMethodException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	private final boolean continueOnFailure;
	private final ExecutionPolicy executionPolicy;
	private final Set<? extends Prefixable> prefixables;
	private final Consumer<Result> reporter;
	private final Consumer<Throwable> errorPrinter;
	private final GroupingInvoker invoker;

	/**
	 * Constructs a new {@code TestClassContext} object.
	 *
	 * @param testClass a test class
	 * @param configurable an optional with configurable testing
	 *	attributes
	 * @param options a set of predicative options
	 * @param filter a class-and-method name filter
	 * @param unassignableTemplet a warning templet for obtained
	 *	unassignable method arguments
	 * @throws AssertionError if verifying whether assertion for a test
	 *	class is enabled is opted for and assertion is disabled
	 * @throws IllegalStateException if any of access checks fail or no
	 *	instance of a test class is obtainable or a method handle for
	 *	a life cycle method cannot be made or required argument(s)
	 *	cannot be obtained
	 */
	TestClassContext(Class<? extends Testable> testClass,
					Optional<Configurable> configurable,
					Set<Option> options,
					ClassMethodNameFilter filter,
					String unassignableTemplet)
	{
		Objects.requireNonNull(testClass, "testClass");
		Objects.requireNonNull(configurable, "configurable");
		Objects.requireNonNull(options, "options");
		Objects.requireNonNull(filter, "filter");
		Objects.requireNonNull(unassignableTemplet,
						"unassignableTemplet");

		if (options.contains(Option.VERIFY_ASSERTION)
				&& !testClass.desiredAssertionStatus())
			throw new AssertionError(testClass.getName()
				.concat(": assertion status is false"));

		final Instantiable<? extends Testable> instantiable;
		final boolean nonBaseInstantiable;
		final Lookup lookup;

		try {
			lookup = MethodHandlesLookup.createLookup(testClass);
			final NonBaseInstantiableInstantiable pair =
							instantiable(
								lookup,
								testClass);
			nonBaseInstantiable = pair.nonBaseInstantiable();
			instantiable = pair.instantiable();
		} catch (final IllegalAccessException e) {
			throw new IllegalStateException("Illegal access", e);
		}

		final Set<Supplier<? extends Testable>> testables = Objects
			.requireNonNull(instantiable.testables(), "testables")
			.stream()
			.filter(Objects::nonNull)
			.collect(Support.toUnmodifiableOrderedSet());

		if (testables.isEmpty())
			throw new IllegalStateException(
				"No non-null test class instances found");

		continueOnFailure = options.contains(
						Option.CONTINUE_ON_FAILURE);
		final boolean plainOutput = options.contains(
						Option.RENDER_OUTPUT_PLAIN);
		final Configurable baseConfigurable = (plainOutput)
			? ConfigurableMerger.BASE_CONFIGURABLE_PLAIN
			: ConfigurableMerger.BASE_CONFIGURABLE_MULTI;
		final Configurable mergedConfigurable = (nonBaseInstantiable)
			? new ConfigurableMerger(plainOutput)
				.merge(instantiable,
					configurable.orElse(baseConfigurable))
			: new ConfigurableMerger(plainOutput)
				.merge(configurable.orElse(baseConfigurable),
					baseConfigurable);
		executionPolicy = mergedConfigurable.executionPolicy();
		prefixables = mergedConfigurable.prefixables()
			.stream()
			.filter(prefixable -> Stream.of(Stream.of(
					prefixable.testMethodNamePrefix()),
					prefixable.lifeCycleMethodNamePrefixes()
							.stream())
				.flatMap(Function.identity())
				.allMatch(Predicate.<String>not(Objects::isNull)
					.and(Predicate.not(String::isBlank))))
			.collect(Support.toUnmodifiableOrderedSet());
		reporter = mergedConfigurable.reportable()
			.apply(mergedConfigurable.printStream())
			.apply(testClass);
		errorPrinter = (options.contains(
					Option.SUPPRESS_THROWABLE_PRINTING))
			? error -> { }
			: Support.<PrintStream, Consumer<Throwable>>
								partialApply(
				stream -> error -> error
					.printStackTrace(stream),
				mergedConfigurable.printStream());
		invoker = new GroupingInvoker(
			mergedConfigurable.printStream(),
			testables,
			new ReadOnlyMap<>(Map.copyOf(instantiable.arguments()),
								Set::of),
			new ReadOnlyMap<>(selectMethods(testClass, filter),
								List::of),
			unassignableWarner(mergedConfigurable.printStream(),
				options.contains(
					Option.SUPPRESS_UNASSIGNABILITY),
				unassignableTemplet),
			Support.<Lookup, Function<Method, MethodHandle>>
								partialApply(
				lookup_ -> method -> {
					try {
						return lookup_.unreflect(
								method);
					} catch (final IllegalAccessException e) {
						throw new IllegalStateException(
							"Failed handle making",
							e);
					}
				},
				lookup),
			(executionPolicy == ExecutionPolicy.SEQUENTIAL));
	}

	private static Function<MethodHandle, Supplier<Testable>>
							testableSupplier()
	{
		return factory -> () -> {
			try {
				return (Testable) factory.invoke();
			} catch (final Throwable th) {
				throw new IllegalStateException(
					"Failed test object creation", th);
			}
		};
	}

	private static Instantiable<? extends Testable> instantiableFactory(
							MethodHandle factory)
	{
		try {
			return Objects.requireNonNull(
				(Instantiable<?>) factory.invokeExact(),
				INSTANTIABLE_METHOD_NAME);
		} catch (final Throwable th) {
			throw new IllegalStateException(new StringBuilder(32)
					.append("Failed \"")
					.append(INSTANTIABLE_METHOD_NAME)
					.append("\" invocation")
					.toString(),
				th);
		}
	}

	private static NonBaseInstantiableInstantiable instantiable(
					Lookup lookup,
					Class<? extends Testable> testClass)
						throws IllegalAccessException
	{
		try {
			try {
				return new NonBaseInstantiableInstantiable(
							true,
							instantiableFactory(
					lookup.findStatic(
						testClass,
						INSTANTIABLE_METHOD_NAME,
						MethodType.methodType(
							Instantiable.class))));
			} catch (final NoSuchMethodException ignored) {
				return new NonBaseInstantiableInstantiable(
							false,
							Instantiable
					.newBuilder(Set.of(testableSupplier()
						.apply(lookup.findConstructor(
							testClass,
							MethodType.methodType(
								void.class)))))
					.build());
			}
		} catch (final NoSuchMethodException ignored) {
			throw new IllegalStateException(new StringBuilder(176)
				.append(testClass.getName())
				.append(": neither parameterless \"")
				.append(INSTANTIABLE_METHOD_NAME)
				.append("\" method nor parameterless constructor found")
				.toString());
		}
	}

	private static Stream<Method> traversePublicMethods(
					Set<? extends String> nameRejects,
					Class<? extends Testable> testClass)
	{
		return Arrays
			.stream(testClass.getMethods())
			.filter(Predicate.<Method>not(method ->
							Modifier.isStatic(
						method.getModifiers()))
				.and(Support.<Set<? extends String>,
							Predicate<Method>>
								partialApply(
					rejects -> method -> !rejects.contains(
							method.getName()),
					nameRejects)))
			.sorted(METHOD_COMPARATOR);
	}

	private static Stream<Method> traversePublicStaticMethods(
					Set<? super String> nameRejects,
					Class<?> klass)
	{
		return Arrays
			.stream(klass.getDeclaredMethods())
			.filter((klass == Object.class
				|| (klass.getPackageName()
						.startsWith(ROOT_PACKAGE_NAME)

					/*
					 * Bypass the bootstrapped tests. Note
					 * that this implies that fleshed out
					 * ROOT_PACKAGE_NAME hierarchies of
					 * test types claim assignability to
					 * org.zzzyxwvut.ate.Testable for
					 * every ascending, contributing type
					 * (life cycle methods, test methods).
					 */
					&& !Testable.class.isAssignableFrom(
								klass)))

				/*
				 * _Unconditionally_ reject method members of
				 * the primordial class java.lang.Object or of
				 * a ROOT_PACKAGE_NAME class (it is a poor
				 * choice to conflate its methods with test
				 * methods proper); also, collect the names of
				 * non-STATIC, PUBLIC or PROTECTED, methods to
				 * expedite a future dismissal of methods from
				 * the derived ROOT_PACKAGE_NAME classes.
				 */
				? Predicate.<Method>not(method ->
							Modifier.isStatic(
						method.getModifiers()))
					.and(method -> (method.getModifiers()
							& (Modifier.PUBLIC
						| Modifier.PROTECTED)) != 0)
					.and(Support.<Set<? super String>,
							Predicate<Method>>
								partialApply(
						rejects -> method -> {
							rejects.add(method
								.getName());
							return false;
						},
						nameRejects))

				/* Reject non-PUBLIC-STATIC methods. */
				: method -> (method.getModifiers()
					& (Modifier.PUBLIC | Modifier.STATIC))
					== (Modifier.PUBLIC | Modifier.STATIC));
	}

	private static Function<Set<? super String>,
				Function<Class<?>, Stream<Method>>>
						publicStaticMethodsTraverser()
	{
		return nameRejects -> klass -> Stream
			.of(new InterfaceHierarchyWalker(klass, TYPE_COMPARATOR)
				.types()
				.stream()
				.flatMap(Support.<Set<? super String>,
						Function<Class<?>,
							Stream<Method>>>
								partialApply(
					rejects -> klass_ ->
						traversePublicStaticMethods(
								rejects,
								klass_),
					nameRejects)),
				traversePublicStaticMethods(nameRejects,
								klass))
			.flatMap(Function.identity());
	}

	private static Function<Class<? extends Testable>,
				Function<List<Pattern>,
					Map<MethodKind, List<Set<Method>>>>>
						methodsSelector(Method sentinel)
	{
		abstract class DS<E>
		{
			private final LinkedList<Set<E>> descendants =
							new LinkedList<>();
			private final LinkedHashSet<E> siblings =
							new LinkedHashSet<>();
			private final Comparator<? super E> comparator;

			private E relation;

			DS(Comparator<? super E> comparator, E sentinel)
			{
				this.comparator = comparator;
				this.relation = sentinel;
			}

			abstract void claim(Deque<Set<E>> descendants,
							Set<E> siblings);
			abstract void dismissSentinel(
						Deque<Set<E>> descendants);

			@SuppressWarnings("unchecked")
			private void doClaimDescendant()
			{
				claim(descendants,
					Collections.unmodifiableSet(
						(Set<E>) siblings.clone()));
				siblings.clear();
			}

			private void doClaimSibling(E relative)
			{
				siblings.add(relative);
				relation = relative;
			}

			private DS<E> claimDescendant(E relative)
			{
				doClaimDescendant();
				doClaimSibling(relative);
				return this;
			}

			private DS<E> claimSibling(E relative)
			{
				doClaimSibling(relative);
				return this;
			}

			final DS<E> connect(E relative)
			{
				return (comparator.compare(relation, relative)
									!= 0)
					? claimDescendant(relative)
					: claimSibling(relative);
			}

			@SuppressWarnings("unchecked")
			final List<Set<E>> toList()
			{
				final LinkedList<Set<E>> race =
						(LinkedList<Set<E>>)
							descendants.clone();
				claim(race,
					Collections.unmodifiableSet(
						(Set<E>) siblings.clone()));
				dismissSentinel(race);
				return Collections.unmodifiableList(race);
			}
		}

		class DDS<E> extends DS<E>
		{
			DDS(Comparator<? super E> comparator, E sentinel)
			{
				super(comparator, sentinel);
			}

			@Override
			void claim(Deque<Set<E>> descendants, Set<E> siblings)
			{
				descendants.offerLast(siblings);
			}

			@Override
			void dismissSentinel(Deque<Set<E>> descendants)
			{
				descendants.pollFirst();
			}
		}

		class ADS<E> extends DS<E>
		{
			ADS(Comparator<? super E> comparator, E sentinel)
			{
				super(comparator, sentinel);
			}

			@Override
			void claim(Deque<Set<E>> descendants, Set<E> siblings)
			{
				descendants.offerFirst(siblings);
			}

			@Override
			void dismissSentinel(Deque<Set<E>> descendants)
			{
				descendants.pollLast();
			}
		}

		record MethodKindMethod(MethodKind methodKind,
							Method method) { }

		class MethodCollector
		{
			private final Map<MethodKind, DS<Method>> methods =
					new EnumMap<>(MethodKind.class);
			private final Function<MethodKind, DS<Method>> mapper;

			MethodCollector(Function<MethodKind, DS<Method>> mapper)
			{
				this.mapper = mapper;
			}

			void collect(MethodKindMethod pair)
			{
				/*
				 * Assume an origin-presorted-method source of
				 * pairs.
				 */
				methods.computeIfAbsent(pair.methodKind(),
								mapper)
					.connect(pair.method());
			}

			Map<MethodKind, List<Set<Method>>> toMap()
			{
				return methods
					.entrySet()
					.stream()
					.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> entry
							.getValue()
							.toList(),
						Support.uoeThrower(),
						() -> new EnumMap<>(
							MethodKind.class)));
			}
		}

		return Support.<Supplier<MethodCollector>,
					Function<Class<? extends Testable>,
					Function<List<Pattern>,
						Map<MethodKind, List<Set<Method>>>>>>
								partialApply(
			collectorSupplier -> testClass -> requests -> {
				final Set<String> nameRejects = new HashSet<>();
				return Stream.of(
				/*
				 * Produce a hierarchy of class methods (with
				 * hidden and interface-implemented ones).
				 */
						Stream.<Class<?>>iterate(
							testClass,
							Objects::nonNull,
							Class::getSuperclass)
					.sorted(TYPE_COMPARATOR) /* Reverse it. */
					.flatMap(publicStaticMethodsTraverser()
						.apply(nameRejects)),

				/* Produce a hierarchy of instance methods. */
						traversePublicMethods(
							Collections
								.unmodifiableSet(
									nameRejects),
							testClass))
				.flatMap(Support.<MethodModifierNameFilter,
						Function<Stream<Method>,
							Stream<MethodKindMethod>>>
								partialApply(
					filter -> methods -> methods
						.map(Support.<MethodModifierNameFilter,
							Function<Method,
								MethodKindMethod>>
								partialApply(
							filter_ -> method ->
							new MethodKindMethod(
								filter_.classify(
									method),
								method),
							filter)),
					new MethodModifierNameFilter(requests)))
				.filter(pair -> pair.methodKind()
							!= MethodKind.OTHER)
				.collect(Collector.of(
						collectorSupplier,
						MethodCollector::collect,
						Support.uoeThrower(),
						MethodCollector::toMap));
			},
			Support.<Function<MethodKind, DS<Method>>,
						Supplier<MethodCollector>>
								partialApply(
				mapper -> () -> new MethodCollector(mapper),
				Support.<MethodKindable,
						Function<Method,
						Function<MethodKind, DS<Method>>>>
								partialApply(
					methodKindable -> sentinel_ -> kind ->
								(methodKindable
							.inAscendingOrder(
									kind))
						? new ADS<>(METHOD_COMPARATOR,
								sentinel_)
						: new DDS<>(METHOD_COMPARATOR,
								sentinel_),
					MethodKindFriend.getMethodKindable())
						.apply(sentinel)));
	}

	private static Map<MethodKind, List<Set<Method>>> selectMethods(
					Class<? extends Testable> testClass,
					ClassMethodNameFilter filter)
	{
		return Collections.unmodifiableMap(filter
			.requestedMethods(testClass)
			.map(METHODS_SELECTOR
				.apply(testClass))
			.orElseGet(Map::of));
	}

	private static Function<Method, Consumer<List<?>>> unassignableWarner(
					PrintStream printStream,
					boolean suppressedUnassignability,
					String unassignableTemplet)
	{
		return Support.<Consumer<Supplier<String>>,
						Function<Function<Method,
							Function<List<?>,
							Supplier<String>>>,
						Function<Method,
						Consumer<List<?>>>>>
								partialApply(
			messager -> misassigner -> method -> arguments ->
								messager
				.accept(misassigner
					.apply(method)
					.apply(arguments)),
			(suppressedUnassignability)
				? message -> { }
				: Support.<PrintStream,
						Consumer<Supplier<String>>>
								partialApply(
					stream -> message ->
						stream.format(
							message.get()),
					printStream))
			.apply(Support.<String, Function<Method,
						Function<List<?>,
						Supplier<String>>>>
								partialApply(
				templet -> method -> arguments -> () ->
								String.format(
					templet,
					Support.signature(method),
					Objects.requireNonNullElseGet(
							arguments,
							() -> List.of(
								"<null>"))
						.stream()
						.map(Support::asString)
						.collect(Collectors
							.joining(","))),
				unassignableTemplet));
	}

	/**
	 * Returns whether continuing execution of remaining tests of a class
	 * after failure is opted for.
	 *
	 * @return whether to continue execution after failure
	 */
	boolean continueOnFailure()		{ return continueOnFailure; }

	/**
	 * Returns the execution policy for groups of methods.
	 *
	 * @return the execution policy for groups of methods
	 */
	ExecutionPolicy executionPolicy()	{ return executionPolicy; }

	/**
	 * Returns the set of prefixes for method names to match.
	 *
	 * @return the set of prefixes for method names to match
	 */
	Set<? extends Prefixable> prefixables() { return prefixables; }

	/**
	 * Returns a functional interface that takes some result and does not
	 * return a value.
	 *
	 * @return a curried function
	 * @see org.zzzyxwvut.ate.Result.Reportable
	 */
	Consumer<Result> reporter()		{ return reporter; }

	/**
	 * Returns a functional interface that takes an error and does not
	 * return a value.
	 *
	 * @return a curried function
	 */
	Consumer<Throwable> errorPrinter()	{ return errorPrinter; }

	/**
	 * Returns the categorised sets of {@link MHAA} and {@link Method}
	 * instances.
	 *
	 * @return the categorised sets of {@code MHAA} and {@code Method}
	 *	instances
	 */
	GroupingInvoker invoker()		{ return invoker; }

	/**
	 * This class exposes lookup objects for creating method handles.
	 *
	 * @see java.lang.invoke.MethodHandles.Lookup
	 */
	static final class MethodHandlesLookup
	{
		private static final Supplier<Function<Class<?>, Lookup>>
							PUBLIC_UPLOOKER = () ->
							contextUplooker()
			.apply(MethodHandles.publicLookup());
		private static final Map<Module, Function<Class<?>, Lookup>>
								CACHE =
						new ConcurrentHashMap<>(8);

		private MethodHandlesLookup() { /* No instantiation. */ }

		private static Function<Lookup, Function<Class<?>, Lookup>>
							contextUplooker()
		{
			return lookup -> lookup::in;
		}

		private static Function<Lookup, Function<Class<?>, Lookup>>
							privateUplooker()
		{
			return lookup -> klass -> {
				try {
					return MethodHandles.privateLookupIn(
								klass,
								lookup);
				} catch (final IllegalAccessException e) {
					throw new UncheckedIAE(e);
				}
			};
		}

		private static boolean privateModuleAccess(Lookup lookup)
		{
			return ((lookup.lookupModes()
					& (Lookup.PRIVATE | Lookup.MODULE))
					== (Lookup.PRIVATE | Lookup.MODULE));
		}

		private static Lookup doCreateLookup(Class<?> klass)
		{
			return CACHE.computeIfAbsent(
						klass.getModule(),
						module -> ServiceLoader
				.load(Optional.ofNullable(module.getLayer())
						.orElseGet(
							ModuleLayer::empty),
					AccessibleUplooker.class)
				.stream()
				.filter(Support.<String,
						Predicate<Provider<AccessibleUplooker>>>
								partialApply(
					moduleName -> provider -> moduleName
						.equals(provider
							.type()
							.getModule()
							.getName()),	/* !null */
					Objects.requireNonNullElse(
						module.getName(),
						"")))			/* null? */
				.findAny()
				.flatMap(provider -> Optional
					.ofNullable(provider
						.get()
						.exportableLookup())
					.map(lookup ->
						((privateModuleAccess(lookup))
							? privateUplooker()
							: contextUplooker())
						.apply(lookup)))
				.orElseGet(PUBLIC_UPLOOKER))
				.apply(klass);
		}

		/**
		 * Creates a lookup object on the passed class. If there is
		 * an {@link org.zzzyxwvut.ate.support.AccessibleUplooker}
		 * service provider defined in the passed class module, its
		 * obtained instance shall be used for access checking during
		 * each method handle creation of the class; {@code Lookup.PUBLIC}
		 * access is otherwise admitted, subject to further reduction
		 * with {@link java.lang.invoke.MethodHandles.Lookup#in(Class)}.
		 *
		 * @param klass a class
		 * @return a lookup object
		 * @throws IllegalAccessException if an access check fails
		 */
		static Lookup createLookup(Class<?> klass) throws
							IllegalAccessException
		{
			Objects.requireNonNull(klass, "klass");

			try {
				return doCreateLookup(klass);
			} catch (final UncheckedIAE e) {
				throw e.getCause();
			}
		}
	}

	/**
	 * Instances of this class wrap an {@code IllegalAccessException}
	 * with an unchecked exception.
	 */
	static final class UncheckedIAE extends RuntimeException
	{
		private static final long serialVersionUID = 1L;

		/**
		 * Constructs a new {@code UncheckedIAE} object.
		 *
		 * @param cause an {@code IllegalAccessException}
		 */
		UncheckedIAE(IllegalAccessException cause)
		{
			super(Objects.requireNonNull(cause, "cause"));
		}

		@Override
		public IllegalAccessException getCause()
		{
			return (IllegalAccessException) super.getCause();
		}

		private void readObject(ObjectInputStream stream) throws
							IOException,
							ClassNotFoundException
		{
			stream.defaultReadObject();

			if (!(super.getCause() instanceof
						IllegalAccessException))
				throw new InvalidObjectException(
					"Not an IllegalAccessException");
		}
	}

	/**
	 * This class serves for merging of configurable testing attributes.
	 */
	static final class ConfigurableMerger
	{
		/**
		 * Default configurable testing attributes with multicoloured
		 * output stream rendition.
		 *
		 * @see org.zzzyxwvut.ate.configuration
		 */
		static final Configurable BASE_CONFIGURABLE_MULTI = Configurable
			.newBuilder()
			.executionPolicy(ExecutionPolicy.SEQUENTIAL)
			.prefixables(Set.of(new DefaultPrefix("test")))
			.printStream(System.err)
			.reportable(Result.resultor()
				.apply(Result.renderer()
					.apply(Map.of(OtherResult.class,
						OtherResult.RENDITION_REVERSE_VIDEO,
						TestResult.class,
						TestResult.RENDITION_RED_YELLOW_GREEN))))
			.build();

		/**
		 * Default configurable testing attributes with plain output
		 * stream rendition.
		 *
		 * @see org.zzzyxwvut.ate.configuration
		 */
		static final Configurable BASE_CONFIGURABLE_PLAIN = Configurable
			.newBuilder()
			.executionPolicy(ExecutionPolicy.SEQUENTIAL)
			.prefixables(Set.of(new DefaultPrefix("test")))
			.printStream(System.err)
			.reportable(Result.resultor()
				.apply(Result.renderer()
					.apply(Map.of(OtherResult.class,
						OtherResult.RENDITION_PLAIN,
						TestResult.class,
						TestResult.RENDITION_PLAIN))))
			.build();

		private static final Function<Configurable,
					Supplier<ExecutionPolicy>>
						EXECUTION_POLICY_SUPPLIER =
							ConfigurableMerger
			.<ExecutionPolicy>accessorSupplier()
			.apply(Configurable::executionPolicy);
		private static final Function<Configurable,
					Supplier<Set<? extends Prefixable>>>
						PREFIXABLES_SUPPLIER =
							ConfigurableMerger
			.<Set<? extends Prefixable>>accessorSupplier()
			.apply(Configurable::prefixables);
		private static final Function<Configurable,
					Supplier<PrintStream>>
						PRINT_STREAM_SUPPLIER =
							ConfigurableMerger
			.<PrintStream>accessorSupplier()
			.apply(Configurable::printStream);
		private static final Function<Configurable,
					Supplier<Reportable>>
						REPORTABLE_SUPPLIER =
							ConfigurableMerger
			.<Reportable>accessorSupplier()
			.apply(Configurable::reportable);

		private final Configurable baseConfigurable;

		/**
		 * Constructs a new {@code ConfigurableMerger} object.
		 *
		 * @param plainOutput whether to render stream output plain
		 */
		ConfigurableMerger(boolean plainOutput)
		{
			baseConfigurable = (plainOutput)
				? BASE_CONFIGURABLE_PLAIN
				: BASE_CONFIGURABLE_MULTI;
		}

		private static <T> Function<Function<Configurable, T>,
					Function<Configurable,
					Supplier<T>>> accessorSupplier()
		{
			return accessor -> configurable -> () -> accessor
				.apply(configurable);
		}

		private <T> T findFirstNonNullValue(
			Function<Configurable, Supplier<T>> accessorSupplier,
			Function<Configurable, T> accessor,
			List<Configurable> configurables)
		{
			return configurables
				.stream()
				.map(accessor)
				.filter(Objects::nonNull)
				.findFirst()
				.orElseGet(accessorSupplier
					.apply(baseConfigurable));
		}

		/**
		 * Returns merged configurable testing attributes.
		 *
		 * @param primaryConfigurable primary configurable testing
		 *	attributes
		 * @param secondaryConfigurable secondary configurable testing
		 *	attributes
		 * @return merged configurable testing attributes
		 */
		Configurable merge(Configurable primaryConfigurable,
					Configurable secondaryConfigurable)
		{
			Objects.requireNonNull(primaryConfigurable,
						"primaryConfigurable");
			Objects.requireNonNull(secondaryConfigurable,
						"secondaryConfigurable");
			final List<Configurable> configurables = List.of(
							primaryConfigurable,
							secondaryConfigurable);
			return Configurable
				.newBuilder()
				.executionPolicy(findFirstNonNullValue(
						EXECUTION_POLICY_SUPPLIER,
						Configurable::executionPolicy,
						configurables))
				.prefixables(findFirstNonNullValue(
						PREFIXABLES_SUPPLIER,
						Configurable::prefixables,
						configurables))
				.printStream(findFirstNonNullValue(
						PRINT_STREAM_SUPPLIER,
						Configurable::printStream,
						configurables))
				.reportable(findFirstNonNullValue(
						REPORTABLE_SUPPLIER,
						Configurable::reportable,
						configurables))
				.build();
		}
	}

	/** An interface hierarchy walker. */
	static final class InterfaceHierarchyWalker
	{
		private final TreeSet<Class<?>> types;

		/**
		 * Constructs a new {@code InterfaceHierarchyWalker} object.
		 *
		 * @param klass a class
		 * @param comparator a comparator
		 */
		InterfaceHierarchyWalker(Class<?> klass,
				Comparator<? super Class<?>> comparator)
		{
			Objects.requireNonNull(klass, "klass");
			Objects.requireNonNull(comparator, "comparator");
			Trampoline.mount(walk(types = new TreeSet<>(comparator),
					new LinkedList<>(List.of(
						klass.getInterfaces()))));
		}

		private static Trampoline<Set<Class<?>>> walk(
						Set<Class<?>> types,
						List<Class<?>> candidates)
		{
			class IH implements Supplier<Trampoline<Set<Class<?>>>>
			{
				private final Set<Class<?>> types;
				private final List<Class<?>> candidates;

				IH(Set<Class<?>> types,
						List<Class<?>> candidates)
				{
					this.types = types;
					this.candidates = candidates;
				}

				@Override
				public Trampoline<Set<Class<?>>> get()
				{
					final Class<?> nextType = candidates
								.remove(0);
					candidates.addAll(0,
							(types.add(nextType))
						? List.of(nextType
							.getInterfaces())
						: List.of());
					return InterfaceHierarchyWalker.walk(
								types,
								candidates);
				}
			}

			return (candidates.isEmpty())
				? Trampoline.land(types)
				: Trampoline.bounce(new IH(types, candidates));
		}

		/**
		 * Returns a copy of the hierarchy of implemented interfaces.
		 *
		 * @return a copy of the hierarchy of implemented interfaces
		 */
		@SuppressWarnings("unchecked")
		NavigableSet<Class<?>> types()
		{
			return (NavigableSet<Class<?>>) types.clone();
		}
	}

	/** This class applies modifier and name filters to methods. */
	static final class MethodModifierNameFilter
	{
		private static final int UNSUPPORTED = Modifier.methodModifiers()
					& ~(Modifier.PUBLIC | Modifier.FINAL
				| Modifier.SYNCHRONIZED | Modifier.STRICT);
		private static final MethodKindable METHOD_KINDABLE =
					MethodKindFriend.getMethodKindable();

		private final List<Pattern> requests;

		/**
		 * Constructs a new {@code MethodModifierNameFilter} object.
		 *
		 * @param requests a list of method patterns to match
		 * @see ClassMethodNameFilter#requestedMethods(Class)
		 */
		MethodModifierNameFilter(List<Pattern> requests)
		{
			this.requests = Objects.requireNonNull(requests,
								"requests");
		}

		private static boolean isSupportedPublic(Method method)
		{
			final int mm = method.getModifiers();
			return (((mm & UNSUPPORTED) == 0)
					&& ((mm & Modifier.PUBLIC) != 0));
		}	/* Reject package-private-Modifier-less candidates. */

		private static boolean isSupportedPublicStatic(Method method)
		{
			final int mm = method.getModifiers();
			final int ps = Modifier.PUBLIC | Modifier.STATIC;
			return (((mm & (UNSUPPORTED & ~Modifier.STATIC)) == 0)
							&& ((mm & ps) == ps));
		}

		private boolean isRequested(String methodName)
		{
			return (requests.isEmpty() || requests
				.stream()
				.anyMatch(Support.<String, Predicate<Pattern>>
								partialApply(
					methodName_ -> pattern -> pattern
						.matcher(methodName_)
						.lookingAt(),
					methodName)));
		}

		/**
		 * Assigns the passed {@code method} to a supported kind.
		 *
		 * @param method a method
		 * @return a supported kind to which the passed {@code method}
		 *	is assignable to; otherwise {@code MethodKind.OTHER},
		 *	if criteria for categorisation are unmet
		 */
		MethodKind classify(Method method)
		{
			Objects.requireNonNull(method, "method");
			final MethodKind lifeCycleOrOther = METHOD_KINDABLE
				.lifeCycleMethodKindOrOther(method);
			return (isSupportedPublicStatic(method))
				? switch (lifeCycleOrOther) {
				case SET_UP_ALL, SET_UP_BATCH,
					TEAR_DOWN_ALL, TEAR_DOWN_BATCH ->
							lifeCycleOrOther;
				case OTHER, TEST,
					SET_UP, TEAR_DOWN ->
							MethodKind.OTHER;
				}
				: (isSupportedPublic(method))
					? switch (lifeCycleOrOther) {
					case SET_UP_ALL, SET_UP_BATCH,
						TEAR_DOWN_ALL, TEAR_DOWN_BATCH ->
							MethodKind.OTHER;
					case SET_UP, TEAR_DOWN ->
							lifeCycleOrOther;
					case OTHER, TEST ->
						isRequested(method.getName())
							? MethodKind.TEST
							: MethodKind.OTHER;
					}
					: MethodKind.OTHER;
		}
	}

	private record NonBaseInstantiableInstantiable(
			boolean nonBaseInstantiable,
			Instantiable<? extends Testable> instantiable) { }
}
