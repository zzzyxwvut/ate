package org.zzzyxwvut.ate.internal;

import java.io.PrintStream;
import java.lang.invoke.MethodHandle;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.zzzyxwvut.ate.Status;
import org.zzzyxwvut.ate.Testable;

/**
 * This class serves for aggregation of a method handle and its arguments,
 * if any.
 */
abstract sealed class MHAA
{
	/** The name of a method. */
	/* Consider filtering &c. */
	final String methodName;

	private final MethodHandle methodHandle;
	private final Optional<PrintStream> statusStream;

	/**
	 * Constructs a new {@code MHAA} object.
	 *
	 * @param methodName the name of a method
	 * @param methodHandle a method handle
	 * @param statusStream an optional with the output stream used
	 *	for testing, iff the first method parameter, if any, is
	 *	a {@code Status}; else an empty optional
	 */
	MHAA(String methodName, MethodHandle methodHandle,
					Optional<PrintStream> statusStream)
	{
		this.methodName = Objects.requireNonNull(methodName,
							"methodName");
		this.methodHandle = Objects.requireNonNull(methodHandle,
							"methodHandle");
		this.statusStream = Objects.requireNonNull(statusStream,
							"statusStream");
	}

	/**
	 * Invokes the passed method handle.
	 *
	 * @param someMethodHandle a method handle
	 * @throws Throwable if anything is thrown by the passed method handle
	 */
	abstract void invoke(MethodHandle someMethodHandle) throws Throwable;

	private Optional<Throwable> doInvoke(MethodHandle someMethodHandle,
					Optional<String> testMethodNamePrefix)
	{
		try {
			/*
			 * Note that the passed method handle shall be used
			 * for invocation, whereas the stored method handle--
			 * for the toString implementation.
			 */
			invoke(statusStream
				.map(MHAA.<Status>bind(someMethodHandle)
					.compose(Support.<Optional<String>,
							Function<PrintStream,
									Status>>
								partialApply(
								namePrefix ->
								printStream ->
							new DefaultStatus(
								printStream,
								namePrefix),
						testMethodNamePrefix)))
				.orElse(someMethodHandle));
		} catch (final AssertionError e) {
			return Optional.of(e);
		} catch (final Error e) {
			throw e;
		} catch (final Throwable th) {
			return Optional.of(th);
		}

		return Optional.empty();
	}

	@Override
	public String toString()	{ return methodHandle.toString(); }

	/**
	 * Returns a functional interface that takes some object and returns
	 * a new method handle, after binding the applied object as the first
	 * argument to the passed method handle.
	 *
	 * @param <T> a type
	 * @param methodHandle a method handle
	 * @return a curried function
	 */
	static <T> Function<T, MethodHandle> bind(MethodHandle methodHandle)
	{
		return Support.<MethodHandle, Function<T, MethodHandle>>
								partialApply(
			handle -> handle::bindTo,
			Objects.requireNonNull(methodHandle, "methodHandle"));
	}

	/**
	 * Invokes the {@link #invoke(MethodHandle)} method on the passed
	 * {@code MHAA} instance by passing it either the stored method handle
	 * of the passed {@code MHAA} instance or, on demand, a new method
	 * handle made by binding a new {@code Status} instance to the first
	 * argument of the stored method handle of the passed {@code MHAA}
	 * instance.
	 *
	 * @param mhaa an aggregate of a method handle and its arguments, if
	 *	any
	 * @param testMethodNamePrefix an optional with a test method name
	 *	prefix, else an empty optional
	 * @return an optional with the caught {@code Exception} or
	 *	{@code AssertionError}, if any; otherwise an empty optional
	 */
	static Optional<Throwable> invokeStatic(MHAA mhaa,
					Optional<String> testMethodNamePrefix)
	{
		Objects.requireNonNull(mhaa, "mhaa");
		Objects.requireNonNull(testMethodNamePrefix,
						"testMethodNamePrefix");
		return mhaa.doInvoke(mhaa.methodHandle, testMethodNamePrefix);
	}

	/**
	 * Invokes the {@link #invoke(MethodHandle)} method on the passed
	 * {@code MHAA} instance by passing it either a new method handle made
	 * by binding the passed {@code Testable} argument to the first
	 * argument of the stored method handle of the passed {@code MHAA}
	 * instance or, on demand, a new method handle made by binding a new
	 * {@code Status} instance to the second argument of a method handle
	 * made by binding the passed {@code Testable} argument to the first
	 * argument of the stored method handle of the passed {@code MHAA}
	 * instance.
	 *
	 * @param mhaa an aggregate of a method handle and its arguments, if
	 *	any
	 * @param testable a test class instance
	 * @param testMethodNamePrefix an optional with a test method name
	 *	prefix, else an empty optional
	 * @return an optional with the caught {@code Exception} or
	 *	{@code AssertionError}, if any; otherwise an empty optional
	 */
	static Optional<Throwable> invokeVirtual(MHAA mhaa,
					Testable testable,
					Optional<String> testMethodNamePrefix)
	{
		Objects.requireNonNull(mhaa, "mhaa");
		Objects.requireNonNull(testable, "testable");
		Objects.requireNonNull(testMethodNamePrefix,
						"testMethodNamePrefix");
		return mhaa.doInvoke(
			MHAA.<Testable>bind(mhaa.methodHandle)
				.apply(testable),
			testMethodNamePrefix);
	}

	/**
	 * Creates a new {@code MHAA} object that takes bound arguments only.
	 *
	 * @param methodName the name of a method
	 * @param methodHandle a method handle
	 * @param statusStream an optional with the output stream used
	 *	for testing, iff the first method parameter, if any, is
	 *	a {@code Status}; else an empty optional
	 * @return a new {@code MHAA} object that takes bound arguments only
	 */
	static MHAA newMHAA(String methodName, MethodHandle methodHandle,
					Optional<PrintStream> statusStream)
	{
		return new MHAA2(methodName, methodHandle, statusStream);
	}

	/**
	 * Creates a new {@code MHAA} object that allows for unbound arguments.
	 *
	 * @param methodName the name of a method
	 * @param methodHandle a method handle
	 * @param statusStream an optional with the output stream used
	 *	for testing, iff the first method parameter, if any, is
	 *	a {@code Status}; else an empty optional
	 * @param arguments a list of supported arguments for the passed method
	 *	handle; else {@code null}, when a method handle references
	 *	a method either <i>without</i> formal parameters or <i>with</i>
	 *	formal parameters for which all supported arguments have
	 *	already been bound
	 * @param varArgsType an optional with a variable-arity type, iff
	 *	the passed method handle takes a variable number of arguments;
	 *	else an empty optional
	 * @return a new {@code MHAA} object that allows for unbound arguments
	 */
	static MHAA newMHAA(String methodName, MethodHandle methodHandle,
			Optional<PrintStream> statusStream, List<?> arguments,
			Optional<Class<?>> varArgsType)
	{
		return new MHAAN(methodName, methodHandle, statusStream,
						arguments, varArgsType);
	}

	/** Instances of this {@code MHAA} take bound arguments only. */
	private static final class MHAA2 extends MHAA
	{
		/**
		 * Constructs a new {@code MHAA2} object.
		 *
		 * @param methodName the name of a method
		 * @param methodHandle a method handle
		 * @param statusStream an optional with the output stream used
		 *	for testing, iff the first method parameter, if any,
		 *	is a {@code Status}; else an empty optional
		 */
		MHAA2(String methodName, MethodHandle methodHandle,
					Optional<PrintStream> statusStream)
		{
			super(methodName, methodHandle, statusStream);
		}

		@Override
		void invoke(MethodHandle someMethodHandle) throws Throwable
		{
			someMethodHandle.invoke();
		}
	}

	/** Instances of this {@code MHAA} allow for unbound arguments. */
	private static final class MHAAN extends MHAA
	{
		private final List<?> arguments;
		private final Optional<Class<?>> varArgsType;

		/**
		 * Constructs a new {@code MHAAN} object.
		 *
		 * @param methodName the name of a method
		 * @param methodHandle a method handle
		 * @param statusStream an optional with the output stream used
		 *	for testing, iff the first method parameter, if any,
		 *	is a {@code Status}; else an empty optional
		 * @param arguments a list of supported arguments for
		 *	the passed method handle; else {@code null}, when
		 *	a method handle references a method either
		 *	<i>without</i> formal parameters or <i>with</i>
		 *	formal parameters for which all supported arguments
		 *	have already been bound
		 * @param varArgsType an optional with a variable-arity type,
		 *	iff the passed method handle takes a variable number
		 *	of arguments; else an empty optional
		 */
		MHAAN(String methodName, MethodHandle methodHandle,
			Optional<PrintStream> statusStream, List<?> arguments,
			Optional<Class<?>> varArgsType)
		{
			super(methodName, methodHandle, statusStream);
			this.arguments = Objects.requireNonNullElseGet(
								arguments,
								List::of);
			this.varArgsType = Objects.requireNonNull(varArgsType,
								"varArgsType");
		}

		private static List<?> fromArray(Object array)
		{
			return (array.getClass().getName().endsWith(";"))
				? List.of((Object[]) array) /* JVMS-11, §4.3.2. */
				: IntStream.range(0, Array.getLength(array))
					.mapToObj(Support.partialApply(
						array_ -> index -> Array.get(
								array_,
								index),
						array))
					.collect(Collectors.toUnmodifiableList());
		}

		private static List<?> plusPlus(List<?> list0, List<?> list1)
		{
			final List<Object> result = new ArrayList<>(list0.size()
							+ list1.size());
			result.addAll(list0);
			result.addAll(list1);
			return Collections.unmodifiableList(result);
		}

		@Override
		void invoke(MethodHandle someMethodHandle) throws Throwable
		{
			final int lastIndex = arguments.size() - 1;
			someMethodHandle
				.withVarargs(varArgsType.isPresent())
				.invokeWithArguments((!arguments.isEmpty()
							&& Objects.nonNull(
								arguments.get(
									lastIndex))
							&& varArgsType
						.filter(Support.<Object,
								Predicate<Class<?>>>
								partialApply(
							arg -> type -> type
								.isInstance(arg),
							arguments.get(lastIndex)))
						.isPresent())
					? plusPlus(arguments.subList(0, lastIndex),
						fromArray(arguments.get(
								lastIndex)))
					: arguments);
		}

		@Override
		public String toString()
		{
			return Stream.of(Stream.of(super.toString()),
					arguments.stream()
						.map(arg -> {
							final String value = Support
								.asString(arg);
							final String type = (arg != null)
								? arg.getClass()
									.getName()
								: "<null>";
							return new StringBuilder(3
									+ value.length()
									+ type.length())
								.append("(")
								.append(type)
								.append(") ")
								.append(value)
								.toString();
						}),
					Stream.of(varArgsType.isPresent()
						? "varargs"
						: ""))
				.flatMap(Function.identity())
				.collect(Collectors.joining(" :: "));
		}
	}

	private record DefaultStatus(PrintStream printStream,
					Optional<String> testMethodNamePrefix)
							implements Status
	{
		/**
		 * Constructs a new {@code DefaultStatus} object.
		 *
		 * @param printStream an output stream used for testing
		 * @param testMethodNamePrefix an optional with the test
		 *	method name prefix in progress, else an empty optional
		 */
		DefaultStatus
		{
			Objects.requireNonNull(printStream, "printStream");
			Objects.requireNonNull(testMethodNamePrefix,
						"testMethodNamePrefix");
		}
	}
}
