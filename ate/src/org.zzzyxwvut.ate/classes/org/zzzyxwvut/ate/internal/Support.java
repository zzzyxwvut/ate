package org.zzzyxwvut.ate.internal;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/** This class lends test-related support. */
public final class Support
{
	private Support() { /* No instantiation. */ }

	/**
	 * Returns a string representation of the passed value.
	 *
	 * @param value a value
	 * @return a string representation of the passed value
	 */
	public static String asString(Object value)
	{
		return (value == null)
				? "null"
				: (!value.getClass().isArray())
			? value.toString()
			: switch (value.getClass()
					.getName()
					.charAt(1)) {
			case 'B' -> Arrays.toString((byte[]) value);
			case 'C' -> Arrays.toString((char[]) value);
			case 'D' -> Arrays.toString((double[]) value);
			case 'F' -> Arrays.toString((float[]) value);
			case 'I' -> Arrays.toString((int[]) value);
			case 'J' -> Arrays.toString((long[]) value);
			case 'L' -> Arrays.toString((Object[]) value);
			case 'S' -> Arrays.toString((short[]) value);
			case 'Z' -> Arrays.toString((boolean[]) value);
			default -> Arrays.deepToString((Object[]) value);
		}; /* Cf. jvalue in jni.h */
	}

	/**
	 * Transforms the passed collection into a stream, concurrent or
	 * sequential.
	 *
	 * @param <E> the type of an element
	 * @param source a collection
	 * @param concurrent whether to prefer a concurrent stream
	 * @return a concurrent or a sequential stream
	 */
	public static <E> Stream<E> concurrentOrSequential(Collection<E> source,
							boolean concurrent)
	{
		Objects.requireNonNull(source, "source");
		return StreamSupport.stream(source.spliterator(), concurrent);
	}

	/**
	 * Returns the value of applying the passed function object to
	 * the passed object.
	 * <p>
	 * This method is intended to be used as follows, e.g.:
	 * <blockquote><pre><code>
	 * final Consumer&lt;String&gt; consumerPartial = Support
	 *		.&lt;java.io.PrintStream,
	 *			Consumer&lt;String&gt;&gt;partialApply(
	 *	stream -&gt; stream::print,
	 *	System.err);
	 *
	 * final Function&lt;Object, String&gt; functionConstant = Support
	 *		.&lt;String, Function&lt;Object, String&gt;&gt;partialApply(
	 *	text -&gt; stub -&gt; text,
	 *	"");
	 *
	 * final Predicate&lt;Object&gt; predicateConstant = Support
	 *		.&lt;Boolean, Predicate&lt;Object&gt;&gt;partialApply(
	 *	condition -&gt; stub -&gt; condition,
	 *	false);
	 *
	 * final Supplier&lt;Long&gt; supplierPartial = Support
	 *		.&lt;Long, Supplier&lt;Long&gt;&gt;partialApply(
	 *	bound -&gt; () -&gt; java.util.concurrent.ThreadLocalRandom
	 *		.current()
	 *		.nextLong(bound),
	 *	2L);
	 * </code></pre></blockquote>
	 *
	 * @param <T> a type
	 * @param <U> a type
	 * @param f a functional interface that takes the passed object and
	 *	returns a value
	 * @param t an object that the passed functional interface takes
	 * @return the value of applying the passed function object to
	 *	the passed object
	 */
	public static <T, U> U partialApply(Function<T, U> f, T t)
	{
		return f.apply(t);
	}

	/**
	 * Returns an optional with the primary cause throwable, if available,
	 * of the passed throwable; otherwise an empty optional.
	 *
	 * @param throwable a throwable
	 * @return an optional with the primary cause throwable, if available,
	 *	of the passed throwable; otherwise an empty optional
	 */
	public static Optional<Throwable> primaryCause(Throwable throwable)
	{
		return Stream.iterate(Objects.requireNonNull(throwable,
								"throwable")
								.getCause(),
							Objects::nonNull,
							Throwable::getCause)
			.reduce((secondaryCause, primaryCause) ->
							primaryCause);
	}

	/**
	 * Returns a string method signature for the passed method.
	 * <p>
	 * For example,
	 * <blockquote><pre><code>
	 * final boolean MethodModifiersReturnTypeSignatureThrowsClauseMatch =
	 *	("public static native java.lang.Object "
	 *	+ "java.lang.reflect.Array.get(java.lang.Object,int) throws "
	 *	+ "java.lang.IllegalArgumentException,"
	 *	+ "java.lang.ArrayIndexOutOfBoundsException")
	 *	.equals(java.lang.reflect.Array.class.getMethod(
	 *				"get", Object.class, int.class)
	 *		.toString());
	 *
	 * final boolean MethodSignatureMatch =
	 *	"java.lang.reflect.Array.get(java.lang.Object,int)"
	 *	.equals(signature(java.lang.reflect.Array.class.getMethod(
	 *				"get", Object.class, int.class)));
	 * </code></pre></blockquote>
	 *
	 * @param method a method
	 * @return a string method signature for the passed method
	 * @jls 8.4.2 Method Signature
	 */
	public static String signature(Method method)
	{
		Objects.requireNonNull(method, "method");
		final String methodString = method.toString();
		final int leftParenIndex = methodString.indexOf('(', 3);
		return methodString.substring(
			methodString.lastIndexOf(' ', leftParenIndex) + 1,
			methodString.indexOf(')', leftParenIndex) + 1);
	}

	/**
	 * Transforms the passed collection of elements into an unmodifiable
	 * enumeration map.
	 *
	 * @param <E> the type of an element
	 * @param <K> the enumeration type of a map key
	 * @param <V> the type of a map value's element
	 * @param <T> the type of a map value
	 * @param source a collection of elements
	 * @param type an enumeration type
	 * @param keyer a functional interface that takes an element and
	 *	returns a derived enumeration map key
	 * @param valuer a functional interface that takes an element and
	 *	returns a stream of derived map values
	 * @param collector a collector for the derived map values
	 * @return an unmodifiable enumeration map derived from the passed
	 *	collection
	 */
	public static <E, K extends Enum<K>, V, T> Map<K, T>
							toUnmodifiableEnumMap(
						Collection<E> source,
						Class<K> type,
						Function<E, K> keyer,
						Function<E, Stream<V>> valuer,
						Collector<V, ?, T> collector)
	{
		Objects.requireNonNull(source, "source");
		Objects.requireNonNull(type, "type");
		Objects.requireNonNull(keyer, "keyer");
		Objects.requireNonNull(valuer, "valuer");
		Objects.requireNonNull(collector, "collector");
		return Collections.unmodifiableMap(source
			.stream()
			.unordered()
			.collect(Collectors.groupingBy(
				keyer,
				Support.<Class<K>, Supplier<Map<K, T>>>
								partialApply(
					type_ -> () -> new EnumMap<>(type_),
					type),
				Collectors.flatMapping(valuer, collector))));
	}

	/**
	 * Returns a collector that accumulates the input elements into
	 * an unmodifiable ordered set.
	 *
	 * @param <E> the type of an element
	 * @return a collector that accumulates the input elements into
	 *	an unmodifiable ordered set
	 */
	public static <E> Collector<E, ?, Set<E>> toUnmodifiableOrderedSet()
	{
		return Collectors.collectingAndThen(
			Collectors.<E, Set<E>>toCollection(LinkedHashSet::new),
			Collections::unmodifiableSet);
	}

	/**
	 * Returns a collector that accumulates the input elements into
	 * an unmodifiable set.
	 *
	 * @param <E> the type of an element
	 * @param ordered whether to prefer an ordered set
	 * @return a collector that accumulates the input elements into
	 *	an unmodifiable set
	 * @see #toUnmodifiableOrderedSet()
	 */
	public static <E> Collector<E, ?, Set<E>> toUnmodifiableSet(
							boolean ordered)
	{
		return (ordered)
			? toUnmodifiableOrderedSet()
			: Collectors.toUnmodifiableSet();
	}

	/**
	 * Returns a functional interface that takes a pair of objects and
	 * returns a value.
	 *
	 * @param <T> a type
	 * @return a curried function
	 * @throws UnsupportedOperationException if application is attempted
	 */
	public static <T> BinaryOperator<T> uoeThrower()
	{
		return (left, right) -> {
			throw new UnsupportedOperationException();
		};
	}
}
