package org.zzzyxwvut.ate.internal;

import java.io.PrintStream;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;

import org.zzzyxwvut.ate.Result.OtherResult;
import org.zzzyxwvut.ate.Result.Reportable.IncompleteReport;
import org.zzzyxwvut.ate.Result.Reportable;
import org.zzzyxwvut.ate.Result.SetUpAllResult;
import org.zzzyxwvut.ate.Result.SetUpBatchResult;
import org.zzzyxwvut.ate.Result.TearDownAllResult;
import org.zzzyxwvut.ate.Result.TearDownBatchResult;
import org.zzzyxwvut.ate.Result.TestResult;
import org.zzzyxwvut.ate.Result;
import org.zzzyxwvut.ate.Testable;

/** A default {@code Reportable.Builder}. */
public final class DefaultReportableBuilder implements Reportable.Builder
{
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<OtherResult>>> otherResultReporter =
				stream -> testClass -> result -> { };
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<TestResult>>> testResultReporter =
				stream -> testClass -> result -> { };
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<SetUpAllResult>>>
						setUpAllResultReporter =
				stream -> testClass -> result -> { };
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<TearDownAllResult>>>
						tearDownAllResultReporter =
				stream -> testClass -> result -> { };
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<SetUpBatchResult>>>
						setUpBatchResultReporter =
				stream -> testClass -> result -> { };
	private Function<PrintStream,
			Function<Class<? extends Testable>,
			Consumer<TearDownBatchResult>>>
						tearDownBatchResultReporter =
				stream -> testClass -> result -> { };

	private final Function<Reporter, Reportable> reportable;

	/** Constructs a new {@code DefaultReportableBuilder} object. */
	public DefaultReportableBuilder()
	{
		reportable = reporter -> stream -> testClass -> result ->
								reporter
			.report(stream, testClass, result);
	}

	/**
	 * Constructs a new {@code DefaultReportableBuilder} object.
	 *
	 * @param reports a modifiable collection for incomplete reports to
	 *	queue in
	 */
	public DefaultReportableBuilder(Queue<IncompleteReport> reports)
	{
		record DefaultIncompleteReport(boolean printStreamInTrouble,
					Exception reportingException,
					Class<? extends Testable> testClass,
					Result result)
				implements IncompleteReport { }

		Objects.requireNonNull(reports, "reports");
		reportable = Support.<Queue<IncompleteReport>,
						Function<Reporter, Reportable>>
								partialApply(
			reports_ -> reporter ->
					stream -> testClass -> result -> {
				try {
					reporter.report(stream,
							testClass,
							result);
				} catch (final Exception exception) {
					reports_.offer(
						new DefaultIncompleteReport(
							stream.checkError(),
							exception,
							testClass,
							result));
				}
			},
			reports);
	}

	@Override
	public DefaultReportableBuilder otherResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<OtherResult>>> reporter)
	{
		otherResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public DefaultReportableBuilder testResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TestResult>>> reporter)
	{
		testResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public DefaultReportableBuilder setUpAllResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpAllResult>>> reporter)
	{
		setUpAllResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public DefaultReportableBuilder tearDownAllResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownAllResult>>> reporter)
	{
		tearDownAllResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public DefaultReportableBuilder setUpBatchResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpBatchResult>>> reporter)
	{
		setUpBatchResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public DefaultReportableBuilder tearDownBatchResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownBatchResult>>> reporter)
	{
		tearDownBatchResultReporter = Objects.requireNonNull(reporter,
								"reporter");
		return this;
	}

	@Override
	public Reportable build()
	{
		class SentinelResult extends Result
		{
			SentinelResult()	{ super(Map.of()); }

			@Override
			protected Optional<String> summary(String templet)
			{
				return Optional.empty();
			}

			@Override
			public Optional<String> testMethodNamePrefix()
			{
				return Optional.empty();
			}
		}

		class ReportableStrategy implements Reporter
		{
			private final Function<SentinelResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						sentinelResultReporter =
				result -> stream -> testClass -> {
					throw new UnsupportedOperationException();
				};
			private final Function<OtherResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						otherResultReporter;
			private final Function<TestResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						testResultReporter;
			private final Function<SetUpAllResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						setUpAllResultReporter;
			private final Function<TearDownAllResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						tearDownAllResultReporter;
			private final Function<SetUpBatchResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						setUpBatchResultReporter;
			private final Function<TearDownBatchResult,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
						tearDownBatchResultReporter;

			ReportableStrategy(DefaultReportableBuilder builder)
			{
				otherResultReporter = interchanger(
					builder.otherResultReporter);
				testResultReporter = interchanger(
					builder.testResultReporter);
				setUpAllResultReporter = interchanger(
					builder.setUpAllResultReporter);
				tearDownAllResultReporter = interchanger(
					builder.tearDownAllResultReporter);
				setUpBatchResultReporter = interchanger(
					builder.setUpBatchResultReporter);
				tearDownBatchResultReporter = interchanger(
					builder.tearDownBatchResultReporter);
			}

			private <T extends Result> Function<T,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>
								interchanger(
					Function<PrintStream,
					Function<Class<? extends Testable>,
					Consumer<T>>> reporter)
			{
				return Support.<Function<PrintStream,
					Function<Class<? extends Testable>,
					Consumer<T>>>,
						Function<T,
					Function<PrintStream,
					Consumer<Class<? extends Testable>>>>>
								partialApply(
				reporter_ -> result -> stream -> testClass ->
								reporter_
					.apply(stream)
					.apply(testClass)
					.accept(result),
				reporter);
			}

			@Override
			public void report(PrintStream stream,
					Class<? extends Testable> testClass,
					Result result)
			{
				(switch (result) {
				case OtherResult result_ ->
						otherResultReporter
							.apply(result_);
				case TestResult result_ ->
						testResultReporter
							.apply(result_);
				case SetUpAllResult result_ ->
						setUpAllResultReporter
							.apply(result_);
				case TearDownAllResult result_ ->
						tearDownAllResultReporter
							.apply(result_);
				case SetUpBatchResult result_ ->
						setUpBatchResultReporter
							.apply(result_);
				case TearDownBatchResult result_ ->
						tearDownBatchResultReporter
							.apply(result_);
				default -> sentinelResultReporter
						.apply(new SentinelResult());
				})
				.apply(stream)
				.accept(testClass);
			}
		}

		return reportable.apply(new ReportableStrategy(this));
	}

	/** This interface declares a method that reports some result. */
	private interface Reporter
	{
		/**
		 * Reports the passed result.
		 *
		 * @param stream a stream to use for output
		 * @param testClass a test class that owns the passed result
		 * @param result some result
		 */
		void report(PrintStream stream,
					Class<? extends Testable> testClass,
					Result result);
	}
}
