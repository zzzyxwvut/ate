package org.zzzyxwvut.ate.internal;

/** The predicative options of {@link org.zzzyxwvut.ate.Tester.Builder}. */
enum Option
{
	/**
	 * An option of continuing execution of remaining test methods of
	 * a class after failure.
	 */
	CONTINUE_ON_FAILURE,

	/** An option of rendering stream output plain. */
	RENDER_OUTPUT_PLAIN,

	/**
	 * An option of suppressing the printing of a throwable and its
	 * backtrace to the output stream.
	 */
	SUPPRESS_THROWABLE_PRINTING,

	/**
	 * An option of suppressing argument unassignability warnings.
	 */
	SUPPRESS_UNASSIGNABILITY,

	/**
	 * An option of verifying whether assertion for a test class is
	 * enabled.
	 */
	VERIFY_ASSERTION
}
