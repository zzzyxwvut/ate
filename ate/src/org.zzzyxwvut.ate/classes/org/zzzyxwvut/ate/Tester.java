package org.zzzyxwvut.ate;

import java.util.Set;

import org.zzzyxwvut.ate.Testable.ExecutionPolicy;
import org.zzzyxwvut.ate.configuration.Configurable;
import org.zzzyxwvut.ate.internal.DefaultTesterBuilder;

/** This interface admits access to testing facilities. */
public interface Tester
{
	/**
	 * The name of a system property that imposes a capacity limit on
	 * the LRU cache map for maintaining of {@link #PROPERTY_SOME}
	 * components, if any. (The use of it may prove worthwhile with
	 * multiple instances of this interface implementation.)
	 * <p>
	 * The assigned value is parsed as an integer that is expected to be
	 * within range of {@code [2, 0x40000001)}; using any other valid
	 * integer inhibits caching. If this property is not defined, or its
	 * value cannot be decoded into an integer, value {@code 32} is used.
	 * <p>
	 * The accepted capacity limit is subject to reduction:
	 * <ol>
	 * <li>round down to the nearest power of two value that is less than
	 * or equal to the accepted capacity limit
	 * <li>subtract one from the intermediate result
	 * </ol>
	 */
	String PROPERTY_CACHE_LIMIT = "ate.cache.limit";

	/**
	 * The name of a system property that serves to only select a subset
	 * of test classes and their test methods for execution.
	 * <p>
	 * The assigned value is parsed as a series of optional components,
	 * treated as regular expression patterns that are separated by
	 * designated Unicode code points as follows:
	 * <ul>
	 * <li>the {@code :} code point delimits class components that are
	 * made of a class name and optionally its method names
	 * <li>the <code>&#64;</code> code point separates the class name from
	 * its method names
	 * <li>the {@code ,} code point delimits method names
	 * </ul>
	 * <p>
	 * E.g. {@code -Date.some=org.example.Foo@test[AB]:.+?\\.Bar}.
	 * <p>
	 * The components are processed in a right-to-left order, to allow for
	 * duplicates, by attempting to match a component done with the use of
	 * {@link java.util.regex.Matcher#lookingAt()}.
	 */
	String PROPERTY_SOME = "ate.some";

	/**
	 * The name of a system property that specifies as its value exactly
	 * three Unicode code points to use as delimiters instead of
	 * {@code :}<code>&#64;</code>{@code ,} for parsing values of
	 * {@link #PROPERTY_SOME}.
	 * <p>
	 * Only the leftmost code point needs to be distinct from the others.
	 */
	String PROPERTY_TOKENS = "ate.tokens";

	/**
	 * The name of a system property that represents as its value a warning
	 * templet for obtained unassignable method arguments.
	 * <p>
	 * The templet string must contain a (pretty-printable) warning message
	 * interspersed with the {@code 2} {@code %s} format specifiers, with
	 * the leftmost one denoting the method signature and the rightmost one
	 * denoting unassignable arguments, and, optionally, line separators
	 * {@code %n}.
	 */
	String PROPERTY_TEMPLET_UNASSIGNABLE = "ate.templet.unassignable";

	/**
	 * Runs batches of public prefix-matching ({@code test} by default)
	 * instance methods, along with any implemented life cycle methods,
	 * and reports errors, if any, for every batch to a stream.
	 * <p>
	 * There are six life cycle groups:<ul>
	 * <li>setUpAll methods (run once before all test method groups)
	 * <li>tearDownAll methods (run once after all test method groups)
	 * <li>setUpBatch methods (run once before every test method group)
	 * <li>tearDownBatch methods (run once after every test method group)
	 * <li>setUp methods (run before every test method)
	 * <li>tearDown methods (run after every test method)
	 * </ul>
	 * <p>
	 * Arbitrary methods are treated as life cycle methods when their
	 * required modifiers and name prefixes accord as follows:
	 * <pre>
	 * {@code public static [void] setUpAll([...])
	 * public static [void] tearDownAll([...])
	 * public static [void] setUpBatch([...])
	 * public static [void] tearDownBatch([...])
	 * public [void] setUp([...])
	 * public [void] tearDown([...])}
	 * </pre>
	 * Each life cycle group may have a varying number of methods that is
	 * maintained by some combination of overloading and hiding and having
	 * distinct name postfixes. Group methods of a test type are executed
	 * in an arbitrary order; however, group methods implemented across
	 * a test type hierarchy observe their hierarchical order, with their
	 * processing arranged from the remotest ancestor type of the test type
	 * toward the test type itself, whereas the {@code tearDown*} groups
	 * follow the inverted hierarchical order of execution.
	 * <p>
	 * Each prefix-matching test method, along with all {@code setUp} and
	 * {@code tearDown} methods, shall be invoked at least once or as many
	 * times as there are signature-matching {@code Collection} elements
	 * (see {@link org.zzzyxwvut.ate.configuration.Instantiable#arguments()})
	 * for the prefix name, repeated for the number of test class instances
	 * (see {@link org.zzzyxwvut.ate.configuration.Instantiable#testables()}),
	 * where every instance shall share a particular {@code Collection}
	 * element, if any.
	 *
	 * @apiNote
	 * The {@link Testable} classes are expected to either be declared
	 * public and belong to unconditionally exportable packages
	 * (see {@link java.lang.invoke.MethodHandles#publicLookup()})
	 * or admit module access to their members
	 * (see {@link org.zzzyxwvut.ate.support.AccessibleUplooker}).
	 *
	 * @return {@code true} when either all prefix-matching test methods
	 *	have been run or there are no prefix-matching test methods to
	 *	run, otherwise {@code false}
	 * @throws AssertionError if {@link Tester.Builder#verifyAssertion()}
	 *	is opted for and assertion is disabled
	 */
	boolean runAndReport();

	/**
	 * Creates a new {@code Builder} object.
	 * <p>
	 * The implementation of the passed set of classes arranges the order
	 * of their <i>sequential</i> processing.
	 *
	 * @param classes a set of test classes
	 * @return a {@code Builder} instance
	 */
	static Builder newBuilder(Set<Class<? extends Testable>> classes)
	{
		return new DefaultTesterBuilder(classes);
	}

	/**
	 * Creates a new {@code Tester} object with default settings.
	 * <p>
	 * These settings are:
	 * <ul>
	 * <li>the sequential execution policy is used for class testing
	 * <li>the default {@link org.zzzyxwvut.ate.configuration configuration}
	 *	of testing attributes is obtained
	 * <li>no remaining test methods of the class are run after failure
	 * <li>the plain rendition of stream output is not opted for
	 * <li>a throwable and its backtrace are printed to the output stream
	 * <li>a warning is raised for the method that takes parameters and
	 *	receives unassignable arguments
	 * <li>whether assertion for the test class is enabled is not verified
	 * </ul>
	 * <p>
	 * The implementation of the passed set of classes arranges the order
	 * of their <i>sequential</i> processing.
	 *
	 * @param classes a set of test classes
	 * @return a {@code Tester} instance
	 */
	static Tester newDefaultTester(Set<Class<? extends Testable>> classes)
	{
		return newBuilder(classes).build();
	}

	/** A builder of {@code Tester}. */
	interface Builder
	{
		/**
		 * Sets an execution policy for class testing.
		 *
		 * @param executionPolicy an execution policy
		 * @return {@code this} instance
		 */
		Builder executionPolicy(ExecutionPolicy executionPolicy);

		/**
		 * Sets configurable testing attributes.
		 *
		 * @param configurable configurable testing attributes
		 * @return {@code this} instance
		 */
		Builder configurable(Configurable configurable);

		/**
		 * Opts for continuing execution of remaining test methods of
		 * a class after failure.
		 * <p>
		 * Note that the default guarantees of aborting are skewed for
		 * test methods running concurrently.
		 *
		 * @return {@code this} instance
		 */
		Builder continueOnFailure();

		/**
		 * Opts for rendering stream output plain.
		 * <p>
		 * Note that {@link Result.Reportable} implementations are
		 * free to ignore this setting.
		 *
		 * @return {@code this} instance
		 * @see Result#renderer()
		 */
		Builder renderOutputPlain();

		/**
		 * Opts for suppressing the printing of a throwable and its
		 * backtrace to the output stream.
		 *
		 * @return {@code this} instance
		 */
		Builder suppressThrowablePrinting();

		/**
		 * Opts for suppressing argument unassignability warnings.
		 * <p>
		 * (This addresses overloaded methods.)
		 *
		 * @return {@code this} instance
		 */
		Builder suppressUnassignability();

		/**
		 * Opts for verifying whether assertion for a test class is
		 * enabled.
		 *
		 * @return {@code this} instance
		 * @see Class#desiredAssertionStatus()
		 * @jls 14.10 The assert Statement
		 */
		Builder verifyAssertion();

		/**
		 * Returns a new instance of {@code Tester}.
		 *
		 * @return a new instance of {@code Tester}
		 */
		Tester build();
	}
}
