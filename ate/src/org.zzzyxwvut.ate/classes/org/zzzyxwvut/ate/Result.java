package org.zzzyxwvut.ate;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.zzzyxwvut.ate.internal.DefaultReportableBuilder;
import org.zzzyxwvut.ate.internal.MethodKindFriend;
import org.zzzyxwvut.ate.internal.ResultFriend;
import org.zzzyxwvut.ate.internal.Support;

/**
 * This class represents a result of some execution or not.
 *
 * @see org.zzzyxwvut.ate.configuration.Configurable#reportable()
 */
public abstract class Result
{
	static {
		ResultFriend.setResultable(new ResultFriend.Resultable() {
			@Override
			public OtherResult newOtherResult(
					String testMethodNamePrefix)
			{
				return new OtherResult(testMethodNamePrefix);
			}

			@Override
			public SetUpAllResult newSetUpAllResult(
					Map<MethodKind, List<Throwable>> errors)
			{
				return new SetUpAllResult(errors);
			}

			@Override
			public TearDownAllResult newTearDownAllResult(
					Map<MethodKind, List<Throwable>> errors)
			{
				return new TearDownAllResult(errors);
			}

			@Override
			public SetUpBatchResult newSetUpBatchResult(
					String testMethodNamePrefix,
					Map<MethodKind, List<Throwable>> errors)
			{
				return new SetUpBatchResult(
							testMethodNamePrefix,
							errors);
			}

			@Override
			public TearDownBatchResult newTearDownBatchResult(
					String testMethodNamePrefix,
					Map<MethodKind, List<Throwable>> errors)
			{
				return new TearDownBatchResult(
							testMethodNamePrefix,
							errors);
			}

			@Override
			public TestResult newTestResult(
					String testMethodNamePrefix,
					Map<MethodKind, List<Throwable>> errors,
					int failureTotal,
					int successTotal)
			{
				return new TestResult(testMethodNamePrefix,
							errors,
							failureTotal,
							successTotal);
			}
		});
	}

	private final Map<MethodKind, List<Throwable>> errors;

	/**
	 * Constructs a new {@code Result} object.
	 *
	 * @param errors a classified map of caught errors, if any; else
	 *	an empty map
	 */
	protected Result(Map<MethodKind, List<Throwable>> errors)
	{
		this.errors = Objects.requireNonNull(errors, "errors");
	}

	/**
	 * Returns the classified map of caught errors, if any; otherwise
	 * the empty map.
	 *
	 * @return the classified map of caught errors, if any; otherwise
	 *	the empty map
	 */
	public final Map<MethodKind, List<Throwable>> errors()
	{
		return errors;
	}

	/**
	 * Returns an optional with a new format string summary derived from
	 * the passed format string, otherwise an empty optional.
	 *
	 * @param templet a format string with the escaped {@code %%s}
	 *	specifier (denoting a {@code Testable} class name) at
	 *	an arbitrary position
	 * @return an optional with a new format string summary, otherwise
	 *	an empty optional
	 */
	/*
	 * Both, OtherResult and TestResult, expect the escaped test class name
	 * argument; other "extra arguments", however, vary and are ignored.
	 *
	 * (See ECMA-48 CSI sequences in console_codes(4).)
	 */
	protected abstract Optional<String> summary(String templet);

	/**
	 * Returns an optional with a prefix of test method names, otherwise
	 * an empty optional.
	 *
	 * @return an optional with a prefix of test method names, otherwise
	 *	an empty optional
	 */
	public abstract Optional<String> testMethodNamePrefix();

	/**
	 * Returns a functional interface that takes a mapping of result types
	 * to templets and returns a functional interface that takes some
	 * result and returns a value that is either the mapped templet of
	 * the applied result or the escaped {@code %%s} string.
	 *
	 * @return a curried function
	 * @throws java.util.IllegalFormatException if the applied mapping of
	 *	result types to templets contains a malformed templet
	 * @see #summary(String)
	 */
	public static Function<Map<Class<? extends Result>, String>,
				Function<Result, String>> renderer()
	{
		return templets -> Support.<Map<Class<? extends Result>, String>,
						Function<Result, String>>
								partialApply(
			templets_ -> result -> Objects.requireNonNullElse(
					templets_.get(result.getClass()),
					"%%s"),
			Support.<Map<Class<? extends Result>, String>,
					Map<Class<? extends Result>, String>>
								partialApply(
				templets_ -> {
					final String testTemplet = Objects
						.requireNonNullElse(
							templets_.get(
								TestResult.class),
							"%%s");

					/*
					 * TestResult.RENDITION_PLAIN =~
					 *		"%%s %s %d %d".
					 */
					final String testTemplet_ =
								new Formatter(
							new StringBuilder(128))
						.format(testTemplet, ".", -1, -1)
						.toString();
					new Formatter(new StringBuilder(128))
						.format(testTemplet_, "..");
					final String otherTemplet = Objects
						.requireNonNullElse(
							templets_.get(
								OtherResult.class),
							"%%s");

					/*
					 * OtherResult.RENDITION_PLAIN =~
					 *			"%%s %s".
					 */
					final String otherTemplet_ =
								new Formatter(
							new StringBuilder(128))
						.format(otherTemplet, ".")
						.toString();
					new Formatter(new StringBuilder(128))
						.format(otherTemplet_, "..");
					return templets_;
				},
				templets));
	}

	private static Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<String>>> reporter()
	{
		return stream -> testClass -> templet -> stream
			.format(templet, testClass.getName());
	}

	/**
	 * Returns a functional interface that takes a functional interface
	 * that takes some applied result and returns a templet value, and
	 * returns a functional interface that takes a print stream and
	 * returns a functional interface that takes a test class and
	 * returns a functional interface that takes some result and, as
	 * a side effect, pretty-prints the result summary, if available, to
	 * the applied stream and does not return a value.
	 *
	 * @return a curried function
	 * @see #renderer()
	 */
	public static Function<Function<Result, String>, Reportable> resultor()
	{
		return renderer -> stream -> testClass -> result -> result
			.summary(renderer
				.apply(result))
			.ifPresent(reporter()
				.apply(stream)
				.apply(testClass));
	}

	/** A {@code Result} for want of execution. */
	public static final class OtherResult extends Result
	{
		/**
		 * A templet string.
		 *
		 * @see #summary(String)
		 */
		public static final String RENDITION_PLAIN =
				"%n(%%s)%nNon-matching prefix: %s%n";

		/**
		 * A templet string with parts rendered in reverse video.
		 *
		 * @see #summary(String)
		 */
		public static final String RENDITION_REVERSE_VIDEO =
				"%n(\033[7m%%s\033[0m)"
				+ "%nNon-matching prefix: \033[7m%s\033[0m%n";

		private final String testMethodNamePrefix;

		/**
		 * Constructs a new {@code OtherResult} object with an empty
		 * errors map.
		 *
		 * @param testMethodNamePrefix a non-matching prefix of test
		 *	method names
		 */
		OtherResult(String testMethodNamePrefix)
		{
			super(Map.of());
			this.testMethodNamePrefix = Objects.requireNonNull(
						testMethodNamePrefix,
						"testMethodNamePrefix");
		}

		/**
		 * Returns an optional with a new format string summary
		 * derived from the passed format string.
		 * <p>
		 * The argument must only contain, at arbitrary positions,
		 * the following {@code 2} format specifiers: {@code %s}
		 * (denoting the name prefix) and the escaped {@code %%s}
		 * (denoting a {@code Testable} class name).
		 *
		 * @param escapedKlassNamePrefixTemplet a format string
		 * @return an optional with a new format string summary
		 */
		@Override
		protected Optional<String> summary(String
					escapedKlassNamePrefixTemplet)
		{
			Objects.requireNonNull(escapedKlassNamePrefixTemplet,
					"escapedKlassNamePrefixTemplet");
			return Optional.of(String.format(
						escapedKlassNamePrefixTemplet,
						testMethodNamePrefix));
		}

		/**
		 * Returns an optional with the non-matching prefix of test
		 * method names.
		 *
		 * @return an optional with the non-matching prefix of test
		 *	method names
		 */
		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.of(testMethodNamePrefix);
		}

		@Override
		public String toString() { return testMethodNamePrefix; }
	}

	/** A {@code Result} of paraprefixable life-cycle methods execution. */
	public static abstract class ParaprefixableResult extends Result
	{
		/**
		 * Constructs a new {@code ParaprefixableResult} object.
		 *
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 */
		protected ParaprefixableResult(
				Map<MethodKind, List<Throwable>> errors)
		{
			super(errors);
		}

		/**
		 * {@inheritDoc}
		 *
		 * @return an empty optional
		 */
		@Override
		protected Optional<String> summary(String templet)
		{
			return Optional.empty();
		}

		/**
		 * {@inheritDoc}
		 *
		 * @return an empty optional
		 */
		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.empty();
		}

		@Override
		public String toString()	{ return errors().toString(); }
	}

	/**
	 * A {@code ParaprefixableResult} of {@code setUpAll} methods
	 * execution.
	 */
	public static final class SetUpAllResult extends ParaprefixableResult
	{
		/**
		 * Constructs a new {@code SetUpAllResult} object.
		 *
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 */
		SetUpAllResult(Map<MethodKind, List<Throwable>> errors)
		{
			super(errors);
		}
	}

	/**
	 * A {@code ParaprefixableResult} of {@code tearDownAll} methods
	 * execution.
	 */
	public static final class TearDownAllResult extends ParaprefixableResult
	{
		/**
		 * Constructs a new {@code TearDownAllResult} object.
		 *
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 */
		TearDownAllResult(Map<MethodKind, List<Throwable>> errors)
		{
			super(errors);
		}
	}

	/**
	 * A {@code ParaprefixableResult} of {@code setUpBatch} methods
	 * execution.
	 */
	public static final class SetUpBatchResult extends ParaprefixableResult
	{
		private final String testMethodNamePrefix;

		/**
		 * Constructs a new {@code SetUpBatchResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 */
		SetUpBatchResult(String testMethodNamePrefix,
				Map<MethodKind, List<Throwable>> errors)
		{
			super(errors);
			this.testMethodNamePrefix = Objects.requireNonNull(
						testMethodNamePrefix,
						"testMethodNamePrefix");
		}

		/**
		 * Returns an optional with the matching prefix of test method
		 * names.
		 *
		 * @return an optional with the matching prefix of test method
		 *	names
		 */
		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.of(testMethodNamePrefix);
		}
	}

	/**
	 * A {@code ParaprefixableResult} of {@code tearDownBatch} methods
	 * execution.
	 */
	public static final class TearDownBatchResult extends ParaprefixableResult
	{
		private final String testMethodNamePrefix;

		/**
		 * Constructs a new {@code TearDownBatchResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 */
		TearDownBatchResult(String testMethodNamePrefix,
				Map<MethodKind, List<Throwable>> errors)
		{
			super(errors);
			this.testMethodNamePrefix = Objects.requireNonNull(
						testMethodNamePrefix,
						"testMethodNamePrefix");
		}

		/**
		 * Returns an optional with the matching prefix of test method
		 * names.
		 *
		 * @return an optional with the matching prefix of test method
		 *	names
		 */
		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.of(testMethodNamePrefix);
		}
	}

	/** A {@code Result} of batched test methods execution. */
	public static final class TestResult extends Result
	{
		/**
		 * A templet string.
		 *
		 * @see #summary(String)
		 */
		public static final String RENDITION_PLAIN =
				"%n(%%s#%s)%nPASSED: %d, FAILED: %d%n";

		/**
		 * A templet string with parts rendered in reverse video.
		 *
		 * @see #summary(String)
		 */
		public static final String RENDITION_REVERSE_VIDEO =
				"%n(\033[7m%%s#%s\033[0m)"
				+ "%n\033[7mPASSED: %d\033[0m,"
				+ " \033[7mFAILED: %d\033[0m%n";

		/**
		 * A templet string with parts rendered in red, yellow, and
		 * green, foreground colours.
		 *
		 * @see #summary(String)
		 */
		public static final String RENDITION_RED_YELLOW_GREEN =
				"%n(\033[1;7;33m%%s#%s\033[0m)"
				+ "%n\033[1;4;7;32mPASSED: %d\033[0m,"
				+ " \033[1;4;7;31mFAILED: %d\033[0m%n";

		private final String testMethodNamePrefix;
		private final int failureTotal;
		private final int successTotal;

		/**
		 * Constructs a new {@code TestResult} object.
		 *
		 * @param testMethodNamePrefix a matching prefix of test
		 *	method names
		 * @param errors a classified map of caught errors, if any;
		 *	else an empty map
		 * @param failureTotal a number of failed name-matching tests
		 * @param successTotal a number of passed name-matching tests
		 */
		TestResult(String testMethodNamePrefix,
					Map<MethodKind, List<Throwable>> errors,
					int failureTotal,
					int successTotal)
		{
			super(errors);
			this.testMethodNamePrefix = Objects.requireNonNull(
						testMethodNamePrefix,
						"testMethodNamePrefix");
			this.failureTotal = (failureTotal < 0)
				? 0
				: failureTotal;
			this.successTotal = (successTotal < 0)
				? 0
				: successTotal;
		}

		/**
		 * Returns the number of failed name-matching tests.
		 *
		 * @return the number of failed name-matching tests
		 */
		public int failureTotal()	{ return failureTotal; }

		/**
		 * Returns the number of passed name-matching tests.
		 *
		 * @return the number of passed name-matching tests
		 */
		public int successTotal()	{ return successTotal; }

		/**
		 * Returns an optional with a new format string summary
		 * derived from the passed format string.
		 * <p>
		 * The argument must only contain {@code 4} format specifiers
		 * in the following order: {@code %s}, {@code %d}, {@code %d}
		 * (denoting the name prefix, success total, and failure
		 * total); and, at some non-disordering position, the escaped
		 * {@code %%s} (denoting a {@code Testable} class name).
		 *
		 * @param escapedKlassNamePrefixSuccessFailureTemplet a format
		 *	string
		 * @return an optional with a new format string summary
		 */
		@Override
		protected Optional<String> summary(String
				escapedKlassNamePrefixSuccessFailureTemplet)
		{
			Objects.requireNonNull(
				escapedKlassNamePrefixSuccessFailureTemplet,
				"escapedKlassNamePrefixSuccessFailureTemplet");
			return Optional.of(String.format(
				escapedKlassNamePrefixSuccessFailureTemplet,
				testMethodNamePrefix,
				successTotal,
				failureTotal));
		}

		/**
		 * Returns an optional with the matching prefix of test method
		 * names.
		 *
		 * @return an optional with the matching prefix of test method
		 *	names
		 */
		@Override
		public Optional<String> testMethodNamePrefix()
		{
			return Optional.of(testMethodNamePrefix);
		}

		@Override
		public String toString()
		{
			return String.format("%s: (+) %d, (-) %d",
							testMethodNamePrefix,
							successTotal,
							failureTotal);
		}
	}

	/**
	 * A functional interface that takes a print stream and
	 * returns a functional interface that takes a test class and
	 * returns a functional interface that takes some result and
	 * performs an arbitrary action and does not return a value.
	 * <p>
	 * The action is carried out after invoking, if implemented:
	 * <ul>
	 * <li>the {@code setUpAll} life cycle methods
	 * <li>the {@code setUpBatch} life cycle methods
	 * <li>every batch of either run prefix-matching test methods or
	 *	skipped non-prefix-matching test methods
	 * <li>the {@code tearDownBatch} life cycle methods
	 * <li>the {@code tearDownAll} life cycle methods
	 * </ul>
	 * <p>
	 * Default implementations:
	 * <blockquote><pre><code>
	 * final Reportable multiColouredOutputReportable = Result.resultor()
	 *	.apply(Result.renderer()
	 *		.apply(Map.of(OtherResult.class,
	 *			OtherResult.RENDITION_REVERSE_VIDEO,
	 *			TestResult.class,
	 *			TestResult.RENDITION_RED_YELLOW_GREEN)));
	 *
	 * final Reportable plainColouredOutputReportable = Result.resultor()
	 *	.apply(Result.renderer()
	 *		.apply(Map.of(OtherResult.class,
	 *			OtherResult.RENDITION_PLAIN,
	 *			TestResult.class,
	 *			TestResult.RENDITION_PLAIN)));
	 * </code></pre></blockquote>
	 *
	 * @see #renderer()
	 * @see #resultor()
	 * @see org.zzzyxwvut.ate.configuration.SummarisingRecorder#recorder()
	 */
	public interface Reportable extends Function<PrintStream,
					Function<Class<? extends Testable>,
					Consumer<Result>>>
	{
		/**
		 * Creates a new {@code Builder} object.
		 *
		 * @return a new {@code Builder} object
		 */
		static Builder newBuilder()
		{
			return new DefaultReportableBuilder();
		}

		/**
		 * Creates a new {@code Builder} object.
		 *
		 * @apiNote
		 * A concurrent modifiable queue must be used for a concurrent
		 * class-testing {@link org.zzzyxwvut.ate.Tester.Builder#executionPolicy(ExecutionPolicy) execution policy}.
		 *
		 * @param reports a modifiable collection for incomplete
		 *	reports to queue in
		 * @return a new {@code Builder} object
		 */
		static Builder newBuilder(Queue<IncompleteReport> reports)
		{
			return new DefaultReportableBuilder(reports);
		}

		/** A builder of {@code Reportable}. */
		interface Builder
		{
			/**
			 * Sets an {@link OtherResult} reporter.
			 *
			 * @param reporter an {@code OtherResult} reporter
			 * @return {@code this} instance
			 */
			Builder otherResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<OtherResult>>> reporter);

			/**
			 * Sets a {@link TestResult} reporter.
			 *
			 * @param reporter a {@code TestResult} reporter
			 * @return {@code this} instance
			 */
			Builder testResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TestResult>>> reporter);

			/**
			 * Sets a {@link SetUpAllResult} reporter.
			 *
			 * @param reporter a {@code SetUpAllResult}
			 *	reporter
			 * @return {@code this} instance
			 */
			Builder setUpAllResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpAllResult>>> reporter);

			/**
			 * Sets a {@link TearDownAllResult} reporter.
			 *
			 * @param reporter a {@code TearDownAllResult}
			 *	reporter
			 * @return {@code this} instance
			 */
			Builder tearDownAllResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownAllResult>>> reporter);

			/**
			 * Sets a {@link SetUpBatchResult} reporter.
			 *
			 * @param reporter a {@code SetUpBatchResult}
			 *	reporter
			 * @return {@code this} instance
			 */
			Builder setUpBatchResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<SetUpBatchResult>>> reporter);

			/**
			 * Sets a {@link TearDownBatchResult} reporter.
			 *
			 * @param reporter a {@code TearDownBatchResult}
			 *	reporter
			 * @return {@code this} instance
			 */
			Builder tearDownBatchResultReporter(
				Function<PrintStream,
				Function<Class<? extends Testable>,
				Consumer<TearDownBatchResult>>> reporter);

			/**
			 * Returns a new instance of {@code Reportable}.
			 * <p>
			 * (Note that only set reporters can report.)
			 *
			 * @return a new instance of {@code Reportable}
			 */
			Reportable build();
		}

		/**
		 * This interface declares methods that describe the state of
		 * an incomplete report.
		 */
		interface IncompleteReport
		{
			/**
			 * Returns whether the output stream assigned to
			 * the {@link org.zzzyxwvut.ate.Tester} instance has
			 * lately encountered any {@link java.io.IOException}
			 * (which bears no relation to the causal exception).
			 *
			 * @return whether the output stream assigned to
			 *	the {@code Tester} instance has lately
			 *	encountered any {@code IOException}
			 * @see org.zzzyxwvut.ate.configuration.Configurable#printStream()
			 */
			boolean printStreamInTrouble();

			/**
			 * Returns the causal exception of this instance.
			 *
			 * @return the causal exception of this instance
			 */
			Exception reportingException();

			/**
			 * Returns the test class that owns this instance
			 * result.
			 *
			 * @return the test class that owns this instance
			 *	result
			 */
			Class<? extends Testable> testClass();

			/**
			 * Returns some result that may not have been reported.
			 *
			 * @return some result that may not have been reported
			 */
			Result result();
		}
	}

	/** This enumeration specifies supported kinds of methods. */
	public enum MethodKind
	{
		/** The other methods kind. */
		OTHER(""),

		/** The {@code setUpAll} methods kind. */
		SET_UP_ALL("setUpAll"),

		/** The {@code setUpBatch} methods kind. */
		SET_UP_BATCH("setUpBatch"),

		/** The {@code setUp} methods kind. */
		SET_UP("setUp"),

		/** The {@code test} methods kind. */
		TEST("test"),

		/** The {@code tearDown} methods kind. */
		TEAR_DOWN("tearDown"),

		/** The {@code tearDownBatch} methods kind. */
		TEAR_DOWN_BATCH("tearDownBatch"),

		/** The {@code tearDownAll} methods kind. */
		TEAR_DOWN_ALL("tearDownAll");

		/**
		 * A set of method kinds whose elements require processing in
		 * their ascending order for their assignable method instances
		 * implemented across a type hierarchy.
		 */
		static final Set<MethodKind> ASCENDING_ORDER_METHOD_KINDS =
						Collections.unmodifiableSet(
								EnumSet.of(
					MethodKind.TEAR_DOWN,
					MethodKind.TEAR_DOWN_BATCH,
					MethodKind.TEAR_DOWN_ALL));

		/**
		 * A reversely-ordered list of life cycle method name prefixes.
		 */
		static final List<String> LIFE_CYCLE_METHOD_KIND_NAMES =
								EnumSet
			.complementOf(EnumSet.of(MethodKind.OTHER,
							MethodKind.TEST))
			.stream()
			.map(MethodKind::toString)
			.sorted(Comparator.reverseOrder())
			.collect(Collectors.toUnmodifiableList());

		private static final Map<String, MethodKind> NAMES = EnumSet
			.allOf(MethodKind.class)
			.stream()
			.collect(Collectors.toUnmodifiableMap(
							MethodKind::toString,
							Function.identity()));

		static {
			MethodKindFriend.setMethodKindable(
					new MethodKindFriend.MethodKindable() {
				@Override
				public boolean inAscendingOrder(
							MethodKind kind)
				{
					return MethodKind.ASCENDING_ORDER_METHOD_KINDS
						.contains(kind);
				}

				@Override
				public MethodKind lifeCycleMethodKindOrOther(
							Method method)
				{
					return MethodKind.LIFE_CYCLE_METHOD_KIND_NAMES
						.stream()
						.filter(Support.<String,
								Predicate<String>>
								partialApply(
							methodName -> methodName
								::startsWith,
							method.getName()))
						.findAny()
						.flatMap(MethodKind::fromString)
						.orElse(MethodKind.OTHER);
				}
			});
		}

		private final String kind;

		private MethodKind(String kind)	{ this.kind = kind; }

		/**
		 * Returns an optional with the method kind that corresponds to
		 * the passed method name prefix, otherwise an empty optional.
		 *
		 * @param kindName a method name prefix of a supported kind
		 * @return an optional with the method kind that corresponds to
		 *	the passed method name prefix, otherwise an empty
		 *	optional
		 */
		public static Optional<MethodKind> fromString(String kindName)
		{
			Objects.requireNonNull(kindName, "kindName");
			return Optional.ofNullable(NAMES.get(kindName));
		}

		/**
		 * Returns the method name prefix of a supported kind.
		 *
		 * @return the method name prefix of a supported kind
		 */
		@Override
		public String toString()		{ return kind; }
	}
}
