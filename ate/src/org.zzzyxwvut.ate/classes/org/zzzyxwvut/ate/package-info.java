/**
 * Provides testing facilities.
 * <h2><a id="implementation">Implementation</a></h2>
 * Make every test class implement {@link Testable}. For a test class (or
 * a test interface) hierarchy, consider making the <i>remotest</i>
 * test-subtype of {@code java.lang.Object} (or of the base interface)
 * conforming to {@code Testable}.
 * <p>
 * With a module declaration as follows:
 * <blockquote><pre><code>
 * module org.group
 * {
 *	requires static org.zzzyxwvut.ate;
 *
 *	exports org.group;
 * }</code></pre></blockquote>
 * declare a test class, for example:
 * <blockquote><pre><code>
 * package org.group;
 *
 * import org.zzzyxwvut.ate.Status;
 * import org.zzzyxwvut.ate.Testable;
 *
 * &#64;SuppressWarnings("exports")
 * public class SomeTests implements Testable
 * {
 *	private double value;
 *
 *	public void setUp(Status status)
 *	{
 *		value = Double.valueOf("NaN");
 *		status.printStream().format("Setting up value: %f%n", value);
 *	}
 *
 *	public void testEquality(Status status)
 *	{
 *		final double expected = Double.valueOf("NaN");
 *		final double obtained = value;
 *		status.printStream().format("%f != %f%n", expected, obtained);
 *		assert expected != obtained;
 *	}
 *
 *	// Other test and life cycle methods.
 * }</code></pre></blockquote>
 * <p>
 * Consider implementing an {@code instantiable} factory method in the test
 * class (see {@link org.zzzyxwvut.ate.configuration.Instantiable}) whenever
 * there is a need to use constructors with parameters or to share test class
 * instances among test methods. Otherwise, a new instance shall be obtained,
 * by invoking an accessible parameterless constructor, declared or default,
 * for every test method to be run.
 * <p>
 * Create an entry point class with the {@code main} method that shall drive
 * execution of test classes with a {@link Tester} instance. For example,
 * <blockquote><pre><code>
 * package other.group;
 *
 * import java.util.Set;
 *
 * import org.zzzyxwvut.ate.Tester;
 *
 * import org.group.SomeTests;
 *
 * class SomeTester
 * {
 *	static {
 *		SomeTester.class
 *			.getClassLoader()
 *			.setPackageAssertionStatus("org.group", true);
 *	}
 *
 *	public static void main(String... args)
 *	{
 *		Tester.newBuilder(Set.of(SomeTests.class))
 *			.verifyAssertion()
 *			.build()
 *			.runAndReport();
 *	}
 * }</code></pre></blockquote>
 * <h2><a id="configuration">Configuration</a></h2>
 * According to the extent of their applicability, the supported means of
 * configuration of testing attributes can be classified as:
 * <ul>
 * <li>Class-global (via system properties, whose names are listed as public
 * constants in {@link Tester})
 * <li>Class-grouped (via {@link Tester.Builder} methods)
 * <li>Class-local (via {@link org.zzzyxwvut.ate.configuration.Instantiable}
 * methods)
 * </ul>
 * <h2><a id="execution">Execution</a></h2>
 * Have executed test methods and life cycle methods of test classes, passed
 * to a {@code Tester#newBuilder(Set)} method in the entry point class:
 * <blockquote><pre><code>
 * java --add-modules org.zzzyxwvut.ate,... \
 *	--module-path ... \
 *	--patch-module org.group=build/tests/modules/org.group \
 *	--module org.group/other.group.SomeTester
 * </code></pre></blockquote>
 * To execute the test methods of a specific test class, use its name as
 * a component of the value of the {@link Tester#PROPERTY_SOME} property
 * value.
 * <p>
 * For example, with
 * {@code -Date.some=org.example.Foo@test[AB]:.+?\\.Bar}, only invoke test
 * methods {@code testA} and {@code testB} of class {@code org.example.Foo},
 * and all test methods of a class {@code Bar} from any package.
 *
 * @see Tester#runAndReport()
 */
package org.zzzyxwvut.ate;
