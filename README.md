Another Testing Engine (and so is ‘she that blindeth all’).

(There are separately maintained branches for the platforms [Java 11](https://bitbucket.org/zzzyxwvut/ate/branch/java/11 "java/11") and  
[Java 17](https://bitbucket.org/zzzyxwvut/ate/branch/java/17 "java/17").)

Refer to exported package-declaration files for details.

Take these steps to configure this project's build:

* supply an Apache Ivy library on the class path  
(see <https://ant.apache.org/ivy/download.cgi>)  
(see <https://ant.apache.org/manual/install.html#optionalTasks>)

* adapt the path to a local (to be) Apache Ivy repository for publishing  
(use `ate.ivy.repo.ivy2.dir` in `ate/common/ivy.properties`)

* adapt the path to a local (to be) Apache Maven repository for publishing  
(use `ate.ivy.repo.m2.dir` in `ate/common/ivy.properties`)

* adapt the path to a JDK-21 (or newer) compiler for the `ant` `javac` task  
(use `test.javac.executable` in `ate/common/build.properties`)

Build the project and locally publish its Apache Ivy and Apache Maven
artifacts:  
`cd repos/ate/ate/`  
`ant -projecthelp`  
`ant ivy-publish ## -lib ${IVYJAR:?}`

See a sample Apache Maven client
[here](https://github.com/zzzyxwvut/numerics.git "here").

See a sample Apache Ant client
[here](https://bitbucket.org/zzzyxwvut/monetary.git "here").

See an XML-test-reporting client
[here](https://bitbucket.org/zzzyxwvut/monetary-examples.git "here").
