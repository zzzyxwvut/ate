#!/bin/sh -e

: "${1:?no such library directory}"
sp_tb=' 	'
nl='
'
ant_tar=apache-ant-1.10.14-bin.tar.gz
ivy_tar=apache-ivy-2.5.2-bin.tar.gz

# A dictionary of newline-delimited entries of blank-delimited key-values, e.g.
# "https://example.org/file_0 /path/to/file_0[ /path/to/file_1 ...]
# https://example.org/file_10 /path/to/file_10[ /path/to/file_11 ...]".
wget_cache="\
https://archive.apache.org/dist/ant/binaries/${ant_tar} $1/${ant_tar}
https://archive.apache.org/dist/ant/ivy/2.5.2/${ivy_tar} $1/${ivy_tar}\
"

_echon()
{
	printf '%s' "$@"
}

_tail()
{
	local IFS="${sp_tb}"
	set -- $@
	shift
	_echon "$*"
}

_head()
{
	local IFS="${sp_tb}"
	set -- $@
	_echon "$1"
}

_try_cache()
{
	local e v miss="" IFS="${nl}"
	set -- $@
	IFS="${sp_tb}"		# Entail telling curtailment on _tail.

	for e
	do
		for v in `_tail $e`
		do
			if test ! -f "$v"
			then
				miss="${miss} `_head $e`"
				break
			fi
		done
	done

	_echon "${miss# }"
}

IFS="${nl}"
cache_miss_1=`_try_cache ${wget_cache}`		# ~49 MiB (untarred).
test -n "${cache_miss_1}" || exit 0
test -d "$1" || mkdir -p "$1"
wget_tries=16
IFS="${sp_tb}"

for i in `seq -s \  ${wget_tries}`
do
	wget -q -o /dev/null -P "$1" ${cache_miss_1}
	test "$?" -ne 0 || break
done

IFS="${nl}"
cache_miss_2=`_try_cache ${wget_cache}`
test -z "${cache_miss_2}" || exit ${wget_tries}

for f in "$1/${ant_tar}" "$1/${ivy_tar}"
do
	if test -s "$f"
	then
		tar -x -f "$f" -C "$1"
		: >| "$f"
		mv "${f%%-bin.tar.gz}" "${f%%-[0-9]*}"
	fi
done
