#!/bin/sh -e
#
# See https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/
#
#
# All generated results-data files are assumed to be named after
# the fully.qualified.Test.number fashion (so as to amass data
# in distinct files for each requested test-method-name prefix
# of a test class) and their contents be laid out as follows:
#
# +---------------------------------------------------------------------+
# |testMethodNamePrefix<SPACE>successCount<SPACE>failureCount<NL>	|
# |[fully.qualified.ExceptionType[<SPACE>Oops!^^See #1.^^trace...]]<NL> |
# |[fully.qualified.ExceptionType[<SPACE>Oops!^^See #2.^^trace...]]<NL> |
# |...									|
# +---------------------------------------------------------------------+
#
# Where every failure instance is written on a separate line with
# an error type and a detail message, if any, and some top stack
# frame traces represented as strings.
# All embedded record-separator characters (0x1E) are assumed to
# be freely replaceable with new-line characters (0x0A).

case "$1" in
-h | --help)
	printf >&2 "Usage: %s /path/to/test-reports /path/to/results-data

xmllint --noout --schema /path/to/surefire-test-report.xsd \\
\t\t\t\t/path/to/test-reports/*.xml

Assemble XML report files from test results data files.

Exit status:
0, success (with no parsed error entries from results data)
1, failure owing to parsed error entries from results data
2, printing of this help message
3, failure reading results data
4, failure writing XML reports
5, failure writing XML reports and parsing of error entries
" \
"$0"
	exit 2
	;;
esac

: "${1:?no such directory}"
: "${2:?no such directory}"
test -x "$2" || exit 3				# No results-data files found.
test -d "$1" || mkdir -p "$1"
set +f						# Enable pathname expansion.
export LC_ALL=C.UTF-8
report_head='"\
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<testsuite xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
    xsi:noNamespaceSchemaLocation=\"https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report.xsd\"
    name=\"$1\"
    tests=\"$2\"
    errors=\"$3\"
    skipped=\"0\"
    failures=\"0\">\
"'
test_passing='"\
  <testcase name=\"$1\" time=\"0.00\" />\
"'
test_failing='"\
  <testcase name=\"$1\" time=\"0.00\">
    <failure type=\"$2\" message=\"$3\" />
  </testcase>\
"'
report_tail="\
</testsuite>\
"
rfd=3						# Readable file descriptor.
wfd=4						# Writable file descriptor.
sp=' '
sp_tb=' 	'
nl='
'
rs=

_substitute_char()
{
	local part text='' IFS="$1"

	for part in $3
	do
		text="${text}${part}$2"
	done

	case "$3" in
	*"$1")	printf '%s' "${text}"
		;;
	*)	printf '%s' "${text%"$2"}"
		;;
	esac
}

_translate_chars()
{
	local char chars="$2" text="$3" IFS="${sp_tb}"
	set -- $1

	for char in ${chars}
	do
		case "${text}" in
		*"$1"*)	text="`_substitute_char "$1" "${char}" "${text}"`"
			;;
		esac

		shift
	done

	printf '%s' "${text}"
}

_write_failure_element()
{
	local name="$2" type="$3" message="$4" IFS="$1"
	message="`_translate_chars '& < > "' '&amp; &lt; &gt; &quot;' \
							"${message}"`"

	case "${message}" in
	*"${rs}"*)
		# Replace _record-separators_ (0x1E) with _new-lines_ (0x0A).
		#
		# (Note that carrying out this replacement and writing out its
		# result must take place in the same function body, otherwise
		# the result obtained via command substitution shall want all
		# trailing new-lines.)
		IFS="${rs}"
		set -- ${message}
		IFS="${nl}"

		case "${message}" in
		*"${rs}")
			set -- "${name}" "${type}" "$*${nl}"
			;;
		*)	set -- "${name}" "${type}" "$*"
			;;
		esac

		eval printf >&${wfd} '%s' "${test_failing}"
		;;
	*)	set -- "${name}" "${type}" "${message}"
		eval printf >&${wfd} '%s' "${test_failing}"
		;;
	esac
}

_write_success_element()
{
	set -- "$1"
	eval printf >&${wfd} '%s' "${test_passing}"
}

_writeln()
{
	printf >&${wfd} "%s${nl}" "$1"
} # See https://pubs.opengroup.org/onlinepubs/9699919799/utilities/echo.html#tag_20_37_16

_pos_num_gt()
{
	case "$1" in
	0[0-9]* | *[!0-9]*)	# Reject (pseudo-)octal numbers as well.
		return 1
		;;
	esac

	test "${1:--1}" -gt "${2:--1}"
}

_write_report()
{
	local agenda element offset offsets status=0 IFS="${nl}"
	test "$#" -gt 5 || return 4
	eval exec "${wfd}>$1"
	shift
	eval _writeln "${report_head}"
	shift 3
	offsets="$1"
	agenda="$2"

	# Process a string of non-delimited elements, with offsets separately
	# stored, composed of newline-delimited variable-sized sub-elements:
	# name[<NL>type<NL>message].
	while test -n "${agenda}"
	do
		offset="${offsets%${offsets#*${sp}}}"	# Have a trailing space!
		offsets="${offsets#${offset}}"
		offset="${offset%${sp}}"

		if test "`! _pos_num_gt "${offset}" 0`"
		then
			status=4
			break
		fi

		# Observe the symmetry of substring operations with _expr_:
		# calculating lengths and applying calculated lengths.
		element="`expr substr "${agenda}" 1 ${offset}`"
		agenda="${agenda#"${element}"}"

		# Balance dropped trailing newlines, if any, after the _expr_
		# command substitution, by stripping leading newlines, e.g.
		#
		# agenda:
		# alpha<NL>beta<NL><NL>gamma<NL>delta<NL><NL>
		#
		# element (after `expr substr "${agenda}" 1 12`):
		# alpha<NL>beta<NL><NL>
		#	       ^^^^^^^^ (dropped)
		#
		# agenda (after ${agenda#"${element}"}):
		# <NL><NL>gamma<NL>delta<NL><NL>
		# ^^^^^^^^ (to be stripped)
		case "${agenda}" in
		"${nl}"*)
			agenda="${agenda#${agenda%%[!"${nl}"]*}}"
			;;
		esac

		set -- ${element}

		case "$#" in
		1)	_write_success_element "$1"		# name
			;;
		*)	_write_failure_element "${nl}" "$@"	# name, type...
			;;
		esac

		_writeln ""
	done

	_writeln "${report_tail}"
	eval exec "${wfd}>&-"
	return ${status}
}

unset agenda agenda_ class class_ failure failure_ offsets offsets_ \
	success success_ dummy element error file message name offset
IFS="${nl}"
status=0

# Pathname expansion produces prefix-matching names in clusters.
for file in "${2%%/}"/* ''
do
	agenda=''
	offsets=''

	case "${file}" in
	"${2%%/}"/\*)
		exit 3				# No results-data files found.
		;;
	'')	class=''			# The last file sentinel for
		success=0			# a _write_report call.
		failure=0
		;;
	*)	file="`_translate_chars '$' '\\$' "${file}"`"
		class="${file##*/}"
		class="${class%.*}"		# fully.qualified.Test.number
		eval exec "${rfd}<${file}"
		IFS="${sp_tb}" read -r name success failure dummy <&${rfd}
		test -n "${name}" || continue
		_pos_num_gt "${success}" || continue
		_pos_num_gt "${failure}" || continue

		while IFS="${sp_tb}" read -r error message
		do
			test -n "${error}" || continue

			case "${message}" in
			'')	# Messages and stack frame traces are optional.
				element="${name}${nl}${error}"
				;;
			*)	element="${name}${nl}${error}${nl}${message}"
				;;
			esac

			offsets="${offsets}${sp}`expr length "${element}"`"
			agenda="${agenda}${element}"
		done <&${rfd}

		eval exec "${rfd}<&-"
		test -z "${agenda}" || status=$((${status} | 1))

		if test "${success}" -gt 0
		then
			element="${name}"
			offset=`expr length "${element}"`

			for dummy in `seq ${success}`
			do
				offsets="${offsets}${sp}${offset}"
				agenda="${agenda}${element}"
			done
		fi
		;;
	esac

	case "${class}" in
	"${class_}")
		success_=$((${success_} + success))
		failure_=$((${failure_} + failure))
		offsets_="${offsets_}${offsets}"
		agenda_="${agenda_}${agenda}"
		;;		# A prefix-matching class name, clustered.
	*)	test -z "${class_}" \
			|| _write_report "${1%%/}/${class_}.xml" \
				"`eval printf '%s' "${class_}"`" \
				${success_} ${failure_} \
				"${offsets_#${sp}}${sp}" "${agenda_}" \
			|| status=4		# Overbear the $? status.
		class_="${class}"
		success_=${success}
		failure_=${failure}
		offsets_="${offsets}"
		agenda_="${agenda}"
		;;		# A disparate class name or the sentinel name.
	esac
done

exit ${status}
