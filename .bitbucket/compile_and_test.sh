#!/bin/sh -e

: "${1:?no such library directory}"
: "${2:?no such cache directory}"
test -d "$1" || exit 1

jdk_home="`dirname \`dirname \\\`command -v javac\\\`\``"
ivy_jar="`printf '%s:' \`find "$1" -maxdepth 2 -iname \*.jar\``"
ivy_cache="$2/ivy2"
mvn_cache="$2/m2"
test -d "${ivy_cache}" || mkdir -p "${ivy_cache}"
test -d "${mvn_cache}" || mkdir -p "${mvn_cache}"

# There is a non-ASCII test class file name.
export LC_ALL=C.UTF-8 ANT_HOME="$1"/apache-ant JAVACMD="${jdk_home}"/bin/java
shift 2
cd ate/

# Let _ant_ and assemble_xml_reports.sh, in turn, make as much progress
# as possible and compute a compound exit status value unless either is
# timed out (124), failed after _timeout_ failure (125), non-executable
# (126), not found (127), or dispatched via signal (128+).
status_a=0
timeout -k 8 128 "${ANT_HOME}"/bin/ant \
	-noinput -silent -quiet -lib "${ivy_jar}" \
	-Date.ivy.repo.ivy2.dir="${ivy_cache}" \
	-Date.ivy.repo.m2.dir="${mvn_cache}" \
	-Dtest.javac.executable="${jdk_home}"/bin/javac \
	clean test "$@" \
|| status_a=$?
test "${status_a}" -lt 124 || exit ${status_a}

status_b=0
timeout -k 8 16 ./../.bitbucket/assemble_xml_reports.sh \
	build/test-results build/results-data \
|| status_b=$?
test "${status_b}" -lt 124 || exit ${status_b}
test "${status_b}" -eq 0 || status_b=$((${status_b} + 32))

exit $((${status_a} + ${status_b}))
